package com.huiyin.js;

import android.util.Log;
import android.webkit.JavascriptInterface;

import com.huiyin.AppContext;
import com.huiyin.api.URLs;

public class JsInterface {
	public interface BackBtnClickListener {
		public void backEnvent();
	}
	
	public interface WebViewClientClickListener {
		public void wvHasClickEnvent(String action, String args);
	}
	
	public interface WebViewClientClickListener2 {
		public void wvHasClickEnvent(String args);
	}

	private static final String TAG = "JsInterface";

	private BackBtnClickListener backBtnClickListener = null;
	
	private WebViewClientClickListener webViewClientClickListener = null;
	
	private WebViewClientClickListener2 webViewClientClickListener2 = null;

	public void setBackBtnClickListener(BackBtnClickListener listener) {
		backBtnClickListener = listener;
	}

	public void setWebViewClientClickListener(WebViewClientClickListener listener) {
		webViewClientClickListener = listener;
	}
	
	public void setWebViewClientClickListener2(WebViewClientClickListener2 listener) {
		webViewClientClickListener2 = listener;
	}
	
	/**
	 * 返回事件
	 */
	@JavascriptInterface
	public void backBtnFunction() {
		if (backBtnClickListener != null)
			backBtnClickListener.backEnvent();
	}
	
	/**
	 * 获取用户ID
	 * @return
	 */
	@JavascriptInterface
	public String getUserId() {
		Log.d(TAG, "getUserId : userId = " + AppContext.userId);
		return AppContext.userId;
	}
	
	/**
	 * 获取用户手机号
	 * @return
	 */
	@JavascriptInterface
	public String getPhone() {
		if(AppContext.mUserInfo != null) {
			Log.d(TAG, "phone : phone = " + AppContext.mUserInfo.phone);
			return AppContext.mUserInfo.phone;
		}
		return "";
	}
	
	/**
	 * 获取ServerUrl
	 * @return
	 */
	@JavascriptInterface
	public String getServerUrl() {
		Log.d(TAG, "SERVER_URL = " + URLs.SERVER_URL);
		return URLs.SERVER_URL;
	}
	
	/**
	 * 获取ImageUrl
	 * @return
	 */
	@JavascriptInterface
	public String getImageUrl() {
		return URLs.IMAGE_URL;
	}

	/**
	 * 调用本地事件
	 * @param action 具体的动作，根据action判断操作
	 * @param args 参数Json字符串
	 */
	@JavascriptInterface
	public void nativeFunction(String action, String args) {
		if(webViewClientClickListener != null) {
			webViewClientClickListener.wvHasClickEnvent(action, args);
		}
	}
	
	/**
	 * 调用本地事件
	 * @param action 具体的动作，根据action判断操作
	 * @param args 参数Json字符串
	 */
	@JavascriptInterface
	public void nativeFunction(String args) {
		if(webViewClientClickListener2 != null) {
			webViewClientClickListener2.wvHasClickEnvent(args);
		}
	}
	
	/**
	 * js调用native代码实现打开登录画面
	 * 使用户登录
	 */
	@JavascriptInterface
	public void login(){
		
	}
}