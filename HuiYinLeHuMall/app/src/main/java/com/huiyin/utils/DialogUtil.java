package com.huiyin.utils;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.huiyin.R;

public class DialogUtil {



	/**
	 * 显示性别对话框
	 * 
	 * @param context
	 */
	public static void showArrayDialog(Context context,final TextView selectView, int resArray, final OnClickListener onclick) {
		final String[] array = context.getResources().getStringArray(resArray);
		new Builder(context).setItems(array, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int position) {
				dialog.dismiss();
				selectView.setText(array[position]);
				onclick.onClick(selectView);
			}
		}).show();
	}

	/**
	 * 显示日期（生日）对话框
	 * 
	 * @param birthdayTextView
	 * @param context
	 */
	public static void showBirthdayDialog(final TextView birthdayTextView, Context context) {
		showBirthdayDialog(birthdayTextView, null, context);
	}
	
	/**
	 * 显示日期（生日）对话框
	 * 
	 * @param birthdayTextView
	 * @param context
	 */
	public static void showBirthdayDialog(final TextView birthdayTextView, final OnClickListener onOkClick, Context context) {
		
		final String oldBirthday = birthdayTextView.getText().toString().trim();
		
		Calendar c = Calendar.getInstance();
		int[] date = getDateInBirthdayItem(birthdayTextView.getText().toString().trim());
		if (null != date) {
			c.set(date[0], date[1] - 1, date[2]);
		}
		Dialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				
				String newBirthday = year + "-" + format(monthOfYear + 1) + "-" + format(dayOfMonth);
				
				//生日做了修改才更新到服务器
				if(!oldBirthday.equals(newBirthday)){
					birthdayTextView.setText(newBirthday);
					if(null != onOkClick){
						onOkClick.onClick(birthdayTextView);
					}
				}
			}
		}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();

	}

	/**
	 * 日期时间显示两位数的方法
	 * 
	 * @param x
	 *            Utils.format(月+1)
	 * @return
	 */
	public final static String format(int x) {
		String s = "" + x;
		if (s.length() == 1)
			s = "0" + s;
		return s;
	}
	
	/**
	 * 显示时间对话框
	 */
	public static void showTimeDialog(final TextView timeTextView, Context context) {
		final Calendar c = Calendar.getInstance();
		int[] date = getTimeItem(timeTextView.getText().toString().trim());
		if (null != date) {
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			c.set(year, month, day, date[0], date[1], date[2]);

		}
		Dialog dialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {

			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				c.setTimeInMillis(System.currentTimeMillis());
				c.set(Calendar.HOUR_OF_DAY, hourOfDay);
				c.set(Calendar.MINUTE, minute);
				c.set(Calendar.SECOND, 0); // 设为 0
				c.set(Calendar.MILLISECOND, 0); // 设为 0
				timeTextView.setText(DateUtil.getTimeShort(c.getTime()));
			}
		}, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
		dialog.show();
	}

	private static int[] getDateInBirthdayItem(String date) {
		if (TextUtils.isEmpty(date)) {
			return null;
		}

		String[] dates = date.split("-");
		int[] bir = new int[dates.length];
		bir[0] = Integer.parseInt(dates[0]);
		bir[1] = Integer.parseInt(dates[1]);
		try {
			bir[2] = Integer.parseInt(dates[2]);
		} catch (NumberFormatException nfe) {
			bir[2] = Integer.parseInt(dates[2].split(" ")[0].trim());
		}
		return bir;
	}

	private static int[] getTimeItem(String time) {
		if (TextUtils.isEmpty(time)) {
			return null;
		}

		String[] dates = time.split(":");
		int[] bir = new int[dates.length];
		bir[0] = Integer.parseInt(dates[0]);
		bir[1] = Integer.parseInt(dates[1]);
		try {
			bir[2] = Integer.parseInt(dates[2]);
		} catch (NumberFormatException nfe) {
			bir[2] = Integer.parseInt(dates[2].split(" ")[0].trim());
		} catch (IndexOutOfBoundsException nfe) {
			bir[2] = 0;
		}
		return bir;
	}


	/** 设置对话框的宽度 **/
	public static void changeDialogWidth(Dialog dialog, Context mContext) {
		if (null != dialog) {
			WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
			int width = mContext.getResources().getDimensionPixelSize(R.dimen.dp_118);
			params.width = width;
			dialog.getWindow().setAttributes(params);
		}
	}


	/**
	 * 获取对话框的message
	 * 
	 * @param message
	 * @param name
	 * @return
	 */
	public static SpannableStringBuilder setContentDsc(Context context, String message, String name, int color) {
		SpannableStringBuilder builder = new SpannableStringBuilder(message);
		ForegroundColorSpan orSpan = new ForegroundColorSpan(context.getResources().getColor(color));
		int index = message.indexOf(name);
		int length = name.length();
		builder.setSpan(orSpan, index, index + length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return builder;
	}
	
	/**
	 * 显示支付失败dialog
	 * @param mContext
	 * @param msg
	 */
	public static void showPayFailDialog(final Context mContext, String msg, final OnClickListener cancelClick) {
		View mView = LayoutInflater.from(mContext).inflate(R.layout.service_card_pay_fail_dialog, null);
		final Dialog mDialog = new Dialog(mContext, R.style.dialog);
		TextView pwd = (TextView) mView.findViewById(R.id.com_message_tv);
		pwd.setText(msg);
		Button yes = (Button) mView.findViewById(R.id.com_ok_btn);
		Button cancle = (Button) mView.findViewById(R.id.com_cancel_btn);
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
				
				if(null != cancelClick){
					cancelClick.onClick(v);
				}
			}
		});
		yes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		mDialog.setContentView(mView);
		mDialog.setCanceledOnTouchOutside(true);
		mDialog.setCancelable(true);
		mDialog.show();
	}
	
	/**
	 * 选择秀一秀的方式（秀好物 秀图  秀视频）
	 * @param mContext
	 * @param clickListener
	 */
	public static void showSelectMomentsMethodDialog(Context mContext, final OnClickListener clickListener) {
		
		View mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_select_moments_method, null);
		final Dialog mDialog = new Dialog(mContext, R.style.dialog);
		mDialog.setContentView(mView);
		mDialog.setCanceledOnTouchOutside(true);
		mDialog.setCancelable(true);
		
		LinearLayout mShowBgLayout = (LinearLayout) mView.findViewById(R.id.ll_show_bg);
		Button mShowGoodsBtn = (Button) mView.findViewById(R.id.btn_show_goods);
		Button mShowCamearBtn = (Button) mView.findViewById(R.id.btn_show_camera);
		Button mShowVideoBtn = (Button) mView.findViewById(R.id.btn_show_video);
		
		mShowBgLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		mShowGoodsBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickListener != null) {
					mDialog.dismiss();
					clickListener.onClick(v);
				}
			}
		});
		mShowCamearBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(clickListener != null) {
					mDialog.dismiss();
					clickListener.onClick(v);
				}
			}
		});
		mShowVideoBtn.setOnClickListener(new OnClickListener() {
	
			@Override
			public void onClick(View v) {
				if(clickListener != null) {
					mDialog.dismiss();
					clickListener.onClick(v);
				}
			}
		});
		
		WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
		lp.width = DensityUtil.getScreenWidth(((Activity) mContext)); //设置宽度
		lp.height = DensityUtil.getScreenHeight(((Activity) mContext)) - DensityUtil.dip2px(mContext, 46) - DensityUtil.getStatusBarHeight(((Activity) mContext)); //设置宽度
		lp.gravity = Gravity.BOTTOM;
		mDialog.getWindow().setAttributes(lp);
		
		mDialog.show();
	}
	
	/**
	 * 选择图片对话框
	 * @param mContext
	 * @param clickListener
	 */
	public static final void showPictureSelectDialog(Context mContext, final DialogInterface.OnClickListener clickListener) {
		AlertDialog dialog;
		Builder alert = new Builder(mContext);
		alert.setTitle("上传图片");
		alert.setItems(R.array.update_user_icon, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				clickListener.onClick(dialog, which);
			}
		});
		dialog = alert.create();
		dialog.show();
	}
	
	/**
	 * 显示带输入框的对话框
	 * @param mContext
	 * @param msg
	 */
	public static void showInputDialog(final Context mContext, String title, String hint, final InputDialogListener inputDialogListener) {
		View mView = LayoutInflater.from(mContext).inflate(R.layout.input_dialog, null);
		final Dialog mDialog = new Dialog(mContext, R.style.dialog);
		TextView titleTv = (TextView) mView.findViewById(R.id.tv_title);
		final EditText inputEdt = (EditText) mView.findViewById(R.id.edt_input);
		if(!StringUtils.isEmpty(title)) {
			titleTv.setText(title);
		}
		if(!StringUtils.isEmpty(hint)) {
			inputEdt.setHint(hint);
		}
		
		Button yes = (Button) mView.findViewById(R.id.btn_ok);
		Button cancle = (Button) mView.findViewById(R.id.btn_cancel);
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		yes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(null != inputDialogListener){
					inputDialogListener.onClick(mDialog, inputEdt.getText().toString().trim());
				}
			}
		});
		mDialog.setContentView(mView);
		mDialog.setCanceledOnTouchOutside(true);
		mDialog.setCancelable(true);
		mDialog.show();
	}
	
	public interface InputDialogListener {
		void onClick(Dialog dialog, String input);
	}
}
