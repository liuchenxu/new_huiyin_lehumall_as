package com.huiyin.utils;

import com.huiyin.AppContext;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.ui.shoppingcar.CommitOrderActivityNew;

import android.content.Context;
import android.content.Intent;

public class GRLiveUtil {

	/**
	 * 支付
	 * @param context   
	 * @param amounts  金额
	 * @param point 金币
	 */
	public static void startPay(final Context context, final String amounts, String point) {
		
		if(AppContext.userId == null) return; 
		
		RequstClient.createOrderForGongRong(AppContext.userId, amounts, point, new CustomResponseHandler(context) {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				int type = JSONParseUtils.getInt(content, "type");
				if(type == 1) {
					String orderCode = JSONParseUtils.getString(content, "orderCode");
					Intent intent = new Intent(context, CommitOrderActivityNew.class);
					intent.putExtra(CommitOrderActivityNew.INTENT_KEY_GR_LIVE, true);
                    intent.putExtra(CommitOrderActivityNew.INTENT_KEY_ORDER_NUM, orderCode);
                    intent.putExtra(CommitOrderActivityNew.INTENT_KEY_ORDER_PRICE, MathUtil.stringKeep2Decimal(amounts));
                    context.startActivity(intent);
				}
			}
		});
		
		
	
	}
	
}
