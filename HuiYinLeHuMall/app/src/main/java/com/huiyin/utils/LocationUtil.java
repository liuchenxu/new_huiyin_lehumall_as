package com.huiyin.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
/**
 * 定位工具类
 * @author zhyao
 *
 */
public class LocationUtil implements LocationListener {

	private static final String TAG = "LocationUtil";

	/**
	 * context
	 */
	private Context context;

	/**
	 * 定位manager
	 */
	private LocationManager locationManager;

	/**
	 * 解析经纬度
	 */
	private Geocoder geocoder;

	/**
	 * 开始定位
	 * @param context
	 */
	public void startLocation(Context context) {
		this.context = context;

		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		geocoder = new Geocoder(context);

		if (locationManager.getProvider(LocationManager.NETWORK_PROVIDER) != null) {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
			Log.d(TAG, "network location provider");
		} else if (locationManager.getProvider(LocationManager.GPS_PROVIDER) != null) {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
			Log.d(TAG, "gps location provider");
		} else {
			Log.d(TAG, "no location provider");
		}
	}

	@Override
	public void onLocationChanged(Location location) {

		HashMap<String , String> locaHashMap = getLocation(location);
		String provinceName = locaHashMap.get("provinceName");
		String cityName = locaHashMap.get("cityName");;
		setCurrentLocation(context, provinceName, cityName);
		if(locationListener != null) {
			locationListener.onReceiveLocation(provinceName, cityName);
		}
		//移除定位监听
		locationManager.removeUpdates(this);

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	/**
	 * 获取省市
	 * 
	 * @param location Location对象
	 * @return 
	 */
	private HashMap<String , String> getLocation(Location location) {

		double lat = location.getLatitude();
		double lng = location.getLongitude();
		List<Address> addressList = null;    
		String provinceName = null;
		String cityName = null;
		HashMap<String , String> locationMap = new HashMap<String , String>();
		try {
			addressList = geocoder.getFromLocation(lat, lng, 1);
			if(!addressList.isEmpty() && addressList.size() > 0) {
				provinceName = addressList.get(0).getAdminArea();
				cityName = addressList.get(0).getLocality();
				locationMap.put("provinceName", provinceName);
				locationMap.put("cityName", cityName);
			}
			//UIHelper.showToast("provinceName = " + provinceName + " cityName = " + cityName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.d(TAG, "provinceName = " + provinceName + "cityName = " + cityName);
		return locationMap;
	}
	
	interface LocationListener {
		void onReceiveLocation(String provinceName, String cityName);
	}
	
	private LocationListener locationListener;
	
	public void setCityLocationListener(LocationListener locationListener) {
		this.locationListener = locationListener;
	}
	
	/**
	 * 设置当前省市
	 * @param context
	 * @param provinceName
	 * @param cityName
	 */
	public static void setCurrentLocation(Context context, String provinceName, String cityName) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("currentCity", Activity.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putString("provinceName", provinceName);
		editor.putString("cityName", cityName);
		editor.commit();
	}
	
	/**
	 * 获取当前省市
	 * @param context
	 * @return
	 */
	public static HashMap<String , String> getCurrentLocation(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences("currentCity", Activity.MODE_PRIVATE);
		String provinceName = sharedPreferences.getString("provinceName", "");
		String cityName = sharedPreferences.getString("cityName", "");
		
		HashMap<String , String> locationMap = new HashMap<String , String>();
		if("".equals(provinceName) || "".equals(cityName)) {
			//定位失败，默认江苏南京
			locationMap.put("provinceName", "江苏省");
			locationMap.put("cityName", "南京市");
		}
		else {
			locationMap.put("provinceName", provinceName);
			locationMap.put("cityName", cityName);
		}
		
		return locationMap;
	}

}
