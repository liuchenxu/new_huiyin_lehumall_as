package com.huiyin.newpackage.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a on 2016/6/20.
 *
 * @author 赵云霄
 * @function:正则校验
 */
public class ZenCheckUtis {


    /**
     * 校验是否全部是汉子
     * */
    public static boolean getStringValue(String value) {
        String reg = "[\\u4e00-\\u9fa5]+$";
        Pattern p = Pattern.compile(reg);
        Matcher result = p.matcher(value);
        return value.matches(reg);
//        return result.find();
    }




}
