/**
 * 
 */
package com.huiyin.newpackage.event;

import java.io.IOException;
import java.net.URI;

import org.apache.http.Header;
import org.apache.http.HttpResponse;

import com.google.gson.Gson;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;
import com.huiyin.newpackage.entity.AppliedServiceInfoBean;
import com.huiyin.newpackage.entity.ApplyInfoBean;

import de.greenrobot.event.EventBus;

/**
 * 售后服务详情信息 查看进度查询中这个订单的详细信息
 * 
 * @author liuchenxu
 * 
 */
public class ApplyServiceDetailInfoEvent extends Event {
	private AppliedServiceInfoBean refundsInfo;

	public AppliedServiceInfoBean getRefundsInfo() {
		return refundsInfo;
	}

	public void setRefundsInfo(AppliedServiceInfoBean refundsInfo) {
		this.refundsInfo = refundsInfo;
	}
	public static void GetApplyServiceDetailInfo(AsyncHttpClient client,String order_service_id){
		RequestParams params=new RequestParams();
		params.put("order_service_id",order_service_id);//23+"");
		client.post(URLs.applied_servcie_order_info, params,new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				ApplyServiceDetailInfoEvent event = gson.fromJson(str, ApplyServiceDetailInfoEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				ApplyServiceDetailInfoEvent event = new ApplyServiceDetailInfoEvent();
				ApplyServiceDetailInfoEvent.onFailure(event);
			}
			
		});
		
	}
}
