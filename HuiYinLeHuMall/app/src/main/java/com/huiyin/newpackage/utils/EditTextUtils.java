package com.huiyin.newpackage.utils;

import android.widget.EditText;

public class EditTextUtils {

	public static String getText(EditText editText) {
		if (editText == null)
			return "";

		return editText.getText().toString().trim();

	}

	public static void setText(EditText editText, String content) {
		if (editText == null)
			return;

		editText.setText(content);

	}

	public static boolean isEmpty(EditText editText) {
		if (editText == null)
			return true;

		return getText(editText).isEmpty();

	}

	public static int getLength(EditText editText) {
		if (editText == null)
			return 0;

		return getText(editText).trim().length();

	}

}
