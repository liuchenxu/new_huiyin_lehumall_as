package com.huiyin.newpackage.utils;

import android.content.Context;
import android.net.Uri;

import com.huiyin.newpackage.ui.activity.CommonWebActivity;
import com.huiyin.ui.MainActivity;
import com.zxinsight.MLink;
import com.zxinsight.mlink.MLinkCallback;
import com.zxinsight.mlink.MLinkIntentBuilder;

import java.util.Map;

/**
 * Created by liuchenxu
 * 魔盒根据key跳转不同页面
 */
public class UrlDispatcher {

    public static void registerWithMLinkCallback(Context context,MLink mLink) {
//        MLink mLink = MagicWindowSDK.getMLink();
        mLink.register("index", new MLinkCallback() {
            public void execute(Map<String, String> paramMap, Uri uri, Context context) {
                paramMap.put("url",uri.toString());
                MLinkIntentBuilder.buildIntent(paramMap, context, MainActivity.class);
            }
        });
        mLink.register("activity", new MLinkCallback() {
            public void execute(Map<String, String> paramMap, Uri uri, Context context) {
                paramMap.put("url",uri.toString());
                MLinkIntentBuilder.buildIntent(paramMap, context, CommonWebActivity.class);
            }
        });
        mLink.register("activities", new MLinkCallback() {
            public void execute(Map<String, String> paramMap, Uri uri, Context context) {
                paramMap.put("url",uri.toString());
                MLinkIntentBuilder.buildIntent(paramMap, context, CommonWebActivity.class);
            }
        });

        mLink.register("general", new MLinkCallback() {
            @Override
            public void execute(Map<String, String> paramMap, Uri uri, Context context) {
                paramMap.put("url",uri.toString());
                MLinkIntentBuilder.buildIntent(paramMap, context, CommonWebActivity.class);
            }
        });

        mLink.registerDefault(new MLinkCallback() {
            @Override
            public void execute(Map<String, String> paramMap, Uri uri, Context context) {
                MLinkIntentBuilder.buildIntent(paramMap, context, MainActivity.class);
            }
        });
    }
}
