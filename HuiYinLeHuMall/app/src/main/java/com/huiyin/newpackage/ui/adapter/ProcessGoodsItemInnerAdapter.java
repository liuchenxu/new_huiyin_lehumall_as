package com.huiyin.newpackage.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.BespeakDetalListEntity;
import com.huiyin.newpackage.entity.GoodsDetail;
import com.huiyin.utils.ImageManager;

/**
 * 
 * @author liuchenxu
 *
 */
public class ProcessGoodsItemInnerAdapter extends BaseAdapter {

	private Context context;

	private List<GoodsDetail> items=new ArrayList<GoodsDetail>();

	public ProcessGoodsItemInnerAdapter(List<GoodsDetail> productList, Context context) {
		this.context = context;
		if(productList!=null){
			items.addAll(productList);
		}
	}
	
	public void refreshDatas(List<GoodsDetail> productList){
		if(productList!=null){
			items.clear();
			items.addAll(productList);
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MyHolder holder = null;
		if (null == convertView) {
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_process_search, null);
			holder = new MyHolder();
			holder.iv_product = (ImageView) convertView.findViewById(R.id.iv_product);
			holder.tv_product_name = (TextView) convertView.findViewById(R.id.tv_product_name);
			holder.tv_product_guige = (TextView) convertView.findViewById(R.id.tv_product_guige);
			holder.tv_product_price_and_num=(TextView)convertView.findViewById(R.id.tv_product_price_and_num);
			convertView.setTag(holder);
		} else
			holder = (MyHolder) convertView.getTag();
		GoodsDetail item = items.get(position);
		ImageManager.LoadWithServer(item.getGOODS_IMG(), holder.iv_product);
		holder.tv_product_name.setText(item.getGOODS_NAME());
		holder.tv_product_guige.setText(item.getGOODS_NORMS());
		holder.tv_product_price_and_num.setText("数量: x"+item.getGOODS_QUANTITY());
		return convertView;
	}

	class MyHolder {
		private ImageView iv_product;
		private TextView tv_product_name;
		private TextView tv_product_guige;
		private TextView tv_product_price_and_num;
	}
}
