package com.huiyin.newpackage.utils;

import android.os.Message;

public class UncodeUtf8 {

	public static String getResult(String msg) {
		String result = "";
		if (msg.isEmpty())
			return result;

		try {
			result = new String(msg.getBytes(), "utf-8");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return result;
	}

	public static String getResult(Object msg) {
		String result = "";
		if (msg.toString().isEmpty())
			return result;

		try {
			result = new String(msg.toString().getBytes(), "utf-8");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return result;
	}

	public static String getResult(Message msg) {
		String result = "";
		if (msg.obj.toString().isEmpty())
			return result;

		try {
			result = new String(msg.obj.toString().getBytes(), "utf-8");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return result;
	}

}
