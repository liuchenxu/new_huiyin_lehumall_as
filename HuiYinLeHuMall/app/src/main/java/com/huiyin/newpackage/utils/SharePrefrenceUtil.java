package com.huiyin.newpackage.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * shareprefrence配置
 * 
 */

public class SharePrefrenceUtil {

	public static final String NAME = "zero_shareperfrence";
	private static final String REALDEEP = "realdeep";
	private static final boolean DEAFULT_REALDEEP = false;

	/**
	 * 实时水深
	 * 
	 * @param context
	 * @param show
	 *            设置是否显示
	 */
	public static void setRealDeep(Context context, boolean show) {
		SharedPreferences spf = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		Editor editor = spf.edit();
		editor.putBoolean(REALDEEP, show);
		editor.commit();
	}

	public static boolean getRealDeep(Context context) {
		SharedPreferences spf = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		return spf.getBoolean(REALDEEP, DEAFULT_REALDEEP);
	}

	// 设置下拉刷新时间
	public static void setLabel(Context context, String className, String value) {
		SharedPreferences spf = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		Editor editor = spf.edit();
		editor.putString(className, value);
		editor.commit();
	}

	// 获取下拉刷新时间
	public static String getLabel(Context context, String className) {
		SharedPreferences spf = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		return spf.getString(className, "");
	}

}
