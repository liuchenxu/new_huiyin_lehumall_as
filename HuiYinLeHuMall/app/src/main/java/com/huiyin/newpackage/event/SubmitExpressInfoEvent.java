/**
 * 
 */
package com.huiyin.newpackage.event;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.huiyin.api.URLs;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;

import de.greenrobot.event.EventBus;

/**
 * @author liuchenxu 寄回商品的接口
 */
public class SubmitExpressInfoEvent extends Event {
	
	public static void SubmitExpressInfo(AsyncHttpClient client, RequestParams params) {
		
		client.post(URLs.submit_express_info, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				SubmitExpressInfoEvent event = gson.fromJson(str, SubmitExpressInfoEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				SubmitExpressInfoEvent event = new SubmitExpressInfoEvent();
				SubmitExpressInfoEvent.onFailure(event);
			}

		});
	}
}
