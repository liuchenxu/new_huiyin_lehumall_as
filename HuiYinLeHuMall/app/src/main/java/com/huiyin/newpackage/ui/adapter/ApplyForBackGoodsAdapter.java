package com.huiyin.newpackage.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.newpackage.entity.GoodsDetail;
import com.huiyin.newpackage.entity.OrderInfo;
import com.huiyin.newpackage.utils.ViewHolder;
import com.huiyin.ui.store.StoreHomeActivity;
import com.huiyin.ui.user.order.BackGoodsActivity;
import com.huiyin.utils.ImageManager;
import com.huiyin.wight.MyListView;

import java.util.ArrayList;

/**
 * 申请售后的订单适配器
 * 
 * @author 刘晨旭
 * 
 * @todo TODO
 * 
 * @date 2016-8-22
 */
public class ApplyForBackGoodsAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<OrderInfo> orderList=new ArrayList<OrderInfo>();

	public ApplyForBackGoodsAdapter(Context context, ArrayList<OrderInfo> orderList) {
		this.context = context;
		if(orderList!=null){
			this.orderList.clear();
			this.orderList.addAll(orderList);
		}
	}

	public void setOrderList(ArrayList<OrderInfo> orderList) {
		if(orderList!=null){
			this.orderList.clear();
			this.orderList.addAll(orderList);
		}
	}

	@Override
	public int getCount() {
		return null != orderList ? orderList.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return orderList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public String getOrderId(int position) {
		return orderList.get(position).getID()+"";
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_apply_for_bakc_goods, null);
		}
		initViews(convertView,position);
		return convertView;
	}
	
	/**
	 * 
	 * @param views
	 */
	private void initViews(View views,final int position){
		final OrderInfo goodsList=orderList.get(position);
		RelativeLayout order_info_layout=ViewHolder.get(views, R.id.order_info_layout);
		ImageView iv_logo = ViewHolder.get(views, R.id.iv_logo);
		TextView tv_store_name = ViewHolder.get(views, R.id.tv_store_name);
		TextView tv_order_time=ViewHolder.get(views,R.id.tv_order_time);//下单时间
		TextView tv_status=ViewHolder.get(views,R.id.tv_status);//订单状态 
		TextView tv_order_no=ViewHolder.get(views,R.id.tv_order_no);//订单号
		TextView tv_back_all_goods=ViewHolder.get(views,R.id.tv_back_all_goods);//整单退款
		MyListView lv_order=ViewHolder.get(views,R.id.lv_order);//货物列表
		//设置店铺icon 点击店铺名称
		ImageManager.LoadWithServer(orderList.get(position).getSTORE_LOGO(), iv_logo);
		if (goodsList.getSTORE_ID() > 0) {
			tv_store_name.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(context, StoreHomeActivity.class);
					intent.putExtra(StoreHomeActivity.STORE_ID, goodsList.getSTORE_ID());
					context.startActivity(intent);					
				}
			});
		}
		//订单详情
//		order_info_layout.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//                Intent intent = new Intent(context,AllOrderDetailActivity.class);
//                intent.putExtra(AllOrderDetailActivity.ORDER_ID, orderList.get(position).getID()+"");
//                context.startActivity(intent);
//
//			}
//		});
		tv_order_no.setText("订单编号："+orderList.get(position).getORDER_CODE());
		tv_store_name.setText(orderList.get(position).getSTORE_NAME() + "  ");
		tv_status.setText(orderList.get(position).getSTATUS_NAME());
		tv_order_time.setText("下单时间：" + orderList.get(position).getCREATE_DATE());
		
		if(goodsList.getIS_SHOW_COMPLETE()==1){
			tv_back_all_goods.setVisibility(View.VISIBLE);
		}
		else{
			tv_back_all_goods.setVisibility(View.INVISIBLE);
		}
		
		//整单退款
		tv_back_all_goods.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, BackGoodsActivity.class);
//				intent.putExtra(BackGoodsActivity.EXTRA_MODEL, returnBean);
				intent.putExtra(BackGoodsActivity.EXTRA_ORDERID,orderList.get(position).getID()+"" );
				intent.putExtra("ID", "");
				intent.putExtra("Order", orderList.get(position));
				context.startActivity(intent);
			}
		});
    		//ListView嵌套ListView
//        	tv_back_all_goods.setVisibility(View.VISIBLE);
    		if(goodsList.getOrderDetalList().size()>=0){
    			ArrayList<GoodsDetail> orderDetails=goodsList.getOrderDetalList();
    			BackGoodsInnerListAdapter adapter=new BackGoodsInnerListAdapter( orderDetails,context,true,orderList.get(position));
    			lv_order.setAdapter(adapter);
    		}
	}

	public void setShowOrGone(OrderHolder holder, int visible) {
		holder.iv_order1.setVisibility(visible);
		holder.iv_order2.setVisibility(visible);
		holder.iv_order3.setVisibility(visible);
		holder.tv_point.setVisibility(visible);
		holder.tv_describ.setVisibility(visible);
	}

	static class OrderHolder {
		public ImageView iv_logo;// 店铺logo
		public TextView tv_store_name;// 店铺名称
		public TextView tv_status;// 状态
		public TextView tv_order_no;// 订单编号
		public ImageView iv_order1;// 产品图片1
		public ImageView iv_order2;// 产品图片2
		public ImageView iv_order3;// 产品图片3
		public TextView tv_point;// 。。。
		public TextView tv_describ;// 产品描述
		public TextView tv_commit_order_time;// 下单时间
		public TextView btn_sqyy;// 申请预约
		public TextView btn_sqsh;// 申请换货
		public TextView btn_sqth;// 申请退货
		public LinearLayout layout_order;// item
		public TextView tv_order_time;//下单时间
	}

}
