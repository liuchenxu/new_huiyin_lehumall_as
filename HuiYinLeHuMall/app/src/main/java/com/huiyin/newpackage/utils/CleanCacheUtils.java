package com.huiyin.newpackage.utils;

import java.io.File;

import android.content.Context;
import android.util.Log;

public class CleanCacheUtils {
	private static final String APP_CACAHE_DIRNAME = "/webcache";
	private static String cache_name = "";

	public static void registLogin(Context context) {
		cache_name = context.getApplicationContext().getFilesDir().getAbsolutePath()
				+ APP_CACAHE_DIRNAME;
		Log.e("页面开始注销", "********************");
		// 清理Webview缓存数据库

		try {
			context.deleteDatabase("webview.db");
			context.deleteDatabase("webviewCache.db");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// WebView 缓存文件
		File appCacheDir = new File(context.getFilesDir().getAbsolutePath()
				+ APP_CACAHE_DIRNAME);

		File webviewCacheDir = new File(context.getCacheDir().getAbsolutePath()
				+ "/webviewCache");

		// 删除webview 缓存目录
		if (webviewCacheDir.exists()) {
			deleteFile(webviewCacheDir);
		}
		// 删除webview 缓存 缓存目录
		if (appCacheDir.exists()) {
			deleteFile(appCacheDir);
		}

	}

	private static void deleteFile(File file) {

		if (file.exists()) {
			if (file.isFile()) {
				file.delete();
			} else if (file.isDirectory()) {
				File files[] = file.listFiles();
				for (int i = 0; i < files.length; i++) {
					deleteFile(files[i]);
				}
			}
			file.delete();
		} else {

		}
	}

}
