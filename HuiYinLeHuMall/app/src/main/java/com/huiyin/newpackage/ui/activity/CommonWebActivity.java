package com.huiyin.newpackage.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.base.BaseActivity;
import com.huiyin.newpackage.utils.UserInfoUtils;
import com.huiyin.ui.web.CustomWebView;
import com.huiyin.utils.UpdateVersionTool;

/**
 * 通用的Webview页面为了让魔盒跳转到的页面
 *
 * @author liuchenxu
 */
public class CommonWebActivity extends BaseActivity {

    private CustomWebView mCustomWebView;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home_web);
        mCustomWebView = (CustomWebView) findViewById(R.id.webview);
        mCustomWebView.setOnRefreshEnable(false);
        mCustomWebView.set404Page(true);
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void loadData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (!TextUtils.isEmpty(intent.getStringExtra("url"))) {
                url=intent.getStringExtra("url").replace("huiyinh5toapp","http");


                if(UserInfoUtils.IsUserLogin()&&(!url.contains("userid="))){
                    if(url.contains("?")&&!url.endsWith("?")){
                        url = url + "&userid="+ AppContext.userId;
                    }
                    else if(url.contains("?")&&url.endsWith("?")){
                        url=url+"userid="+AppContext.userId;
                    }
                    else{
                        url=url+"?userid="+AppContext.userId;
                    }
                }
                //处理url中appVersion
                if(!url.contains("version=")){
                    String versionCode= UpdateVersionTool.getVersionCode(this)+"";
                    if(url.contains("?")&&!url.endsWith("?")){
                        url = url + "&version="+versionCode;
                    }
                    else if(url.contains("?")&&url.endsWith("?")){
                        url=url+"version="+versionCode;
                    }
                    else{
                        url=url+"?version="+versionCode;
                    }
                }

                try {
                    mCustomWebView.loadUrl(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
