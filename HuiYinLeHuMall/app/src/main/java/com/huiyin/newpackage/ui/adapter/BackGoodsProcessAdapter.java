/**
 * 
 */
package com.huiyin.newpackage.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import com.huiyin.R;
import com.huiyin.newpackage.entity.BackGoodsStepInfo;
import com.huiyin.newpackage.utils.ViewHolder;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 物流跟踪以及进度查询对应的adapter
 * @author liuchenxu
 *
 */
public class BackGoodsProcessAdapter extends BaseAdapter {

	private List<BackGoodsStepInfo> items=new ArrayList<BackGoodsStepInfo>();
	
	private Context mContext;
	
	public BackGoodsProcessAdapter(Context context){
		mContext=context;
	}
	
	public void setDatas(List<BackGoodsStepInfo> values){
		if(values!=null){
			items.clear();
			items.addAll(values);
			notifyDataSetChanged();
		}
	}
	
	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			convertView=LayoutInflater.from(mContext).inflate(R.layout.adapter_back_goods_step, null);
		}
		showView(convertView,position);
		return convertView;
	}
	
	private void showView(View convertView,int position){
		BackGoodsStepInfo bean=items.get(position);
		ImageView adapter_step_img=ViewHolder.get(convertView, R.id.iv_status);
		TextView first_time=ViewHolder.get(convertView, R.id.tv_time);
//		TextView second_time=ViewHolder.get(convertView, R.id.second_time);
		TextView main_info=ViewHolder.get(convertView, R.id.tv_describ);
//		TextView sub_info=ViewHolder.get(convertView, R.id.sub_info);
		ImageView dotted_pre=ViewHolder.get(convertView, R.id.dotted_pre);
		ImageView dotted=ViewHolder.get(convertView, R.id.dotted);
		first_time.setText(bean.getFirst_time());
		main_info.setText(bean.getMain_info());
		if(position==0){
			if(items.size()==1){
				dotted.setVisibility(View.INVISIBLE);
			}
			else {
				dotted.setVisibility(View.VISIBLE);
			}
			dotted_pre.setVisibility(View.INVISIBLE);
			adapter_step_img.setImageResource(R.drawable.logistics_icon_green);
			main_info.setTextColor(Color.parseColor("#32cf75"));
			first_time.setTextColor(Color.parseColor("#32cf75"));
		}
		else if(position==items.size()-1){
			main_info.setTextColor(Color.parseColor("#535353"));
			first_time.setTextColor(Color.parseColor("#aaaaaa"));
//			adapter_step_img.setImageResource(R.drawable.logistics_icon_gray);
			adapter_step_img.setImageResource(R.drawable.grey_cricle);
			dotted_pre.setVisibility(View.VISIBLE);
			dotted.setVisibility(View.INVISIBLE);
		}
		else{
			main_info.setTextColor(Color.parseColor("#535353"));
			first_time.setTextColor(Color.parseColor("#aaaaaa"));
//			adapter_step_img.setImageResource(R.drawable.logistics_icon_gray);
			adapter_step_img.setImageResource(R.drawable.grey_cricle);
			dotted_pre.setVisibility(View.VISIBLE);
			dotted.setVisibility(View.VISIBLE);
		}
	}

}
