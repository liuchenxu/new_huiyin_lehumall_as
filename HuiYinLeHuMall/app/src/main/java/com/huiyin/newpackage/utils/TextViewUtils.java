package com.huiyin.newpackage.utils;

import android.widget.TextView;

public class TextViewUtils {

    public static void setText(TextView textView, String content) {
        if (textView == null)
            return;

        textView.setText(content);

    }

    public static boolean isEmpty(TextView textView) {
        if (textView == null)
            return true;

        return getText(textView).isEmpty();

    }


    public static String getText(TextView textView) {
        if (textView == null)
            return "";

        return textView.getText().toString();

    }

    public static boolean getTextIsqt(String textView) {
        if (textView != null && textView.equals("其它")) {
            return true;
        } else {
            return false;
        }
    }

    public static String getTextString(String textView) {
        return textView == null ? "" : textView;
    }

    public static Long getTextLong(Long textView) {
        return textView == null ? 0 : textView;

    }

    public static int getLength(TextView textView) {
        if (textView == null)
            return 0;

        return getText(textView).length();

    }


}
