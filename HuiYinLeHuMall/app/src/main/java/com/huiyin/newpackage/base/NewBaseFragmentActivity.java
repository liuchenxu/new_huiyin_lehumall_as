package com.huiyin.newpackage.base;

import com.huiyin.R;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.ui.widget.CommonProgressDialog;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;


/**
 * @author Watermelon
 * @ClassName: BaseFragmentActivity
 * @Description:(基类配置)
 * @date 2015-9-7 下午3:41:52
 */
public class NewBaseFragmentActivity extends FragmentActivity {
    public InputMethodManager im;
    public DisplayMetrics dm; // 图片位置
    public static int screenX, ScreenY;
    public AsyncHttpClient client = new AsyncHttpClient();
    public SharedPreferences spf;
    public boolean register = false; // 是否注册接收的监听
    public Context mContext;
    public CommonProgressDialog pd = null;

    /**
     * @param savedInstanceState
     * @Title: onCreate
     * @Description:(系统方法)
     * @see FragmentActivity#onCreate(Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        screenX = dm.widthPixels;
        ScreenY = dm.heightPixels;
        if (register) {
            EventBus.getDefault().register(this);
        }
        im = ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE));
        pd = new CommonProgressDialog(this, client);
    }

    /**
     * @Title: onResume
     * @Description:(系统方法)
     * @see FragmentActivity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();

        if (!EventBus.getDefault().isRegistered(this) && register) {
            EventBus.getDefault().register(this);
        }

    }

    /**
     * @Title: onDestroy
     * @Description:(系统方法)
     * @see FragmentActivity#onDestroy()
     */
    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        hideSoftInput();
        super.onDestroy();
    }

    /**
     * @Title: onStop
     * @Description:(系统方法)
     * @see FragmentActivity#onStop()
     */
    @Override
    protected void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: hideSoftInput
     * @Description:(关闭输入法)
     */
    protected void hideSoftInput() {
        View view = getCurrentFocus();
        if (view != null) {
            IBinder binder = view.getWindowToken();
            if (binder != null) {
                im.hideSoftInputFromWindow(binder, 0);
            }
        }
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: showSoftInput
     * @Description:(弹出输入法)
     */
    public void showSoftInput() {
        im.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * @param @param message
     * @return void
     * @throws
     * @Title: showToast
     * @Description:(弹出Toast)
     */
    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param @param context
     * @param @param message
     * @return void
     * @throws
     * @Title: showToast
     * @Description:(弹出Toast)
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param @param text
     * @return void
     * @throws
     * @Title: showProgress
     * @Description:(显示异步上传时的等待进度框)
     */
    public void showProgress(Boolean isfinish) {
        pd.show();
        pd.setTip("请稍后");
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: finishProgress
     * @Description:(结束进度框)
     */
    public void finishProgress() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    /**
     * 刷新后更新
     *
     * @param listview
     */
    public void refreshComplete(final PullToRefreshBase<ListView> listview) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (listview != null) {
                    listview.onRefreshComplete();
                }
            }
        }, 100);
    }


}
