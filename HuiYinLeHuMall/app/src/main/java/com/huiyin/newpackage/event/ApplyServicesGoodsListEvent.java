package com.huiyin.newpackage.event;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import com.google.gson.Gson;
import com.huiyin.AppContext;
import com.huiyin.api.URLs;
import com.huiyin.bean.GoodList;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.android.http.AsyncHttpResponseHandler;
import com.huiyin.newpackage.android.http.RequestParams;
import com.huiyin.newpackage.entity.OrderInfo;

import de.greenrobot.event.EventBus;

/**
 * 申请售后请求的货物列表接口
 * @author liuchenxu
 *
 */
public class ApplyServicesGoodsListEvent extends Event {
	
	public ArrayList<OrderInfo> orderList;

	private int pageIndex;

	private int totalPageNum;

	public ArrayList<OrderInfo> getOrderList() {
		return orderList;
	}



	public void setOrderList(ArrayList<OrderInfo> orderList) {
		this.orderList = orderList;
	}



	public int getPageIndex() {
		return pageIndex;
	}



	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}



	public int getTotalPageNum() {
		return totalPageNum;
	}



	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}



	/**
	 * 可申请售后服务订单列表
	 * @param pageIndex
	 */
	public static void GetApplyServcieGoodsList(AsyncHttpClient client, int pageIndex,String keyWords){
		RequestParams params = new RequestParams();
		params.put("user_id", AppContext.userId);
		params.put("searchKey", keyWords);
		params.put("pageIndex", pageIndex+"");
		client.post(URLs.apply_goods_list, params, new AsyncHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String str = new String(responseBody);
				Gson gson = new Gson();
				ApplyServicesGoodsListEvent event = gson.fromJson(str, ApplyServicesGoodsListEvent.class);
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
				ApplyServicesGoodsListEvent event = new ApplyServicesGoodsListEvent();
				ApplyServicesGoodsListEvent.onFailure(event);
			}
			
		} );
	}
	
}
