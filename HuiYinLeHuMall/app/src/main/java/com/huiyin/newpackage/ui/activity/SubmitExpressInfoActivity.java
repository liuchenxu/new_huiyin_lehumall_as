package com.huiyin.newpackage.ui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.newpackage.android.http.RequestParams;
import com.huiyin.newpackage.base.NewBaseActivity;
import com.huiyin.newpackage.consts.HTTPConsts;
import com.huiyin.newpackage.entity.AppliedServiceInfoBean;
import com.huiyin.newpackage.entity.ExpressInfoEntity;
import com.huiyin.newpackage.event.GetExpressCompanyListEvent;
import com.huiyin.newpackage.event.SubmitExpressInfoEvent;
import com.huiyin.ui.housekeeper.SelectApplyReasonWindow;
import com.huiyin.utils.DeviceUtils;

/**
 * 提交寄回商品的快递信息
 * 
 * @author liuchenxu
 * 
 */
public class SubmitExpressInfoActivity extends NewBaseActivity {

	private ArrayList<ExpressInfoEntity> expressCompanies = new ArrayList<ExpressInfoEntity>();
	private LinearLayout select_express_company;
	private TextView express_company_name;
	private EditText express_num;
	private TextView submit_express_info;
	private TextView not_send_package;
	private TextView ab_back;
	Map<String, String> expressMap = new HashMap<String, String>();
	private String order_service_id;
	private String user_name;
	private String store_id;
	private String address;
	private TextView ab_title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		register = true;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_express_info);
		Intent intent = getIntent();
		order_service_id = intent.getStringExtra("order_service_id");
		user_name = intent.getStringExtra("user_name");
		store_id = intent.getStringExtra("store_id");
		address = intent.getStringExtra("address");
		initViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		 showProgress(true);
		GetExpressCompanyListEvent.GetExpressCompanies(client);
	}

	private void initViews() {
		ab_title=(TextView)findViewById(R.id.ab_title);
		ab_title.setText("");
		ab_title.setText("寄回商品");
		ab_back = (TextView) findViewById(R.id.ab_back);
		select_express_company = (LinearLayout) findViewById(R.id.select_express_company);
		express_company_name = (TextView) findViewById(R.id.express_company_name);
		express_num = (EditText) findViewById(R.id.express_num);
		submit_express_info = (TextView) findViewById(R.id.submit_express_info);
		not_send_package = (TextView) findViewById(R.id.not_send_package);
		select_express_company.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SelectApplyReasonWindow pop = new SelectApplyReasonWindow(SubmitExpressInfoActivity.this, expressMap,
						null, false, new SelectApplyReasonWindow.IOnItemClick() {
							@Override
							public void onItemClick(String id, String value) {
								express_company_name.setTag(id);
								express_company_name.setText(value);
							}
						});
				if (!pop.isShowing()) {
					pop.showAtLocation(select_express_company, Gravity.BOTTOM, 0, 0);
					int height = pop.getHeight();
					int screenHeight = DeviceUtils.getHeightMaxPx(SubmitExpressInfoActivity.this);
					int maxHeight = ((screenHeight / 3) * 2);
					if (height > maxHeight) {
						height = maxHeight;
						pop.setHeight(height);
					}
				}
			}
		});
		// 未寄包裹
		not_send_package.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SubmitExpressInfoActivity.this.finish();
			}
		});
		// 寄出包裹
		submit_express_info.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!checkEmpty()){
					return;
				}
				RequestParams params = new RequestParams();
				params.put("order_service_id", order_service_id);
				params.put("express_company", express_company_name.getText().toString());
				params.put("express_no", express_company_name.getTag().toString().trim());
				params.put("express_code", express_num.getText().toString());
				params.put("user_id", AppContext.userId + "");
				params.put("user_name", user_name);
				params.put("store_id", store_id);
				params.put("address", address);
				SubmitExpressInfoActivity.this.showProgress(true);
				SubmitExpressInfoEvent.SubmitExpressInfo(client, params);
			}
		});
		ab_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SubmitExpressInfoActivity.this.finish();
			}
		});
		express_company_name.setText("");
	}
	
	private boolean checkEmpty(){
		if(TextUtils.isEmpty(express_company_name.getText().toString())||TextUtils.isEmpty(express_num.getText().toString())){
			Toast.makeText(this, "请补全物流信息", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param event
	 */
	public void onEvent(GetExpressCompanyListEvent event) {
//		 showProgress(false);
		finishProgress();
		if (event.type == HTTPConsts.HTTP_OK) {
			if (event.getCompanyList() != null) {
				expressCompanies.clear();
				expressCompanies.addAll(event.getCompanyList());
				expressMap.clear();
				for (ExpressInfoEntity iterable_element : expressCompanies) {
					expressMap.put(iterable_element.getCOMPANY_NO() + "", iterable_element.getCOMPANY_NAME());
				}
				express_company_name.setTag(expressCompanies.get(0).getCOMPANY_NO() + "");
				express_company_name.setText(expressCompanies.get(0).getCOMPANY_NAME() + "");
			}
		} else {
			String message = event.msg;
			if (message != null) {
				showToast(message);
			}
		}
		express_company_name.setText("");
	}

	/**
	 * 退换货提交了物流信息
	 * 
	 * @param event
	 */
	public void onEvent(SubmitExpressInfoEvent event) {
//		showProgress(false);
		finishProgress();
		if (event.type == HTTPConsts.HTTP_OK) {
			this.finish();
		} else {
			String message = event.msg;
			if (message != null) {
				showToast(message);
			}
		}
	}

}
