/**
 * 
 */
package com.huiyin.newpackage.entity;

import java.io.Serializable;

/**
 * @author liuchenxu
 *退货进度实体类
 */
public class ServiceStepInfo implements Serializable {
	
	private String CREATE_TIME;

	private String DESCRIPTION;

	public String getCREATE_TIME() {
		return CREATE_TIME;
	}

	public void setCREATE_TIME(String cREATE_TIME) {
		CREATE_TIME = cREATE_TIME;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	
}
