package com.huiyin.newpackage.base;
import com.huiyin.R;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.newpackage.android.http.AsyncHttpClient;
import com.huiyin.newpackage.ui.widget.CommonProgressDialog;
import com.huiyin.wight.Tip;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;


/**
 * @author Watermelon
 * @ClassName: BaseFragment
 * @Description:(基类配置)
 * @date 2015-9-7 下午3:40:35
 */
public abstract class NewBaseFragment extends Fragment {
    public Context mContext;
    public AsyncHttpClient client = new AsyncHttpClient();
    public boolean register = false;
    public int x, y;
    public int pageSize = 10;
    protected Dialog loading, dialog;
    public CommonProgressDialog pd = null;

    /**
     * @param savedInstanceState
     * @Title: onCreate
     * @Description:(系统方法)
     * @see Fragment#onCreate(Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (register) {
            EventBus.getDefault().register(this);
        }
        mContext = getActivity();
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        x = dm.widthPixels;
        y = dm.heightPixels;
        pd = new CommonProgressDialog(getActivity(), client);
        super.onCreate(savedInstanceState);
    }

    /**
     * @Title: onResume
     * @Description:(系统方法)
     * @see Fragment#onResume()
     */
    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this) && register) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    /**
     * @Title: onStop
     * @Description:(系统方法)
     * @see Fragment#onStop()
     */
    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        // ImageLoader.getInstance().clearMemoryCache();
        super.onStop();
    }

    /**
     * @Title: onDestroy
     * @Description:(系统方法)
     * @see Fragment#onDestroy()
     */
    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: hideSoftInput
     * @Description:(输入法)
     */
    protected void hideSoftInput() {
        if ((getActivity() == null) || (getView() == null))
            return;
        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    /**
     * @param @param text
     * @return void
     * @throws
     * @Title: showProgress
     * @Description:(显示异步上传时的等待进度框)
     */
    public void showProgress(Boolean isfinish) {
//        pd.show();
//        pd.setTip("请稍后...");
        
        Tip.showLoadDialog(mContext,
                mContext.getString(R.string.loading));
    }

    /**
     * @param
     * @return void
     * @throws
     * @Title: finishProgress
     * @Description:(结束进度框)
     */
    public void finishProgress() {
//        if (pd != null) {
//            pd.dismiss();
//        }
    	Tip.colesLoadDialog();
    }

    /**
     * @param @param message
     * @return void
     * @throws
     * @Title: showToast
     * @Description:(弹出Toast)
     */
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param @param message
     * @return void
     * @throws
     * @Title: showToast
     * @Description:(弹出Toast)
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 刷新后更新
     *
     * @param listview
     */
    public void refreshComplete(final PullToRefreshBase<ListView> listview) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (listview != null) {
                    listview.onRefreshComplete();
                }
            }
        }, 100);
    }

    // 弹窗显示Dialog
    protected void showDialogView(String msg, boolean outsideCancel) {
        disShowDialogView();
        if (loading == null)
            loading = new Dialog(getActivity(), R.style.Theme_Light_FullScreenDialogAct);
        View view = getActivity().getLayoutInflater().inflate(R.layout.layout_progress_dialog_view, null);
        TextView textview = (TextView) view.findViewById(R.id.dialogtext);
        textview.setText(msg);

        loading.addContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        Window manager = loading.getWindow();
        loading.setCanceledOnTouchOutside(outsideCancel);
        manager.setGravity(Gravity.CENTER);
        loading.show();
    }

    // 消失

    protected void disShowDialogView() {
        if (loading != null) {
            loading.dismiss();
        }
    }

    // 显示dialog的view
    protected void showMyDialog(View view, boolean outsideCancel) {
        disMyDialog();
        if (dialog == null) {
            dialog = new Dialog(getActivity(), R.style.dialog2);
        }
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.width = x / 4 * 3;
        dialog.addContentView(view, params);
        dialog.setCanceledOnTouchOutside(outsideCancel);
        dialog.show();

    }

    protected void disMyDialog() {
        if (dialog != null)
            if (dialog.isShowing())
                dialog.dismiss();

    }
//    public void onEvent(NoticeShowAlertEvent event) {
//
//        new AlertReciverDialog(mContext).builder().setViewcon(event.notice).show();
//    }

}
