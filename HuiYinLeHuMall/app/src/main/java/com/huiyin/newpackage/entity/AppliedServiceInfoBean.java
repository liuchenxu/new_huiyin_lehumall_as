/**
 * 
 */
package com.huiyin.newpackage.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author liuchenxu 售后服务详情信息 实体类
 */
public class AppliedServiceInfoBean implements Serializable {
	private Address address;
	private ArrayList<GoodsDetail> orderDetailList;
	private ApplyInfoBean orderServiceInfo;
	private ArrayList<ServiceStepInfo> orderServiceLog;
	private LogisticsInfoBean orderServiceLogistical;
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public ArrayList<GoodsDetail> getOrderDetailList() {
		return orderDetailList;
	}
	public void setOrderDetailList(ArrayList<GoodsDetail> orderDetailList) {
		this.orderDetailList = orderDetailList;
	}
	public ApplyInfoBean getOrderServiceInfo() {
		return orderServiceInfo;
	}
	public void setOrderServiceInfo(ApplyInfoBean orderServiceInfo) {
		this.orderServiceInfo = orderServiceInfo;
	}
	public ArrayList<ServiceStepInfo> getOrderServiceLog() {
		return orderServiceLog;
	}
	public void setOrderServiceLog(ArrayList<ServiceStepInfo> orderServiceLog) {
		this.orderServiceLog = orderServiceLog;
	}
	public LogisticsInfoBean getOrderServiceLogistical() {
		return orderServiceLogistical;
	}
	public void setOrderServiceLogistical(LogisticsInfoBean orderServiceLogistical) {
		this.orderServiceLogistical = orderServiceLogistical;
	}
}
