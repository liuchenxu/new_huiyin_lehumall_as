/**
 * 
 */
package com.huiyin.newpackage.entity;

import java.io.Serializable;

/**
 * @author liuchenxu
 *物流公司信息实体类
 */
public class ExpressInfoEntity implements Serializable {
	
	private String COMPANY_NO;

	private String COMPANY_NAME;

	private long ID;

	public String getCOMPANY_NO() {
		return COMPANY_NO;
	}

	public void setCOMPANY_NO(String cOMPANY_NO) {
		COMPANY_NO = cOMPANY_NO;
	}

	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}

	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}

	public long getID() {
		return ID;
	}

	public void setID(long iD) {
		ID = iD;
	}
	
	
}
