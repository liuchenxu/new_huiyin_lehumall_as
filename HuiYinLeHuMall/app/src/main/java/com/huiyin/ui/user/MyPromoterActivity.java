package com.huiyin.ui.user;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.PromoterBean;
import com.huiyin.bean.PromoterBean.RebatesStatistics;
import com.huiyin.utils.JSONParseUtils;
import com.huiyin.utils.MathUtil;
import com.huiyin.utils.StringUtils;

/**
 * 我的推广页面
 * 
 * @author zhyao
 * 
 */
public class MyPromoterActivity extends BaseActivity implements OnClickListener {

	/** 账户余额 **/
	private TextView mAccountMoneyTv;

	/** 推广订单 **/
	private TextView mOrderCountTv;

	/** 推广订单总金额 **/
	private TextView mOrderAllTv;

	/** 本月新增客户 **/
	private TextView mClientCountTv;

	/** 累计客户 **/
	private TextView mClientAllTv;

	/** 我的推荐码 **/
	private TextView mMyReferralCode;

	/** 申请推荐码 **/
	private Button mApplyBtn;

	/** 推广员编号 **/
	private String promoterId;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_my_promoter);

		initView();

		initData();
	}

	private void initView() {

		mAccountMoneyTv = (TextView) findViewById(R.id.tv_account_money);
		mOrderCountTv = (TextView) findViewById(R.id.tv_order_count);
		mOrderAllTv = (TextView) findViewById(R.id.tv_order_all);
		mClientCountTv = (TextView) findViewById(R.id.tv_client_count);
		mClientAllTv = (TextView) findViewById(R.id.tv_client_all);
		mMyReferralCode = (TextView) findViewById(R.id.tv_my_referralCode);
		mApplyBtn = (Button) findViewById(R.id.btn_apply);

		mApplyBtn.setOnClickListener(this);
	};

	private void initData() {

		String userId = AppContext.getInstance().userId;
		queryPromoter(userId);

	}

	/**
	 * 查询我的推广
	 * 
	 * @param userId
	 */
	private void queryPromoter(String userId) {
		RequstClient.queryPromoter(userId, null,
				new CustomResponseHandler(this) {
					@Override
					public void onSuccess(String content) {
						super.onSuccess(content);

						PromoterBean bean = PromoterBean.explainJson(content,
								MyPromoterActivity.this);
						try {
							if (bean.isSuccess()) {

								promoterId = bean.getPromoter().getId();
								refreshPromoterUI(bean.getPromoter()
										.getRebatesStatistics());
								refreshReferralCode(bean.getPromoter()
										.getReferralCode());

							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	/**
	 * 申请我的推广码
	 * 
	 * @param promoterId
	 */
	private void getReferralCode(String promoterId) {
		RequstClient.getReferralCode(promoterId,
				new CustomResponseHandler(this) {
					@Override
					public void onSuccess(String content) {
						super.onSuccess(content);

						boolean isSuccess = JSONParseUtils.getBoolean(content,
								"success");
						if (isSuccess) {
							String referralCode = JSONParseUtils.getString(
									content, "referralCode");
							refreshReferralCode(referralCode);
						} else {
							UIHelper.showToast(JSONParseUtils.getString(
									content, "msg"));
						}
					}
				});
	}

	/**
	 * 设置我的推广信息
	 * 
	 * @param rebatesStatistics
	 */
	private void refreshPromoterUI(RebatesStatistics rebatesStatistics) {
		mAccountMoneyTv = (TextView) findViewById(R.id.tv_account_money);
		mOrderCountTv = (TextView) findViewById(R.id.tv_order_count);
		mOrderAllTv = (TextView) findViewById(R.id.tv_order_all);
		mClientCountTv = (TextView) findViewById(R.id.tv_client_count);
		mClientAllTv = (TextView) findViewById(R.id.tv_client_all);

		mAccountMoneyTv.setText(MathUtil.stringKeep2Decimal(rebatesStatistics
				.getAmount()));
		mOrderCountTv.setText(rebatesStatistics.getOrderNum());
		mOrderAllTv.setText(MathUtil.stringKeep2Decimal(rebatesStatistics
				.getTotalOrderAmount()));
		mClientCountTv.setText(rebatesStatistics.getCurrentMonthRegisterNum());
		mClientAllTv.setText(rebatesStatistics.getTotalRegisterNum());

	}

	/**
	 * 设置是否需要申请推广码
	 * 
	 * @param referralCode
	 */
	private void refreshReferralCode(String referralCode) {
		if (StringUtils.isEmpty(referralCode) || "0".equals(referralCode)) {
			mApplyBtn.setVisibility(View.VISIBLE);
			mMyReferralCode.setVisibility(View.GONE);
		} else {
			mApplyBtn.setVisibility(View.GONE);
			mMyReferralCode.setVisibility(View.VISIBLE);
			mMyReferralCode.setText("我的推荐码:" + referralCode);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_apply:
			getReferralCode(promoterId);
			break;

		default:
			break;
		}

	}

}
