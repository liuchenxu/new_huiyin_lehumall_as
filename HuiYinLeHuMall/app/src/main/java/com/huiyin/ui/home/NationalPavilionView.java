package com.huiyin.ui.home;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.huiyin.R;
import com.huiyin.bean.NationalPavilionBean;
import com.huiyin.ui.web.GoodsListWebActivity;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
/**
 * 国家馆
 * @author zhyao
 *
 */
public class NationalPavilionView extends LinearLayout{
	
	private Context mContext;
	
	private List<NationalPavilionBean> mNationalPavilionList;
	
	private MyGridView mNationalPavilionGridView;
	
	private NationalPavilionAdapter mNationalPavilionAdapter;


	public NationalPavilionView(Context context) {
		super(context);
		initLayout(context);
	}

	public NationalPavilionView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initLayout(context);
	}

	public NationalPavilionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLayout(context);
	}
	

	public NationalPavilionView(Context context, List<NationalPavilionBean> nationalPavilionList) {
		super(context);
		mNationalPavilionList = nationalPavilionList;
		initLayout(context);
	}
	
	private void initLayout(Context context) {
		mContext = context;
		View view = LayoutInflater.from(mContext).inflate(R.layout.national_pavilion_view, null, false);
		mNationalPavilionGridView = (MyGridView) view.findViewById(R.id.national_pavilion_grid);
		mNationalPavilionAdapter = new NationalPavilionAdapter();
		mNationalPavilionGridView.setAdapter(mNationalPavilionAdapter);
		mNationalPavilionGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				NationalPavilionBean bean = mNationalPavilionList.get(position);
//				Intent intent = new Intent(mContext, CategorySearchActivity.class);
//                intent.putExtra(CategorySearchActivity.BUNDLE_MARK, "8");
//				intent.putExtra(CategorySearchActivity.BUNDLE_ORGIN_ID, bean.getORIGIN_IDS());
//				mContext.startActivity(intent);
				NationalPavilionBean bean = mNationalPavilionList.get(position);
				Intent intent = new Intent(mContext, GoodsListWebActivity.class);
                intent.putExtra(GoodsListWebActivity.BUNDLE_MARK, "8");
				intent.putExtra(GoodsListWebActivity.BUNDLE_ORGIN_ID, bean.getORIGIN_IDS());
				mContext.startActivity(intent);
			}
		});
		
		addView(view);
	}
	
	class NationalPavilionAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return mNationalPavilionList != null ? mNationalPavilionList.size() : 0;
		}

		@Override
		public Object getItem(int position) {
			return mNationalPavilionList != null ? mNationalPavilionList.get(position) : null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ViewHolder holder;
			if(convertView == null) {
				holder = new ViewHolder();
				convertView = LayoutInflater.from(mContext).inflate(R.layout.national_pavilion_gridview_item, null);
				holder.mNationalPavilionImg = (ImageView) convertView.findViewById(R.id.img_national_pavilion);
				int width = (DensityUtil.getScreenWidth((Activity)mContext) - 2 * DensityUtil.dip2px(mContext, 15) - 3 * DensityUtil.dip2px(mContext, 10)) /4;
				LayoutParams params = new LayoutParams(width, width);
				holder.mNationalPavilionImg.setLayoutParams(params);
				convertView.setTag(holder);
			}
			else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			ImageManager.Load(mNationalPavilionList.get(position).getIMG(), holder.mNationalPavilionImg);
			
			return convertView;
		}
		
	    class ViewHolder {
			ImageView mNationalPavilionImg;
		}
	}

}
