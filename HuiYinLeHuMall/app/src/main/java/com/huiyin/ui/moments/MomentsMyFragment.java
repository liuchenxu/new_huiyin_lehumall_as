package com.huiyin.ui.moments;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.bean.MomentsShowItem;
import com.huiyin.bean.MomentsShowListBean;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.Mode;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.OnRefreshListener2;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshListView;
import com.huiyin.ui.moments.adapter.MomentsShowAdapter;
import com.huiyin.utils.Utils;
import com.jcvideoplayer.JCVideoPlayer;

/**
 * 秀场我的
 * 
 * @author zhyao
 * 
 */
public class MomentsMyFragment extends Fragment implements OnRefreshListener2<ListView> {

	private static final String TAG = "MomentsMyFragment";

	private View rootView;

	/** 无数据布局 **/
	private LinearLayout mNoDataLayout;

	/** 我的秀场列表 **/
	private PullToRefreshListView mMyShowList;

	/** 秀场列表adapter **/
	private MomentsShowAdapter mMomentsShowAdapter;

	/** 我的秀场 list **/
	private ArrayList<MomentsShowItem> myShowList;

	/** 分页： 当前页 **/
	private int pageIndex = 1;

	/** 分页： 每页大小 **/
	private int pageSize = 10;

	/** 分页： 总页数 **/
	private int totalPageNum;

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		rootView = inflater.inflate(R.layout.fragment_moments_my, null);
		initView();
		initData();
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, "onResume");

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);

		if (!isVisibleToUser) {
			JCVideoPlayer.releaseAllVideos();
		}
	}
	
	/**
	 * 解决setUserVisibleHint不响应bug，父类调用刷新方法
	 */
	public void onRefreshData() {
		
	}

	@Override
	public void onPause() {
		super.onPause();
		JCVideoPlayer.releaseAllVideos();
	}

	private void initView() {
		mNoDataLayout = (LinearLayout) rootView.findViewById(R.id.ll_no_data);
		// 秀场列表
		mMyShowList = (PullToRefreshListView) rootView.findViewById(R.id.lv_moments_show);
		mMyShowList.setMode(Mode.BOTH);
		mMyShowList.getLoadingLayoutProxy().setLastUpdatedLabel(Utils.getCurrTiem());
		mMyShowList.getLoadingLayoutProxy().setPullLabel("往下拉更新数据...");
		mMyShowList.getLoadingLayoutProxy().setRefreshingLabel("正在载入中...");
		mMyShowList.getLoadingLayoutProxy().setReleaseLabel("放开更新数据...");
		mMyShowList.setOnRefreshListener(this);
	}

	public void initData() {
		if (myShowList == null) {
			myShowList = new ArrayList<MomentsShowItem>();
		}
		else {
			myShowList.clear();
		}
		pageIndex = 1;

		requestMyShowList(pageIndex, pageSize);
	}

	/**
	 * 下拉刷新
	 * 
	 * @param refreshView
	 */
	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
		JCVideoPlayer.releaseAllVideos();

		initData();
	}

	/**
	 * 上拉加载更多
	 * 
	 * @param refreshView
	 */
	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		pageIndex++;
		if (pageIndex <= totalPageNum) {
			requestMyShowList(pageIndex, pageSize);
		} else {
			// mMyShowList.setMode(Mode.PULL_FROM_START);
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					mMyShowList.onRefreshComplete();
				}
			}, 100);
		}
	}

	/**
	 * 请求秀场首页数据
	 * 
	 * @param pageIndex
	 * @param pageSize
	 */
	private void requestMyShowList(int pageIndex, int pageSize) {
		RequstClient.myShowList(AppContext.userId, pageIndex, pageSize, new CustomResponseHandler(
				getActivity()) {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);

				MomentsShowListBean bean = MomentsShowListBean.explainJson(content, getActivity());

				// 成功
				if (bean.type == 1) {
					totalPageNum = bean.getTotalPageNum();
					try {
						refreshShowListData(bean);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				// 失败
				else {
					UIHelper.showToast(bean.msg);
				}
			}
			
			@Override
			public void onFailure(String error, String errorMessage) {
				super.onFailure(error, errorMessage);
				
				setNoDataUI(true);
				
				mMyShowList.onRefreshComplete();
			}
		});
	}

	/**
	 * 刷新数据
	 * 
	 * @param bean
	 */
	private void refreshShowListData(MomentsShowListBean bean) {

		if (bean.getShowList() != null) {
			myShowList.addAll(bean.getShowList());
		}
		
		setAdapter();

		mMyShowList.onRefreshComplete();
		
		setNoDataUI(myShowList.isEmpty());
	}

	/**
	 * 设置列表adapter
	 */
	private void setAdapter() {

		if (mMomentsShowAdapter == null) {
			mMomentsShowAdapter = new MomentsShowAdapter(getActivity(), myShowList);
			mMyShowList.getRefreshableView().setAdapter(mMomentsShowAdapter);
		}

	}

	/**
	 * 无数据UI
	 * 
	 * @param flag 
	 */
	private void setNoDataUI(boolean flag) {
		if (flag) {
			mNoDataLayout.setVisibility(View.VISIBLE);
			mMyShowList.setVisibility(View.GONE);
		} else {
			mNoDataLayout.setVisibility(View.GONE);
			mMyShowList.setVisibility(View.VISIBLE);
		}
	}

}
