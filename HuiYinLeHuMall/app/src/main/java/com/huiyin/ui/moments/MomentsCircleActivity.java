package com.huiyin.ui.moments;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.CircleDetailBean;
import com.huiyin.bean.CircleDetailBean.CircleMap;
import com.huiyin.bean.MomentsShowItem;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.Mode;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.OnRefreshListener2;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshListView;
import com.huiyin.ui.moments.adapter.MomentsShowAdapter;
import com.huiyin.utils.AppManager;
import com.huiyin.utils.ImageManager;
import com.huiyin.utils.Utils;

/**
 * 当前圈子界面
 * 
 * @author zhyao
 * 
 */
public class MomentsCircleActivity extends BaseActivity implements OnClickListener, OnRefreshListener2<ListView> {

	private static final String TAG = "MomentsCircleActivity";

	public static final String INTENT_KEY_CIRCLE_ID = "circleId";
	
	public static final String INTENT_KEY_CIRCLE_NAME = "circleName";

	/** 返回 **/
	private TextView mBackTv;

	/** 标题 **/
	private TextView mTitleTv;

	/** 圈子图片 **/
	private ImageView mCircleImg;

	/** 圈子名称 **/
	private TextView mCircleNameTv;

	/** 圈子标题 **/
	private TextView mCircleTitleTv;

	/** 今日发布数量 **/
	private TextView mDateCountTv;

	/** 圈子描述 **/
	private TextView mDescTv;

	/** 发布总数量 **/
	private TextView mTalkCountTv;

	/** 秀场列表 **/
	private PullToRefreshListView mShowList;

	/** 秀场列表adapter **/
	private MomentsShowAdapter mMomentsShowAdapter;

	/** 圈子ID **/
	private String circleId;

	/** 圈子Name **/
	private String circleName;
	
	/** 秀场 list **/
	private ArrayList<MomentsShowItem> showList;

	/** 分页： 当前页 **/
	private int pageIndex = 1;

	/** 分页： 每页大小 **/
	private int pageSize = 10;

	/** 分页： 总页数 **/
	private int totalPageNum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_moments_circle);
		
		circleId = getIntent().getStringExtra(INTENT_KEY_CIRCLE_ID);
		circleName = getIntent().getStringExtra(INTENT_KEY_CIRCLE_NAME); 

		initView();

		initData();
	}

	/**
	 * 初始化控件
	 */
	private void initView() {

		// 导航栏
		mBackTv = (TextView) findViewById(R.id.tv_back);
		mTitleTv = (TextView) findViewById(R.id.tv_title);
		mTitleTv.setText(circleName);
		mBackTv.setOnClickListener(this);

		// 头部
		View headView = LayoutInflater.from(this).inflate(R.layout.view_circle_list_header, null);
		mCircleImg = (ImageView) headView.findViewById(R.id.img_circle);
		mCircleNameTv = (TextView) headView.findViewById(R.id.tv_circle_name);
		mCircleTitleTv = (TextView) headView.findViewById(R.id.tv_circle_title);
		mDateCountTv = (TextView) headView.findViewById(R.id.tv_date_count);
		mDescTv = (TextView) headView.findViewById(R.id.tv_desc);
		mTalkCountTv = (TextView) headView.findViewById(R.id.tv_talk_count);

		// 秀场列表
		mShowList = (PullToRefreshListView) findViewById(R.id.lv_moments_show);
		mShowList.setMode(Mode.BOTH);
		mShowList.getLoadingLayoutProxy().setLastUpdatedLabel(Utils.getCurrTiem());
		mShowList.getLoadingLayoutProxy().setPullLabel("往下拉更新数据...");
		mShowList.getLoadingLayoutProxy().setRefreshingLabel("正在载入中...");
		mShowList.getLoadingLayoutProxy().setReleaseLabel("放开更新数据...");
		mShowList.setOnRefreshListener(this);

		mShowList.getRefreshableView().addHeaderView(headView);

	}

	/**
	 * 初始化数据
	 */
	private void initData() {

		showList = new ArrayList<MomentsShowItem>();
		pageIndex = 1;

		requestCircleDetail(circleId, pageIndex, pageSize);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// 返回
		case R.id.tv_back:
			AppManager.getAppManager().finishActivity();
			break;

		default:
			break;
		}
	}

	/**
	 * 下拉刷新
	 * 
	 * @param refreshView
	 */
	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
		Log.d(TAG, "onRefresh");

		initData();
	}

	/**
	 * 上拉加载更多
	 * 
	 * @param refreshView
	 */
	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		Log.d(TAG, "onLodeMore");
		pageIndex++;
		if (pageIndex <= totalPageNum) {
			requestCircleDetail(circleId, pageIndex, pageSize);
		} else {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					mShowList.onRefreshComplete();
				}
			}, 100);
		}
	}

	/**
	 * 请求圈子详情，当前圈子下的秀场列表
	 * 
	 * @param circleId
	 * @param pageIndex
	 * @param pageSize
	 */
	private void requestCircleDetail(String circleId, int pageIndex, int pageSize) {
		RequstClient.circleDetail(circleId, pageIndex, pageSize, new CustomResponseHandler(this) {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);

				CircleDetailBean bean = CircleDetailBean.explainJson(content, MomentsCircleActivity.this);

				// 成功
				if (bean.type == 1) {
					totalPageNum = bean.getTotalPageNum();
					try {
						refreshCircleData(bean);
					}catch(Exception e) {
						e.printStackTrace();
					}
				
				}
				// 失败
				else {
					UIHelper.showToast(bean.msg);
				}
			}
		});
	}

	/**
	 * 刷新圈子数据
	 * 
	 * @param bean
	 */
	private void refreshCircleData(CircleDetailBean bean) {

		if(bean.getShowList() != null) {
			showList.addAll(bean.getShowList());
		}

		if (pageIndex == 1) {
			setHeaderData(bean.getCircleMap());
			setAdapter();
		}

		mShowList.onRefreshComplete();
	}

	/**
	 * 设置头部信息
	 * 
	 * @param data
	 */
	private void setHeaderData(CircleMap data) {
		ImageManager.Load(data.getIMG(), mCircleImg, ImageManager.squareOptions);
		mCircleNameTv.setText(data.getNAME());
		mCircleTitleTv.setText(data.getNAME());
		mDateCountTv.setText(data.getCOUNT_TODAY());
		mDateCountTv.setText(String.format(getString(R.string.moments_circle_count_today), data.getCOUNT_TODAY()));
		mDescTv.setText(data.getDESCRIPTION());
		mTalkCountTv.setText(data.getCOUNT_ALL());
	}
	
	/**
	 * 设置列表adapter
	 */
	private void setAdapter() {
		mMomentsShowAdapter = new MomentsShowAdapter(this, showList);
		mShowList.getRefreshableView().setAdapter(mMomentsShowAdapter);
	}
}
