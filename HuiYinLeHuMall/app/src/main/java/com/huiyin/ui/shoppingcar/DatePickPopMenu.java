package com.huiyin.ui.shoppingcar;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.wight.wheel.OnWheelChangedListener;
import com.huiyin.wight.wheel.OnWheelScrollListener;
import com.huiyin.wight.wheel.WheelView;
import com.huiyin.wight.wheel.adapters.AbstractWheelTextAdapter;
import com.huiyin.wight.wheel.adapters.ArrayWheelAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

public class DatePickPopMenu extends PopupWindow {

    private TextView text;
    private Button confirm;
    private WheelView day, hour;
    private View view;
    private Context mContext;
    private ConfirmClick mClickListener;
    private boolean scrolling = false;
    private OnWheelScrollListener mOnWheelScrollListener;
    private OnWheelChangedListener mChangedListener;
    private DayArrayAdapter mDayArrayAdapter;

    private String[] timeLc = {"8:00-11:00", "12:00-16:00", "17:00-20:00"};  //配送时间段
    private Time time;

    public DatePickPopMenu(Context mContext) {
        super(mContext);
        this.mContext = mContext;
        findView();
        initView();
        setlistener();
    }

    private void findView() {
        view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                R.layout.date_time_pick_popmenu, null);
        text = (TextView) view.findViewById(R.id.time);
        confirm = (Button) view.findViewById(R.id.confirm);
        day = (WheelView) view.findViewById(R.id.wheelView1);
        hour = (WheelView) view.findViewById(R.id.wheelView2);
    }

    private void initView() {

        Calendar calendar = Calendar.getInstance(Locale.CHINESE);

        //超过当天下午4点就只能选择明天的送货
        time = new Time();
        time.setToNow();
        if (time.hour >= 16) {
            calendar.roll(Calendar.DAY_OF_YEAR, 1);
        }

        ArrayWheelAdapter<String> ampmAdapter = new ArrayWheelAdapter<String>(mContext, getTheTimeL());
        ampmAdapter.setItemResource(R.layout.wheel_text_item);
        ampmAdapter.setItemTextResource(R.id.text);
        hour.setViewAdapter(ampmAdapter);

        mDayArrayAdapter = new DayArrayAdapter(mContext, calendar);
        day.setViewAdapter(mDayArrayAdapter);

        String temp;
        int select = day.getCurrentItem();
        temp = mDayArrayAdapter.getItemTextS(select);
        temp += ampmAdapter.getItemText(hour.getCurrentItem());
        text.setText(temp);

        setContentView(view);
        setWidth(LayoutParams.MATCH_PARENT);
        setHeight(LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        setAnimationStyle(R.style.PopupAnimation);
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        setBackgroundDrawable(dw);
        setOutsideTouchable(true);
        update();
    }

    private void setlistener() {

        day.addScrollingListener(new OnWheelScrollListener() {
            @Override
            public void onScrollingStarted(WheelView wheel) {
                scrolling = true;
            }

            @Override
            public void onScrollingFinished(WheelView wheel) {
                scrolling = false;

                if (time == null) {
                    time = new Time();
                    time.setToNow();
                }

                int select = day.getCurrentItem();
                ArrayWheelAdapter<String> ampmAdapter;
                if (select == 0 && time.hour < 16) {
                    ampmAdapter = new ArrayWheelAdapter<String>(mContext, getTheTimeL());
                    ampmAdapter.setItemResource(R.layout.wheel_text_item);
                    ampmAdapter.setItemTextResource(R.id.text);
                    hour.setViewAdapter(ampmAdapter);
                } else {
                    ampmAdapter = new ArrayWheelAdapter<String>(mContext, timeLc);
                    ampmAdapter.setItemResource(R.layout.wheel_text_item);
                    ampmAdapter.setItemTextResource(R.id.text);
                    hour.setViewAdapter(ampmAdapter);
                }

                String temp;
                temp = mDayArrayAdapter.getItemTextS(select);
                temp += ampmAdapter.getItemText(hour.getCurrentItem());
                text.setText(temp);
            }
        });

        day.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                if (!scrolling) {
                    if (time == null) {
                        time = new Time();
                        time.setToNow();
                    }

                    int select = day.getCurrentItem();
                    ArrayWheelAdapter<String> ampmAdapter;
                    if (select == 0 && time.hour < 16) {
                        ampmAdapter = new ArrayWheelAdapter<String>(mContext, getTheTimeL());
                        ampmAdapter.setItemResource(R.layout.wheel_text_item);
                        ampmAdapter.setItemTextResource(R.id.text);
                        hour.setViewAdapter(ampmAdapter);
                    } else {
                        ampmAdapter = new ArrayWheelAdapter<String>(mContext, timeLc);
                        ampmAdapter.setItemResource(R.layout.wheel_text_item);
                        ampmAdapter.setItemTextResource(R.id.text);
                        hour.setViewAdapter(ampmAdapter);
                    }

                    String temp;
                    temp = mDayArrayAdapter.getItemTextS(select);
                    temp += ampmAdapter.getItemText(hour.getCurrentItem());
                    text.setText(temp);
                }
            }
        });

        mOnWheelScrollListener = new OnWheelScrollListener() {

            @Override
            public void onScrollingStarted(WheelView wheel) {
                scrolling = true;
            }

            @Override
            public void onScrollingFinished(WheelView wheel) {
                scrolling = false;
                String temp;
                int select = day.getCurrentItem();
                temp = mDayArrayAdapter.getItemTextS(select);
                temp += ((ArrayWheelAdapter<String>)hour.getViewAdapter()).getItemText(hour.getCurrentItem());
                text.setText(temp);
            }
        };

        mChangedListener = new OnWheelChangedListener() {

            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                if (!scrolling) {
                    String temp;
                    int select = day.getCurrentItem();
                    temp = mDayArrayAdapter.getItemTextS(select);
                    temp += ((ArrayWheelAdapter<String>)hour.getViewAdapter()).getItemText(hour.getCurrentItem());
                    text.setText(temp);
                }
            }
        };

        hour.addScrollingListener(mOnWheelScrollListener);
        hour.addChangingListener(mChangedListener);

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    String s = text.getText().toString();
                    mClickListener.OnConfirmClick(s.replaceAll("[\\(][\\s\\S]*[\\)]", " "));
                }
                dismiss();
            }
        });
    }

    public void setmClickListener(ConfirmClick mClickListener) {
        this.mClickListener = mClickListener;
    }

    /*
        1、配送时间：8点-11点；会员只能在前一天16点-当日8点范围内选择
        2、配送时间：12点-16点；会员只能在当日8点-12点范围内选择
        3、配送时间：17点-20点；会员只能在当日12点-16点范围内选择
    */
    private String[] getTheTimeL() {
        if (time == null) {
            time = new Time();
            time.setToNow();
        }
        if (time.hour < 8) {
            return new String[]{"8:00-11:00", "12:00-16:00", "17:00-20:00"};
        } else if (time.hour < 12) {
            return new String[]{"12:00-16:00", "17:00-20:00"};
        } else if (time.hour < 16) {
            return new String[]{"17:00-20:00"};
        } else {
            return new String[]{"8:00-11:00", "12:00-16:00", "17:00-20:00"};
        }
    }

    public interface ConfirmClick {
        public void OnConfirmClick(String temp);
    }

    /**
     * Day adapter
     */
    private class DayArrayAdapter extends AbstractWheelTextAdapter {
        // Count of days to be shown
        private final int daysCount = 7;

        // Calendar
        Calendar calendar;

        /**
         * Constructor
         */
        protected DayArrayAdapter(Context context, Calendar calendar) {
            super(context, R.layout.wheel_text_item, NO_RESOURCE);
            this.calendar = calendar;

            setItemTextResource(R.id.text);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            int day = index;
            Calendar newCalendar = (Calendar) calendar.clone();
            newCalendar.roll(Calendar.DAY_OF_YEAR, day);

            View view = super.getItem(index, cachedView, parent);
            TextView text = (TextView) view.findViewById(R.id.text);
            String temp = "";
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
            temp += format.format(newCalendar.getTime());
            DateFormat format1 = new SimpleDateFormat("(EEEE)", Locale.CHINA);
            temp += format1.format(newCalendar.getTime());
            text.setText(temp);
            // TextView weekday = (TextView)
            // view.findViewById(R.id.time2_weekday);
            // if (day == 0) {
            // weekday.setText("");
            // } else {
            // DateFormat format = new SimpleDateFormat("(EEEE)");
            // weekday.setText(format.format(newCalendar.getTime()));
            // }
            //
            // TextView monthday = (TextView)
            // view.findViewById(R.id.time2_monthday);
            // if (day == 0) {
            // monthday.setText("Today");
            // monthday.setTextColor(0xFF0000F0);
            // } else {
            // DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            // monthday.setText(format.format(newCalendar.getTime()));
            // monthday.setTextColor(0xFF111111);
            // }

            return view;
        }

        @Override
        public int getItemsCount() {
            return daysCount;
        }

        @Override
        protected CharSequence getItemText(int index) {
            return "";
        }

        public String getItemTextS(int index) {
            int day = index;
            Calendar newCalendar = (Calendar) calendar.clone();
            newCalendar.roll(Calendar.DAY_OF_YEAR, day);
            String temp = "";
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
            temp += format.format(newCalendar.getTime());
            DateFormat format1 = new SimpleDateFormat("(EEEE)", Locale.CHINA);
            temp += format1.format(newCalendar.getTime());
            text.setText(temp);
            return temp;
        }
    }

}
