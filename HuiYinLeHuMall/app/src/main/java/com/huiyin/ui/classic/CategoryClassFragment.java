package com.huiyin.ui.classic;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.huiyin.R;
import com.huiyin.adapter.CategoryClassAdapter;
import com.huiyin.bean.CategoryAndBrandBean.Category;
/**
 * 分类-分类fragment
 * @author zhyao
 *
 */
public class CategoryClassFragment extends Fragment {
	
	private static final String TAG = "CategoryClassFragment";
	
	private View rootView;
	
	private ListView mCategoryList;
	
	private CategoryClassAdapter mAdapter;
	
	private ArrayList<Category> mCategories;
	
	public static CategoryClassFragment newInstance(ArrayList<Category> categories) {
		CategoryClassFragment instance = new CategoryClassFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("data", categories);
		instance.setArguments(bundle);
		return instance;
	}
	
	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
		
		mCategories = (ArrayList<Category>) getArguments().getSerializable("data");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.i(TAG, "onCreateView");
		rootView = inflater.inflate(R.layout.fragment_category_classisc, null);
		initView();
		setData(mCategories);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, "onResume");

	}

	private void initView() {
		mCategoryList = (ListView) rootView.findViewById(R.id.lv_category_classisc);
	}
	
	public void setData(ArrayList<Category> categories) {
		//if(mAdapter == null) {
			mAdapter = new CategoryClassAdapter(getActivity(), categories);
			mCategoryList.setAdapter(mAdapter);
		//}
	}

}
