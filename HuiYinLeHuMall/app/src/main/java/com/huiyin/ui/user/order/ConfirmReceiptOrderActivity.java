package com.huiyin.ui.user.order;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.adapter.FragmentViewPagerAdapter;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.GoodLikeBean;
import com.huiyin.bean.GoodLikeBeanList;
import com.huiyin.bean.GoodList;
import com.huiyin.ui.MainActivity;
import com.huiyin.ui.user.GoodsLikeFragment;
import com.huiyin.utils.AppManager;
import com.huiyin.utils.JSONParseUtils;

/**
 * 确认收货-交易成功
 * 
 * @author zhyao
 * 
 */
public class ConfirmReceiptOrderActivity extends BaseActivity implements OnClickListener {

	/**
	 * 标题
	 */
	private TextView mTitleTv;
	
	/**
	 * 立即评价
	 */
	private Button mAppariseBtn;

	/**
	 * 继续购物
	 */
	private Button mShoppingBtn;

	/**
	 * 猜你喜欢
	 */
	private ViewPager mebyLikeViewPager;

	/**
	 * 商品
	 */
	private GoodList orderItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_comfirm_receipt_order);

		orderItem = (GoodList) getIntent().getSerializableExtra(OrderCommentActivity.ORDER_ITEM);
	}

	@Override
	protected void onFindViews() {
		super.onFindViews();
		
		mTitleTv = (TextView) findViewById(R.id.middle_title_tv);
		mTitleTv.setText("交易成功");

		mAppariseBtn = (Button) findViewById(R.id.btn_appraise);
		mShoppingBtn = (Button) findViewById(R.id.btn_shopping);
		mebyLikeViewPager = (ViewPager) findViewById(R.id.mebyLikeViewPager);

	}

	@Override
	protected void onSetListener() {
		super.onSetListener();

		mAppariseBtn.setOnClickListener(this);
		mShoppingBtn.setOnClickListener(this);
	}

	@Override
	protected void onLoadData() {
		super.onLoadData();

		loadMayBeLikeByUser();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		//立即评价
		case R.id.btn_appraise:
			Intent intent = new Intent(ConfirmReceiptOrderActivity.this, OrderCommentActivity.class);
			intent.putExtra(OrderCommentActivity.ORDER_ITEM, orderItem);
			startActivity(intent);
			AppManager.getAppManager().finishActivity();
			break;
		//继续购物
		case R.id.btn_shopping:
			//指定跳转到首页
			AppContext.MAIN_TASK = AppContext.FIRST_PAGE;
			
			Intent i = new Intent();
			i.setClass(ConfirmReceiptOrderActivity.this, MainActivity.class);
			startActivity(i);
			AppManager.getAppManager().finishActivity();
			break;

		default:
			break;
		}
	}

	/**
	 * 获取猜你喜欢商品
	 */
	private void loadMayBeLikeByUser() {
		if (appIsLogin(false)) {

			// 获取我的信息
			RequstClient.mayBeLikeByUser(new CustomResponseHandler(mContext, false) {
				@Override
				public void onSuccess(int statusCode, Header[] headers, String content) {
					super.onSuccess(statusCode, headers, content);
					if (!appIsLogin(false)) {
						// 用户已经注销，数据请求又回来了，这里不做处理，以解决用户注销失败的问题
						return;
					}

					// 异常消息显示
					if (JSONParseUtils.isErrorJSONResult(content)) {
						String msg = JSONParseUtils.getString(content, "msg");
						showMyToast(msg);
						return;
					}

					// 解析猜你喜欢数据
					List<GoodLikeBean> maybeLike = JSONParseUtils.parseMaybeLike(content);// 登录之后系统当前时间
					if (null != maybeLike && maybeLike.size() > 0) {
						showLikeList(maybeLike);
					}

				}

			});
		}
	}

	/**
	 * 显示猜你喜欢数据
	 */
	private void showLikeList(List<GoodLikeBean> maybeLike) {

		// 没有数据
		if (null == maybeLike || 0 == maybeLike.size()) {
			mebyLikeViewPager.setVisibility(View.GONE);
			return;
		} else {
			mebyLikeViewPager.setVisibility(View.VISIBLE);
		}

		List<GoodLikeBean> dataList = new ArrayList<GoodLikeBean>();
		List<Fragment> listFragments = new ArrayList<Fragment>();

		for (int i = 0; i < maybeLike.size(); i++) {
			int yu = i % 6;
			if (((yu == 0) || i == (maybeLike.size() - 1)) && (i != 0)) {
				// 6的倍数 || 是最后一个 && 不是第一个 （每六条数据显示一屏）
				GoodsLikeFragment fragment = GoodsLikeFragment.getInstance(new GoodLikeBeanList(dataList));
				listFragments.add(fragment);

				dataList = new ArrayList<GoodLikeBean>();
			}
			dataList.add(maybeLike.get(i));
		}

		FragmentViewPagerAdapter adapter = new FragmentViewPagerAdapter(getSupportFragmentManager(), listFragments);
		mebyLikeViewPager.setAdapter(adapter);
	}

}
