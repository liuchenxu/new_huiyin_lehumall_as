package com.huiyin.ui.shoppingcar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.grong.hyin.sdk.recharge.RechargeActivity;
import com.huiyin.R;
import com.huiyin.base.BaseActivity;
import com.huiyin.utils.AppManager;

/**
 * 共融直播支付结果展示页面
 * @author zhyao
 *
 */
public class PaymentGRLiveResultActivity extends BaseActivity implements OnClickListener {

	private static final String TAG = "PaymentGRLiveResultActivity";
	
	/**订单编号**/
	public static final String INTENT_KEY_ORDER_NUM = "order_num";
	
	/**支付结果**/
	public static final String INTENT_KEY_PAYMENT_RESULT = "payment_result";
	
	/**支付方式**/
	public static final String INTENT_KEY_PAY_WAY = "payway";
	
	/**支付成功**/
	public static final int PAYMENT_SUCCESS = 1;
	
	/**支付失败**/
	public static final int PAYMENT_FAIL = 0;

	/**倒计时**/
	private static final int HANDLER_TIME_FINISH = 1;
	
	/**倒计时1s**/
	private static final int HANDLER_TIME_DURATION = 1000;
	
	/**支付结果**/
	private int mPaymentResult;
	
	/**支付方式**/
	private int mPayWay;

	/**倒计时10s**/
	private int count = 10;
	
	/**订单编号**/
	private String mOrderNum; 

	/**导航标题**/
	private TextView mTitleTv;
	
	/**重新支付**/
	private TextView mRepayTv;
	
	/**返回直播**/
	private TextView mBackLiveTv;
	
	/**支付结果**/
	private TextView mPayTipTitleTv;
	
	/**支付结果详情**/
	private TextView mPayTipDetailTv;
	
	/**订单编号**/
	private TextView mOrderNumTv;
	
	/**倒计时10s提示**/
	private TextView mCountDownTv;
	
	/**支付渠道图片**/
	private ImageView mPayWayImg;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pay_result_gr_live);

		initData();

		initView();
		
		showPaymentInfo();
		
	}
	
	/**
	 * 获取intent传递的数据
	 */
	private void initData() {
		mPaymentResult = getIntent().getIntExtra(INTENT_KEY_PAYMENT_RESULT, PAYMENT_FAIL);
		mPayWay = getIntent().getIntExtra(INTENT_KEY_PAY_WAY, -1);
		mOrderNum = getIntent().getStringExtra(INTENT_KEY_ORDER_NUM);
	}

	/**
	 * 初始化组件
	 */
	private void initView() {
		mTitleTv = (TextView) findViewById(R.id.title);
		mRepayTv = (TextView) findViewById(R.id.pay_repay);
		mBackLiveTv = (TextView) findViewById(R.id.pay_back_live);
		mPayTipTitleTv = (TextView) findViewById(R.id.tv_pay_tip1);
		mPayTipDetailTv = (TextView) findViewById(R.id.tv_pay_tip2);
		mOrderNumTv = (TextView) findViewById(R.id.tv_pay_orderid);
		mCountDownTv = (TextView) findViewById(R.id.tv_countdown);
		mPayWayImg = (ImageView) findViewById(R.id.iv_pay_image);
		
		mRepayTv.setOnClickListener(this);
		mBackLiveTv.setOnClickListener(this);
		
		mHandler.sendEmptyMessageDelayed(HANDLER_TIME_FINISH, HANDLER_TIME_DURATION);
	}
	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		// 重新支付
		case R.id.pay_repay:
			AppManager.getAppManager().finishActivity();
			break;
		// 返回直播
		case R.id.pay_back_live:
			backGRLive();
			break;
		default :
			break;
		}
	}
	
	/**
	 * 支付结果展示
	 */
	private void showPaymentInfo() {
		
		String paywayName = "";
		int paywayResId = -1;
		
		//支付方式
		switch (mPayWay) {
		//便民服务卡
		case CommitOrderActivityNew.PAYWAY_SERVICE_CARD:
			paywayName = "便民服务卡";
			paywayResId = R.drawable.pay_service_card;
			break;
		//支付宝
		case CommitOrderActivityNew.PAYWAY_ALIPAY:
			paywayName = "支付宝";
			paywayResId = R.drawable.pay_alipay;
			break;
		//微信
		case CommitOrderActivityNew.PAYWAY_WX:
			paywayName = "微信";
			paywayResId = R.drawable.pay_weixin;
			break;
		//银联
		case CommitOrderActivityNew.PAYWAY_YL:
			paywayName = "银联";
			paywayResId = R.drawable.pay_up;
			break;
		default:
			break;
		}
		
		//支付成功
		if(mPaymentResult == PAYMENT_SUCCESS) 
		{
			mTitleTv.setText("支付成功");
			mPayTipTitleTv.setText("支付成功，谢谢惠顾！");
			mPayTipDetailTv.setText("您已成功使用" + paywayName + "支付完成交易");
			mRepayTv.setVisibility(View.GONE);
		}
		//支付失败
		else if(mPaymentResult == PAYMENT_FAIL)
		{
			mTitleTv.setText("支付失败");
			mPayTipTitleTv.setText("支付失败！");
			mPayTipDetailTv.setText("您使用" + paywayName + "支付交易失败");
			mRepayTv.setVisibility(View.VISIBLE);
		}
		
		mOrderNumTv.setText("订单编号：" + mOrderNum);
		mPayWayImg.setImageResource(paywayResId);
		
	}
	

	/**
	 * 10s倒计时，返回直播
	 */
	@SuppressLint("HandlerLeak") 
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case HANDLER_TIME_FINISH:
				if (count > 0) {
					count--;
					mHandler.sendEmptyMessageDelayed(HANDLER_TIME_FINISH, HANDLER_TIME_DURATION);
					mCountDownTv.setText(count + "s后将自动返回");
				} else {
					backGRLive();
				}
				break;
			default:
				break;
			}
		}
	};

	@Override
	protected void onDestroy() {
		mHandler.removeMessages(HANDLER_TIME_FINISH);
		super.onDestroy();
	};
	
	private void backGRLive() {
		//销毁选择支付方式页面
		AppManager.getAppManager().finishActivity(CommitOrderActivityNew.class);
		//AppManager.getAppManager().finishActivity();
		Intent intent = new Intent(this, RechargeActivity.class);
		intent.putExtra("payResult", mPaymentResult == 1 ? true : false);
		startActivity(intent);
	}
}