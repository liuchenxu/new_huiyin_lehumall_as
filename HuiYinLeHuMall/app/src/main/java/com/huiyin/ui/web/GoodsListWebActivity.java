package com.huiyin.ui.web;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.content.Intent;
import android.os.Bundle;

import com.huiyin.R;
import com.huiyin.api.URLs;
import com.huiyin.base.BaseActivity;
import com.huiyin.utils.StringUtils;

/**
 * 	商品列表Html5版本
 * 
 * @author zhyao
 * 
 */
public class GoodsListWebActivity extends BaseActivity {
	
	public static final String BUNDLE_KEY_CATEGORY_ID = "category_id";
	public static final String BUNDLE_KEY_WORD = "key_word";
	public static final String BUNDLE_MARK = "mark";
	public static final String BUNDLE_BRAND_ID = "BRAND_ID";
	public static final String BUNDLE_PROPERTY_ID = "propertyId";
	public static final String BUNDLE_STORE_ID = "storeId";
	//add by zhyao @2016/3/3 添加国家馆搜索参数
	public static final String BUNDLE_ORGIN_ID = "originIds";

	private CustomWebView mCustomWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fragment_home_web);

		mCustomWebView = (CustomWebView) findViewById(R.id.webview);
//		mCustomWebView.setOnRefreshEnable(false);
		mCustomWebView.setOnRefreshEnable(true);
		mCustomWebView.set404Page(true);
		try {
			loadData(getIntent());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
		try {
			loadData(intent);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onStop() {
		super.onStop();

	}

	private void loadData(Intent intent) throws UnsupportedEncodingException {
		
		String categoryId = intent.getStringExtra(BUNDLE_KEY_CATEGORY_ID);
		String keyword = intent.getStringExtra(BUNDLE_KEY_WORD);
		String mark = intent.getStringExtra(BUNDLE_MARK);
		String brandIds = intent.getStringExtra(BUNDLE_BRAND_ID);
		String attributeIds = intent.getStringExtra(BUNDLE_PROPERTY_ID);
		String storeId = intent.getStringExtra(BUNDLE_STORE_ID);
		String originIds = intent.getStringExtra(BUNDLE_ORGIN_ID);
		
		StringBuffer params = new StringBuffer();
		params.append("?");
		if(!StringUtils.isEmpty(categoryId)) {
			params.append("categoryId=").append(categoryId).append("&");
		}
		if(!StringUtils.isEmpty(keyword)) {
			params.append("keyword=").append(URLEncoder.encode(keyword, "UTF-8")).append("&");
		}
		if(!StringUtils.isEmpty(brandIds)) {
			params.append("brandIds=").append(brandIds).append("&");
		}
		if(!StringUtils.isEmpty(attributeIds)) {
			params.append("attributeIds=").append(attributeIds).append("&");
		}
		if(!StringUtils.isEmpty(storeId)) {
			params.append("storeId=").append(storeId).append("&");
		}
		if(!StringUtils.isEmpty(originIds)) {
			params.append("originIds=").append(originIds).append("&");
		}
			
		params.append("mark=").append(mark);
			
		try {
			mCustomWebView.loadUrl(URLs.goods_list_web + params.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
