package com.huiyin.ui.web;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.base.BaseActivity;
import com.huiyin.newpackage.utils.UserInfoUtils;
import com.huiyin.utils.UpdateVersionTool;

import de.greenrobot.event.EventBus;

/**
 * 通用的Webview页面 html5页面中location.href没有特殊处理的话跳转到CustomeWebActivity
 * 
 * @author zhyao
 * 
 */
public class CustomeWebActivity extends BaseActivity {

	public static final String INTENT_KEY_URL = "url";

	private CustomWebView mCustomWebView;

	public static void startActivity(Context contxt, String url) {
		Intent intent = new Intent(contxt, CustomeWebActivity.class);
		intent.putExtra(INTENT_KEY_URL, url);
		contxt.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.fragment_home_web);
		EventBus.getDefault().register(this);
		mCustomWebView = (CustomWebView) findViewById(R.id.webview);
		mCustomWebView.setOnRefreshEnable(false);
		mCustomWebView.set404Page(true);
//		loadData();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		loadData();
	}

	private void loadData() {

		try {
			String url = getIntent().getStringExtra(INTENT_KEY_URL);
			if(UserInfoUtils.IsUserLogin()&&(!url.contains("userid="))){
				if(url.contains("?")&&!url.endsWith("?")){
					url = url + "&userid="+AppContext.userId;
				}
				else if(url.contains("?")&&url.endsWith("?")){
					url=url+"userid="+AppContext.userId;
				}
				else{
					url=url+"?userid="+AppContext.userId;
				}
			}
			//处理url中appVersion
			if(!url.contains("version=")){
				String versionCode= UpdateVersionTool.getVersionCode(this)+"";
				if(url.contains("?")&&!url.endsWith("?")){
					url = url + "&version="+versionCode;
				}
				else if(url.contains("?")&&url.endsWith("?")){
					url=url+"version="+versionCode;
				}
				else{
					url=url+"?version="+versionCode;
				}
			}
			mCustomWebView.loadUrl(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	protected void onDestroy() {
		EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
	
	public void onEvent(String url){
		Log.i("OnEvent", url);
	}

}
