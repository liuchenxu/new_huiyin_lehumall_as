package com.huiyin.ui.home;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.api.URLs;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.AddressItem;
import com.huiyin.js.JsInterface;
import com.huiyin.js.JsInterface.BackBtnClickListener;
import com.huiyin.js.JsInterface.WebViewClientClickListener;
import com.huiyin.ui.classic.ProductsDetailActivity;
import com.huiyin.ui.user.AddressManagementActivity;
import com.huiyin.utils.FileUtils;
import com.huiyin.utils.LogUtil;
import com.orhanobut.logger.Logger;
/**
 * 申请免费试用
 * @author zhyao
 *
 */
public class TryApplyActivity extends BaseActivity {

	protected static final String TAG = "tryApplyActivity";
	
	public static String TRY_TYPE = "type";
	public static int TRY_ACTITY = 0;
	public static int TRY_MY = 1;

	private WebView mWebView;

	JsInterface jsInterface = new JsInterface();
	
	public static final int FILECHOOSER_RESULTCODE = 1;
	/**
	 * 照相
	 */
	private static final int REQ_CAMERA = FILECHOOSER_RESULTCODE+1;
	
	/**
	 * 相册
	 */
	private static final int REQ_CHOOSE = REQ_CAMERA+1;
	
	/**
	 * 图片回调地址
	 */
	private ValueCallback<Uri> mUploadMessage;
	
	/**
	 * 相册图片保存路径
	 */
	private String compressPath = "";
	
	/**
	 * 拍照图片保存路径
	 */
	
	private String imagePaths;
	
	/**
	 * 拍照图片Uri
	 */
	private Uri cameraUri;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_try_apply);

		initViews();

		loadWebData();
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN) 
	public void initViews() {

		mWebView = (WebView) findViewById(R.id.webView);
		WebSettings webSettings = mWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
		webSettings.setAllowFileAccess(true);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			webSettings.setAllowFileAccessFromFileURLs(true);
		}
	
		mWebView.addJavascriptInterface(jsInterface, "JSInterface");
		mWebView.setWebChromeClient(new MyWebChromeClient());
		mWebView.setWebViewClient(new WebViewClient() {
			// 这个方法在用户试图点开页面上的某个链接时被调用
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url != null) {
					// 如果想继续加载目标页面则调用下面的语句
					view.loadUrl(url);
					// 如果不想那url就是目标网址，如果想获取目标网页的内容那你可以用HTTP的API把网页扒下来。
				}
				// 返回true表示停留在本WebView（不跳转到系统的浏览器）
				return true;
			}
		});
		jsInterface.setBackBtnClickListener(new BackBtnClickListener() {
			
			@Override
			public void backEnvent() {
				TryApplyActivity.this.finish();
			}
		});
		jsInterface.setWebViewClientClickListener(new WebViewClientClickListener() {
			
			@Override
			public void wvHasClickEnvent(String action, String args) {
				LogUtil.d(TAG, "wvHasClickEnvent : action = " + action);
				LogUtil.d(TAG, "wvHasClickEnvent : args = " + args);
				JSONArray jsonArray;
				JSONObject jsonObject;
				Intent intent;
				if("startGoodDetail".equals(action)) {
					try {
						jsonArray = new JSONArray(args);
						jsonObject = (JSONObject) jsonArray.get(0);
						String className = jsonObject.getString("class");
						String storeId = jsonObject.getString("storeId");
						String goodsNo = jsonObject.getString("goodsNo");
						String goodsId = jsonObject.getString("goodsId");
						intent = new Intent(TryApplyActivity.this, Class.forName(className));
						intent.putExtra(ProductsDetailActivity.STORE_ID, storeId);
						intent.putExtra(ProductsDetailActivity.GOODS_NO, goodsNo);
						intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID, goodsId);
						
						startActivity(intent);
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
				else if("modifyAddress".equals(action)) {
					Intent i = new Intent();
	                i.setClass(TryApplyActivity.this, AddressManagementActivity.class);
	                i.putExtra(AddressManagementActivity.TAG,
	                        AddressManagementActivity.tryApply);
	                startActivityForResult(i, 0);
				}
			}
		});
	}

	public void loadWebData() {
		int type = getIntent().getIntExtra(TRY_TYPE, -1);
		if(type == TRY_ACTITY) {
//			mWebView.loadUrl("file:///android_asset/www/page/tryout/try.html");
//			mWebView.loadUrl("file:///mnt/sdcard/huiyinlehu/www/page/tryout/view/try_activity.html");
//			mWebView.loadUrl("file:///android_asset/www/page/tryout/view/try_apply.html");
//			mWebView.loadUrl("http://192.168.200.109:8080/x5/UI2/hello/www/page/tryout/view/try_activity.html");
			mWebView.loadUrl(URLs.try_actvity);
		}
		else if(type == TRY_MY) {
//			mWebView.loadUrl("file:///android_asset/www/page/tryout/try_my.html");
//			mWebView.loadUrl("file:///mnt/sdcard/huiyinlehu/www/page/tryout/view/try_activity.html#/try_my_list");
//			mWebView.loadUrl("http://192.168.200.109:8080/x5/UI2/hello/www/page/tryout/view/try_activity.html#/try_my_list");
			mWebView.loadUrl(URLs.try_my);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		
		Log.d(TAG, "requestCode = " + requestCode + "  mUploadMessage = " + mUploadMessage);
		
		if(requestCode == 0 && resultCode == RESULT_OK) {
			AddressItem addressItem = (AddressItem)intent.getSerializableExtra("AddressItem");
			Log.d(TAG, "addressItem = " + addressItem);
			//{"address":{"CITYID":"231","NAME":"shenchun","USER_ID":"21353","AREA":"邗江区","ADDRESS":"田林路888号科技绿洲5号楼","TEL_NO":"","REMARK":"","PROVINCEID":"16","PHONE":"15821572570","EX_TEL":"",
			//"ENABLE":"1","TEL":"","PROVINCE":"江苏","ID":"6583","CITY":"扬州","AREAID":"1947","CODE":""}}
			
			StringBuilder result = new StringBuilder();
			if(addressItem != null) {
				result.append("{'address':{")
				.append("'CITYID':'").append(addressItem.CITY_ID).append("',\n")
				.append("'NAME':'").append(addressItem.CONSIGNEE_NAME).append("',\n")
				.append("'USER_ID':'").append(AppContext.userId).append("',\n")
				.append("'AREA':'").append("").append("',\n")
				.append("'ADDRESS':'").append(addressItem.ADDRESS).append("',\n")
				.append("'TEL_NO':'").append("").append("',\n")
				.append("'REMARK':'").append("").append("',\n")
				.append("'PROVINCEID':'").append(addressItem.PROVINCE_ID).append("',\n")
				.append("'PHONE':'").append(addressItem.CONSIGNEE_PHONE).append("',\n")
				.append("'EX_TEL':'").append("").append("',\n")
				.append("'ENABLE':'").append("1").append("',\n")
				.append("'TEL':'").append("").append("',\n")
				.append("'PROVINCE':'").append("").append("',\n")
				.append("'ID':'").append(addressItem.ADDRESSID).append("',\n")
				.append("'CITY':'").append("").append("',\n")
				.append("'AREAID':'").append(addressItem.AREA_ID).append("',\n")
				.append("'CODE':'").append(addressItem.POSTAL_CODE).append("',\n")
				.append("'LEVELADDR':'").append(addressItem.LEVELADDR).append("'}\n")
				.append("}");
			}else {
				result.append("{'address':''}");
			}
			
			Log.d(TAG, "result = " + result.toString());
			Logger.json(result.toString());
			mWebView.loadUrl("javascript:modifyAddressCallback("+result.toString()+")");  
		}
		else {
			if (null != mUploadMessage)
			{
				Uri uri = null;
				if (requestCode == REQ_CAMERA) {
					afterOpenCamera();
					uri = cameraUri;
				} else if (requestCode == REQ_CHOOSE && null != intent) {
					uri = afterChosePic(intent);
				} else {
					uri = null;
				}
				mUploadMessage.onReceiveValue(uri);
				mUploadMessage = null;
				super.onActivityResult(requestCode, resultCode, intent);
			}
		}
		
	}
	
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
//			mWebView.goBack();
//			return true;
//		}
//	    return super.onKeyDown(keyCode, event);
//	}
	
	private class MyWebChromeClient extends WebChromeClient {

		// For Android 3.0+
		public void openFileChooser(ValueCallback<Uri> uploadMsg,
				String acceptType) {
			if (mUploadMessage != null)
				return;
			mUploadMessage = uploadMsg;
			selectImage();
//			//暂时去除拍照,保留相册start
//			if (!checkSDcard())
//				return;
//			chosePic();
//			compressPath = Environment
//					.getExternalStorageDirectory().getPath()
//					+ "/huiyinlehu/temp";
//			new File(compressPath).mkdirs();
//			compressPath = compressPath + File.separator +  (System.currentTimeMillis() + ".jpg");
			//暂时去除拍照，保留相册end
			// Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			// i.addCategory(Intent.CATEGORY_OPENABLE);
			// i.setType("*/*");
			// startActivityForResult( Intent.createChooser( i, "File Chooser"
			// ), FILECHOOSER_RESULTCODE );
		}

		// For Android < 3.0
		public void openFileChooser(ValueCallback<Uri> uploadMsg) {
			openFileChooser(uploadMsg, "");
		}

		// For Android > 4.1.1
		public void openFileChooser(ValueCallback<Uri> uploadMsg,
				String acceptType, String capture) {
			openFileChooser(uploadMsg, acceptType);
		}

	}
	
	/**
	 * 检查SD卡是否存在
	 * 
	 * @return
	 */
	public final boolean checkSDcard() {
		boolean flag = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
		if (!flag) {
			UIHelper.showToast("请插入手机存储卡再使用本功能");
		}
		return flag;
	}

	/**
	 * 选择图片方式:拍照和相册
	 */
	protected final void selectImage() {
		if (!checkSDcard())
			return;
		String[] selectPicTypeStr = { "拍照", "相册" };
		new AlertDialog.Builder(this).setItems(selectPicTypeStr,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						// 相机拍摄
						case 0:
							openCarcme();
							break;
						// 手机相册
						case 1:
							chosePic();
							break;
						default:
							break;
						}
						compressPath = Environment
								.getExternalStorageDirectory().getPath()
								+ "/huiyinlehu/temp";
						new File(compressPath).mkdirs();
						compressPath = compressPath + File.separator +  (System.currentTimeMillis() + ".jpg");
					}
				})
				.setOnCancelListener(new OnCancelListener() {
					
					@Override
					public void onCancel(DialogInterface dialog) {
						if (null == mUploadMessage)
							return;
						mUploadMessage.onReceiveValue(null);
						mUploadMessage = null;
					}
				})
				.show();
	}

	

	/**
	 * 打开照相机
	 */
	private void openCarcme() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		imagePaths = Environment.getExternalStorageDirectory().getPath()
				+ "/huiyinlehu/temp/" + (System.currentTimeMillis() + ".jpg");
		// 必须确保文件夹路径存在，否则拍照后无法完成回调
		File vFile = new File(imagePaths);
		if (!vFile.exists()) {
			File vDirPath = vFile.getParentFile();
			vDirPath.mkdirs();
		} else {
			if (vFile.exists()) {
				vFile.delete();
			}
		}
		cameraUri = Uri.fromFile(vFile);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
		startActivityForResult(intent, REQ_CAMERA);
	}

	/**
	 * 拍照结束后
	 */
	private void afterOpenCamera() {
		File f = new File(imagePaths);
		addImageGallery(f);
//		File newFile = FileUtils.compressFile(f.getPath(), compressPath);
	}

	/** 解决拍照后在相册中找不到的问题 */
	private void addImageGallery(File file) {
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
		getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
	}

	/**
	 * 本地相册选择图片
	 */
	private void chosePic() {
		FileUtils.delFile(compressPath);
//		Intent innerIntent = new Intent(Intent.ACTION_GET_CONTENT); // "android.intent.action.GET_CONTENT"
//		String IMAGE_UNSPECIFIED = "image/*";
//		innerIntent.setType(IMAGE_UNSPECIFIED); // 查看类型
//		Intent wrapperIntent = Intent.createChooser(innerIntent, null);
//		startActivityForResult(wrapperIntent, REQ_CHOOSE);
		
		Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, REQ_CHOOSE);
	}

	/**
	 * 选择照片后结束
	 * 
	 * @param data
	 */
	private Uri afterChosePic(Intent data) {

		// 获取图片的路径：
		String[] proj = { MediaStore.Images.Media.DATA };
		// 好像是android多媒体数据库的封装接口，具体的看Android文档
		@SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(data.getData(), proj, null, null, null);
		if (cursor == null) {
			UIHelper.showToast("上传的图片仅支持png或jpg格式");
			return null;
		}
		// 按我个人理解 这个是获得用户选择的图片的索引值
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		// 将光标移至开头 ，这个很重要，不小心很容易引起越界
		cursor.moveToFirst();
		// 最后根据索引值获取图片路径
		String path = cursor.getString(column_index);
		if (path != null
				&& (path.endsWith(".png") || path.endsWith(".PNG")
						|| path.endsWith(".jpg") || path.endsWith(".JPG"))) {
			File newFile = FileUtils.compressFile(path, compressPath);
			return Uri.fromFile(newFile);
		} else {
			UIHelper.showToast("上传的图片仅支持png或jpg格式");
		}
		return null;
	}


}
