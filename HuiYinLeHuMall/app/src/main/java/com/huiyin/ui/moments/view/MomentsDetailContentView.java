package com.huiyin.ui.moments.view;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.huiyin.ui.classic.PhotoViewActivity;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
import com.jcvideoplayer.JCVideoPlayerStandard;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * 秀场详情展示图片或者视频 view
 * 
 * @author zhyao
 * 
 */
public class MomentsDetailContentView extends LinearLayout implements OnClickListener {

	private Context mContext;
	
	private ArrayList<String> imgUrlList;

	public MomentsDetailContentView(Context context) {
		super(context);

		initView(context);
	}

	public MomentsDetailContentView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		initView(context);
	}

	public MomentsDetailContentView(Context context, AttributeSet attrs) {
		super(context, attrs);

		initView(context);
	}

	private void initView(Context context) {
		setOrientation(VERTICAL);

		mContext = context;
	}

	/**
	 * 设置view数据
	 * 
	 * @param urls
	 *            图片url
	 */
	public void setViewData(String imgUrls) {
		if (!TextUtils.isEmpty(imgUrls)) {
			
			imgUrlList = new ArrayList<String>();
			
			String showImgUrls[] = imgUrls.split(",");
			
			for (int i = 0; i < showImgUrls.length; i++) {
				createImageView(showImgUrls[i], i);
			}
		}
	}

	/**
	 * 设置view数据
	 * 
	 * @param urls
	 *            视频url
	 * @param thumbUrl
	 *            视频略缩图
	 */
	public void setViewData(String videoUrl, String thumbUrl, String title) {
		createVieoView(videoUrl, thumbUrl, title);
	}

	/**
	 * 创建图片
	 * 
	 * @param imgUrl
	 */
	private void createImageView(String imgUrl, int index) {
		
		imgUrlList.add(index, imgUrl);

		ImageView imgItem = new ImageView(mContext);
		LayoutParams lp = new LayoutParams(DensityUtil.getScreenWidth((Activity) mContext),
				DensityUtil.getScreenWidth((Activity) mContext));
		lp.bottomMargin = DensityUtil.dp2px(mContext, 15);
		imgItem.setLayoutParams(lp);
		imgItem.setScaleType(ScaleType.CENTER_CROP);
		imgItem.setTag(index);
		ImageManager.Load(imgUrl, imgItem, ImageManager.squareOptions);
		imgItem.setOnClickListener(this);

		addView(imgItem);
	}

	/**
	 * 创建视频
	 * 
	 * @param videoUrl
	 */
	private void createVieoView(String videoUrl, String thumbUrl, String title) {

		JCVideoPlayerStandard jcVideoPlayerStandard = new JCVideoPlayerStandard(mContext);
		jcVideoPlayerStandard.setUp(videoUrl, title);
		ImageLoader.getInstance().displayImage(thumbUrl, jcVideoPlayerStandard.ivThumb);
		LayoutParams lp = new LayoutParams(DensityUtil.getScreenWidth((Activity) mContext), DensityUtil.dp2px(mContext,
				200));
		lp.bottomMargin = DensityUtil.dp2px(mContext, 15);
		jcVideoPlayerStandard.setLayoutParams(lp);

		addView(jcVideoPlayerStandard);
	}

	@Override
	public void onClick(View v) {
		
		//查看大图
		try {
			Intent intent = new Intent();
			intent.setClass(mContext, PhotoViewActivity.class);
			intent.putStringArrayListExtra(PhotoViewActivity.INTENT_KEY_PHOTO, imgUrlList);
			intent.putExtra(PhotoViewActivity.INTENT_KEY_POSITION, (Integer)v.getTag());
			mContext.startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
