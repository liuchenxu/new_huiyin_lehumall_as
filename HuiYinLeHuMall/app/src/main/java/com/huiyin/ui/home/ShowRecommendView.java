package com.huiyin.ui.home;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.adapter.HotRecommendViewflowAdapter;
import com.huiyin.bean.MomentsShowItem;
import com.huiyin.bean.HotRecommendationBean.RecommendationBean;
import com.huiyin.ui.moments.adapter.MomentsShowAdapter;
import com.huiyin.wight.MyListView;
import com.huiyin.wight.viewflow.CircleFlowIndicator;
import com.huiyin.wight.viewflow.ViewFlow;
import com.huiyin.wight.viewflow.ViewFlow.ViewSwitchListener;
/**
 * 秀场推荐
 * @author zhyao
 *
 */
public class ShowRecommendView extends LinearLayout{
	
	private Context mContext;
	
	private MyListView mShowRecommendList;
	
	private ArrayList<MomentsShowItem> showRecommendList;	
	
	private MomentsShowAdapter mMomentsShowAdapter;

	public ShowRecommendView(Context context) {
		super(context);
		initLayout(context);
	}

	public ShowRecommendView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initLayout(context);
	}

	public ShowRecommendView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initLayout(context);
	}
	

	public ShowRecommendView(Context context, ArrayList<MomentsShowItem> showRecommendList) {
		super(context);
		this.showRecommendList = showRecommendList;
		initLayout(context);
	}
	
	private void initLayout(Context context) {
		mContext = context;
		View view = LayoutInflater.from(mContext).inflate(R.layout.show_recomeend_view, null, false);
		
		mShowRecommendList = (MyListView) view.findViewById(R.id.lv_recommend_show);
		mMomentsShowAdapter = new MomentsShowAdapter(mContext, showRecommendList);
		mShowRecommendList.setAdapter(mMomentsShowAdapter);
		
		addView(view);
	}

}
