package com.huiyin.ui.moments.view;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.MomentsAppraiseListBean.AppraiseItem;
import com.huiyin.ui.moments.adapter.MomentsCommentAdapter;
import com.huiyin.wight.MyListView;

/**
 * 秀场详情评论列表view
 * 
 * @author zhyao
 * 
 */
public class MomentsCommentView extends LinearLayout implements OnItemClickListener {

	private Context mContext;

	private TextView mCommentCountTv;

	private MyListView mCommentList;

	private MomentsCommentAdapter mCommentAdapter;

	private ArrayList<AppraiseItem> appraiseList;

	private CommentItemSelectListener mListener;

	public MomentsCommentView(Context context) {
		super(context);

		initView(context);
		initData();
	}

	public MomentsCommentView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		initView(context);
		initData();
	}

	public MomentsCommentView(Context context, AttributeSet attrs) {
		super(context, attrs);

		initView(context);
		initData();
	}

	private void initView(Context context) {
		mContext = context;

		View view = LayoutInflater.from(mContext).inflate(R.layout.view_moments_detail_comment, null);

		mCommentCountTv = (TextView) view.findViewById(R.id.tv_comment_count);
		mCommentList = (MyListView) view.findViewById(R.id.lv_comment);
		mCommentList.setOnItemClickListener(this);

		addView(view);
	}

	private void initData() {
		appraiseList = new ArrayList<AppraiseItem>();

	}

	private void setAdapter() {
		mCommentAdapter = new MomentsCommentAdapter(mContext, appraiseList);
		mCommentList.setAdapter(mCommentAdapter);
	}

	public void setData(ArrayList<AppraiseItem> appraiseList) {
		this.appraiseList = appraiseList;
		setAdapter();

		mCommentCountTv.setText(String.format(getResources().getString(R.string.moments_comments_count),
				appraiseList.size()));
	}

	public void setCommentItemSelectListener(CommentItemSelectListener listener) {
		mListener = listener;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (mListener != null) {
			mListener.onCommentSelect(appraiseList.get(position));
		}
	}

	public interface CommentItemSelectListener {
		void onCommentSelect(AppraiseItem appraiseItem);
	}
}
