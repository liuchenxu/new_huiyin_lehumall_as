package com.huiyin.ui.home;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UIHelper;
import com.huiyin.adapter.GalleryViewflowAdapter;
import com.huiyin.adapter.LnkToolsGridViewAdapter;
import com.huiyin.adapter.LnkToolsGridViewAdapter.BackItemClickListener;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.api.URLs;
import com.huiyin.bean.ChannelItem;
import com.huiyin.bean.GalleryAd;
import com.huiyin.bean.HomeAdBean;
import com.huiyin.bean.HomeBean;
import com.huiyin.bean.HomeFlashBean;
import com.huiyin.bean.HomePoly;
import com.huiyin.bean.HomeSeckillBean;
import com.huiyin.bean.HomeSeckillListItemBean;
import com.huiyin.bean.HotRecommendationBean;
import com.huiyin.bean.HotRecommendationBean.GoodBean;
import com.huiyin.bean.HotRecommendationBean.RecommendationBean;
import com.huiyin.bean.MomentsShowItem;
import com.huiyin.bean.MomentsShowListBean;
import com.huiyin.bean.NationalPavilionBean;
import com.huiyin.bean.ProductBespeakBean;
import com.huiyin.bean.SalesPromotion;
import com.huiyin.bean.TopList;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.Mode;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshBase.OnRefreshListener2;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshScrollView;
import com.huiyin.common.widget.pulltorefresh.PullToRefreshScrollView.ScrollListener;
import com.huiyin.constants.Constants;
import com.huiyin.db.LnkToolsDao;
import com.huiyin.db.SQLOpearteImpl;
import com.huiyin.ui.MainActivity;
import com.huiyin.ui.classic.ProductsDetailActivity;
import com.huiyin.ui.classic.SaleRankActivity;
import com.huiyin.ui.club.ClubActivity;
import com.huiyin.ui.home.ClassificationView.CassificationViewCoallBack;
import com.huiyin.ui.home.CuXiaoHuodongView.CuXiaoHuodongViewCoallBack;
import com.huiyin.ui.home.prefecture.ZhuanQuActivity;
import com.huiyin.ui.housekeeper.HouseKeeperActivity;
import com.huiyin.ui.lehuvoucher.LehuVoucherActivity;
import com.huiyin.ui.servicecard.BindServiceCard;
import com.huiyin.ui.servicecard.ServiceCardActivity;
import com.huiyin.ui.shark.ShakeActivity;
import com.huiyin.ui.user.KefuCenterActivity;
import com.huiyin.ui.user.LoginActivity;
import com.huiyin.ui.user.MyMessageActivity;
import com.huiyin.ui.user.order.AllOrderActivity;
import com.huiyin.ui.user.order.ApplyBespeakActivity;
import com.huiyin.utils.DateUtil;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
import com.huiyin.utils.JSONParseUtils;
import com.huiyin.utils.MathUtil;
import com.huiyin.utils.NetworkUtils;
import com.huiyin.utils.PreferenceUtil;
import com.huiyin.utils.StringUtils;
import com.huiyin.utils.Utils;
import com.huiyin.wight.PageControlView;
import com.huiyin.wight.ScrollLayout;
import com.huiyin.wight.ScrollLayout.OnScreenChangeListenerDataLoad;
import com.huiyin.wight.viewflow.CircleFlowIndicator;
import com.huiyin.wight.viewflow.ViewFlow;
import com.jcvideoplayer.JCVideoPlayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.zxing.scan.activity.ZxingCodeActivity;

public class HomeFragment extends Fragment implements OnClickListener, OnRefreshListener2, BackItemClickListener {

	//add by zhyao @2016/4/10 添加标题栏悬浮框
	//悬浮框的参数
    private static WindowManager.LayoutParams titleBarLayoutParams;  
    //悬浮框manager
    private WindowManager mWindowManager;  
    //搜索框背景
	private LinearLayout searchBgLayout;
	//悬浮标题栏view
	private View titleBarView;
	//scrollview滑动的距离
	private int mScrollY;
	
	private PullToRefreshScrollView scrollview;
	boolean requestFlag, requestPolyFlag, requestShowFlag;

	// 广告轮换图
	private RelativeLayout layout_gallery;
	private ViewFlow viewFlow;
	private CircleFlowIndicator indic;
	
	//广告轮播适配器，坑爹的跳转，在Adapter里面
	private GalleryViewflowAdapter galleryViewflowAdapter;
	
	//add by zhyao @2016/2/25 添加热点推荐
	private LinearLayout mHotRecommendLayout;
//	private ViewFlow hotRecommendViewFlow;
//	private CircleFlowIndicator hotRecommendIndic;
//	private MomentsViewflowAdapter hotRecommendViewflowAdapter;
	
//	private TextView hot_recommend_tilte_name;
//	private TextView hot_recommend_tilte_desc;
	
	//add by zhyao @2016/3/3 添加国家馆
	private LinearLayout mNationalPavilionLayout;

	private HomeBean homeBean;// 整个主页的实体类

	private List<GalleryAd> listGalleryAds;

	// private GridView mGridView;

	// private LnkToolsGridViewAdapter toolsGridViewAdapter;
	private ScrollLayout mScrollLayout;
	private PageControlView pageControl;
	private List<ChannelItem> listChannelItems;
	private DataLoading dataLoad;

	private ImageView fastImg;
	
	//add by zhyao @2015/12/26 首页布局添加2个广告位
	private ImageView adImg01;
	
	private ImageView adImg02;

	// 销量排行榜
	private ImageView list_top;

	private ImageLoader imageLoader;

	private LinearLayout layout_seckill;

	private LinearLayout layout_cuxiao;

	// private CuXiaoHuodongView layout_prommotion1;
	// private CuXiaoHuodongView layout_prommotion2;
	// private CuXiaoHuodongView layout_prommotion3;
	// private CuXiaoHuodongView layout_prommotion4;

	// add by zhyao @2016/5/6 新增秀场推荐
	private LinearLayout home_show_recommend_linlayout;
	
	// 分类聚合
	private LinearLayout home_classification_linlayout;

	private View ab_search;
	private ImageView activity_index_code_scan;
	
	private RelativeLayout new_msg_layout;
	
	private LnkToolsDao lnkToolsDao;
	View view;
	
	//add by xiatianhang @2015/8/11 新增城市列表（分站点）
	private TextView regionTv;//城市分站点区域(城市)
	private String provinceName;//省
	private String cityName;//市
	private String areaName;//区
	private int provinceid = -1;//省id
	private int cityid = -1;//市id
	private int areaid = -1;//区id
	private SQLOpearteImpl soi;//区域数据库操作类

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (view == null) {
			view = inflater.inflate(R.layout.fragment_home, null);
		}
		// 缓存的rootView需要判断是否已经被加过parent，
		// 如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。
		ViewGroup parent = (ViewGroup) view.getParent();
		if (parent != null) {
			parent.removeView(view);
		}

		requestData();// 请求主页数据

		requestPolyData();// 请求分类聚合的数据
		
		// add by zhyao @2016/5/6 新增秀场推荐
	    requestShowRecommendData(); 

		initData();

		initViews(view);

		//initLocation();//定位城市 区
		
		dataLoad = new DataLoading();

		String indexCache = AppContext.getInstance().getCacheAppindexFirstData();
		if (indexCache != null && !indexCache.equals("")) {
			analyticalHomeData(indexCache, true);
		}
		String polyindexCache = AppContext.getInstance().getCacheAppIndexPolyFirstData();
		if (polyindexCache != null && !polyindexCache.equals("")) {
			analyticalPolyData(polyindexCache, true);
		}

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if(mScrollY >= 10 && AppContext.MAIN_TASK == AppContext.FIRST_PAGE) {
			showTitleBarWindow();
		}
	}
	
    @Override
    public void onPause() {
    	super.onPause();
    	removeTitleBarWindow();
    }
 
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		//add by zhyao @2016/4/10 添加/删除标题的悬浮框
		if(isVisibleToUser) {
			
			if(mScrollY >= 10) {
				showTitleBarWindow();
			}
			else if(mScrollY <= DensityUtil.dip2px(getActivity(), 46)){  
				//解决首页和分类切换，首页莫名其妙往下滑动一段距离的BUG，让scrollview滑动到顶部
				if(scrollview != null) {
					scrollview.getRefreshableView().post(new Runnable() {
						
						@Override
						public void run() {
							scrollview.getRefreshableView().scrollTo(0, 0);
						}
					});
					
				}
				removeTitleBarWindow();  
	        }  
		}
		else {
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					removeTitleBarWindow();
				}
			}, 100);
			
			JCVideoPlayer.releaseAllVideos();
		}
		
	}
	
	private void initData() {
		lnkToolsDao = new LnkToolsDao();

		homeBean = new HomeBean();
		listGalleryAds = new ArrayList<GalleryAd>();
		listChannelItems = new ArrayList<ChannelItem>();

		imageLoader = ImageLoader.getInstance();
	}


	private void initViews(View view) {
		mWindowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);  
		
		//add by zhyao @2016/4/10 添加标题栏悬浮框
		//设置标题栏背景透明
		searchBgLayout = (LinearLayout) view.findViewById(R.id.search_bg_layout);
		searchBgLayout.setAlpha(0);
		
		scrollview = (PullToRefreshScrollView) view.findViewById(R.id.home_body_scrollview);
		scrollview.setMode(Mode.PULL_FROM_START);
		scrollview.getLoadingLayoutProxy().setLastUpdatedLabel(Utils.getCurrTiem());
		scrollview.getLoadingLayoutProxy().setPullLabel("往下拉更新数据...");
		scrollview.getLoadingLayoutProxy().setRefreshingLabel("正在载入中...");
		scrollview.getLoadingLayoutProxy().setReleaseLabel("放开更新数据...");

		// 下拉刷新数据
		scrollview.setOnRefreshListener(this);
		// 滑动监听
		scrollview.setOnScrollListener(new ScrollListener() {
			
			@Override
			public void onScroll(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY,
					int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
				mScrollY = scrollY;
				if(scrollY >= 10) {
					showTitleBarWindow();
				}
				else if(scrollY <= DensityUtil.dip2px(getActivity(), 46)){  
					removeTitleBarWindow();  
		        }  
			}
		});
		//add by xiatianhang @2015/8/11 新增城市列表
		regionTv = (TextView) view.findViewById(R.id.region_tv);
		regionTv.setOnClickListener(this);
		
		layout_gallery = (RelativeLayout) view.findViewById(R.id.layout_gallery);
		viewFlow = (ViewFlow) view.findViewById(R.id.mHomeViewflow);// 获得viewFlow对象
		indic = (CircleFlowIndicator) view.findViewById(R.id.mHomeViewflowindic); // viewFlow下的indic
		indic.setVisibility(View.GONE);
		
		layout_gallery.setLayoutParams(new LayoutParams(DensityUtil.getScreenWidth(getActivity()), (int) (DensityUtil.getScreenWidth(getActivity()) * 0.49)));//0.33
	
		mHotRecommendLayout = (LinearLayout) view.findViewById(R.id.layout_hot_recommend);
//		hotRecommendViewFlow = (ViewFlow) view.findViewById(R.id.mHotRecommendViewflow);// 获得viewFlow对象
//		hotRecommendIndic = (CircleFlowIndicator) view.findViewById(R.id.mHotRecommendViewflowindic); // viewFlow下的indic
//		hotRecommendIndic.setVisibility(View.GONE);
		
//		hot_recommend_tilte_name = (TextView) view.findViewById(R.id.hot_recommend_tilte_name);
//		hot_recommend_tilte_desc = (TextView) view.findViewById(R.id.hot_recommeed_tilte_desc);
		
		mNationalPavilionLayout = (LinearLayout) view.findViewById(R.id.layout_national_pavilion);

		// mGridView = (GridView) view.findViewById(R.id.mHomeGridView);
		// toolsGridViewAdapter = new LnkToolsGridViewAdapter(getActivity(),
		// listChannelItems);
		// mGridView.setAdapter(toolsGridViewAdapter);

		// mGridView.setOnItemClickListener(this);

		mScrollLayout = (ScrollLayout) view.findViewById(R.id.ScrollLayoutTest);

		fastImg = (ImageView) view.findViewById(R.id.fastImg);
		
		adImg01 = (ImageView) view.findViewById(R.id.adImg01);
		
		adImg02 = (ImageView) view.findViewById(R.id.adImg02);

		list_top = (ImageView) view.findViewById(R.id.list_top);

		layout_seckill = (LinearLayout) view.findViewById(R.id.layout_seckill);
		layout_cuxiao = (LinearLayout) view.findViewById(R.id.layout_cuxiao);

		// layout_prommotion1 = (CuXiaoHuodongView) view
		// .findViewById(R.id.layout_prommotion1);
		// layout_prommotion2 = (CuXiaoHuodongView) view
		// .findViewById(R.id.layout_prommotion2);
		// layout_prommotion3 = (CuXiaoHuodongView) view
		// .findViewById(R.id.layout_prommotion3);
		// layout_prommotion4 = (CuXiaoHuodongView) view
		// .findViewById(R.id.layout_prommotion4);

		list_top.setOnClickListener(this);

		home_show_recommend_linlayout = (LinearLayout) view.findViewById(R.id.home_show_recommend_linlayout);
		home_classification_linlayout = (LinearLayout) view.findViewById(R.id.home_classification_linlayout);

		ab_search = view.findViewById(R.id.ab_search);

		ab_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent i = new Intent();
				i.setClass(getActivity(), SiteSearchActivity.class);
				i.putExtra("content", "");
				startActivity(i);
			}
		});

		activity_index_code_scan = (ImageView) view.findViewById(R.id.activity_index_code_scan);
		activity_index_code_scan.setOnClickListener(this);
		
		new_msg_layout = (RelativeLayout) view.findViewById(R.id.new_msg_layout);
		new_msg_layout.setOnClickListener(this);
	}
	
	 /** 
     * 显示搜索悬浮框 
     */  
    private void showTitleBarWindow(){  
        if(titleBarView == null){  
        	titleBarView  = LayoutInflater.from(getActivity()).inflate(R.layout.actionbar_home, null);
        	View ab_search = titleBarView.findViewById(R.id.ab_search);
    		ab_search.setOnClickListener(new OnClickListener() {

    			@Override
    			public void onClick(View arg0) {
    				Intent i = new Intent();
    				i.setClass(getActivity(), SiteSearchActivity.class);
    				i.putExtra("content", "");
    				startActivity(i);
    			}
    		});
    		ImageView activity_index_code_scan = (ImageView) titleBarView.findViewById(R.id.activity_index_code_scan);
    		activity_index_code_scan.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					startActivity(new Intent(getActivity(), ZxingCodeActivity.class));
				} 
			});
    		
    		RelativeLayout new_msg_layout = (RelativeLayout) titleBarView.findViewById(R.id.new_msg_layout);
    		new_msg_layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					String userId = AppContext.getInstance().userId;
		    		if (TextUtils.isEmpty(userId) || null == AppContext.mUserInfo) {
		    			startActivity(new Intent(getActivity(), LoginActivity.class));
		    		}
		    		else {
		    			startActivity(new Intent(getActivity(), MyMessageActivity.class));
		    		}
					
				}
			});
    		
            if(titleBarLayoutParams == null){  
                titleBarLayoutParams = new WindowManager.LayoutParams();  
                titleBarLayoutParams.type = WindowManager.LayoutParams.TYPE_PHONE; //悬浮窗的类型，一般设为2002，表示在所有应用程序之上，但在状态栏之下   
                titleBarLayoutParams.format = PixelFormat.RGBA_8888;   
                titleBarLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL    
                         | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;  //悬浮窗的行为，比如说不可聚焦，非模态对话框等等   
                titleBarLayoutParams.gravity = Gravity.TOP;  //悬浮窗的对齐方式  
                titleBarLayoutParams.width = DensityUtil.getScreenWidth(getActivity()); //悬浮窗宽度 
                titleBarLayoutParams.height = DensityUtil.dip2px(getActivity(), 46); //悬浮窗高度   
                titleBarLayoutParams.x = 0;  //悬浮窗X的位置  
                titleBarLayoutParams.y = 0;  //悬浮窗Y的位置  
            }  
            mWindowManager.addView(titleBarView, titleBarLayoutParams);  
        }  
        
       
          
    }  
    
    /** 
     * 移除搜索悬浮框 
     */  
    private void removeTitleBarWindow(){  
        if(titleBarView != null){  
            mWindowManager.removeView(titleBarView);  
            titleBarView = null;  
        }  
    }  
    
//	/**
//	 * 城市分站点
//	 * 1.读取本地保存的省市
//	 * 2.本地没有保存，则获取定位的省市
//	 * 3.定位失败，则默认江苏省扬州市
//	 */
//	private void initLocation() {
//		//add by xiatianhang @2015/8/11 新增城市列表
//		soi = new SQLOpearteImpl(getActivity());
//		HashMap<String, String> locationMap = AppContext.getInstance().getLocation();
//		provinceName = locationMap.get("provinceName");
//		cityName = locationMap.get("cityName");
//		//areaName = locationMap.get("areaName");
//		provinceid = Integer.parseInt(locationMap.get("provinceId"));
//		cityid = Integer.parseInt(locationMap.get("cityId"));
//		//areaid = Integer.parseInt(locationMap.get("areaId"));
//		Log.d("HomeFragement", "initLocation : provinceName = " + provinceName + " cityName = " + cityName + " areaName = " + areaName);
//		
//		//if(StringUtils.isBlank(areaName) || areaid == -1) {
//		if(StringUtils.isBlank(cityName) || cityid == -1) {
//			//定位读取当前城市
//			BDLocation mBd = AppContext.getInstance().getBDLocation();
//	        if (null != mBd) {
//	        	provinceName = mBd.getProvince();
//	        	cityName = mBd.getCity();
//	        	//areaName = mBd.getDistrict();
//	        }
//			if (!StringUtils.isBlank(areaName)) {
//				provinceid = soi.checkIdByName(provinceName);
//				cityid = soi.checkIdByName(cityName);
//				//areaid = soi.checkIdByAreaName(areaName);
//				
//			} 
//			//定位失败，默认江苏省扬州市
//			else {
//				provinceName = "江苏省";
//	        	cityName = "南京市";
//	        	//areaName = "广陵区";
//				provinceid = 16;
//				cityid = 220;
//				//areaid = 1945;
//				
//			}
//		}
//		//regionTv.setText(cityName + ">" + areaName);
//		soi.CloseDB();
//		Log.d("HomeFragement", "initLocation : provinceid = " + provinceid + " cityid = " + cityid + " areaid = " + areaid);
//		//AppContext.getInstance().setRegionId(areaid);
//		//AppContext.getInstance().saveLocation(provinceid, cityid, areaid, provinceName, cityName, areaName);
//		AppContext.getInstance().saveLocation(provinceid, cityid, provinceName, cityName);
//	}

	/***
	 * 首页数据（除分类聚合的数据）
	 * */
	private void requestData() {
		CustomResponseHandler handler = new CustomResponseHandler(getActivity(), false) {

			@Override
			public void onFinish() {
				super.onFinish();
				requestFlag = true;
				if (requestFlag && requestPolyFlag && requestShowFlag)
					scrollview.onRefreshComplete();
			}

			@Override
			public void onRefreshData(String content) {
				
				//设置上一次更新的时间
				scrollview.getLoadingLayoutProxy().setLastUpdatedLabel(Utils.getCurrTiem());
				layout_seckill.removeAllViews();
				layout_cuxiao.removeAllViews();
				
				//解析数据
				analyticalHomeData(content, false);
			}
		};
		RequstClient.appIndexFirst(handler);
	}

	/***
	 * 分类聚合的数据
	 * */
	private void requestPolyData() {
		CustomResponseHandler handler = new CustomResponseHandler(getActivity(), false) {
			@Override
			public void onFinish() {
				super.onFinish();
				requestPolyFlag = true;
				if (requestFlag && requestPolyFlag && requestShowFlag)
					scrollview.onRefreshComplete();
			}

			@Override
			public void onRefreshData(String content) {
				home_classification_linlayout.removeAllViews();
				analyticalPolyData(content, false);
			}
		};
		RequstClient.appIndexPoly(handler);
	}
	
	// add by zhyao @2016/5/6 新增秀场推荐
	/***
	 * 分类聚合的数据
	 * */
	private void requestShowRecommendData() {
		CustomResponseHandler handler = new CustomResponseHandler(getActivity(), false) {
			@Override
			public void onFinish() {
				super.onFinish();
				requestShowFlag = true;
				if (requestFlag && requestPolyFlag && requestShowFlag)
					scrollview.onRefreshComplete();
			}

			@Override
			public void onRefreshData(String content) {
				MomentsShowListBean bean = MomentsShowListBean.explainJson(content, getActivity());
				try {
					loadShowRecommendData(bean.getShowList());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		};
		RequstClient.recommendShowList(handler);
	}
	
	// add by zhyao @2016/5/6 新增秀场推荐
	/**
	 * 加载推荐秀场
	 * @param showList
	 */
	private void loadShowRecommendData(ArrayList<MomentsShowItem> showList) {
		home_show_recommend_linlayout.removeAllViews();
		if(showList != null && !showList.isEmpty()) {
			ShowRecommendView recommendView = new ShowRecommendView(getActivity(), showList);
			home_show_recommend_linlayout.addView(recommendView);
			home_show_recommend_linlayout.setVisibility(View.VISIBLE);
		}
		else {
			home_show_recommend_linlayout.setVisibility(View.GONE);
		}
	}

	/**
	 * 解析首页数据（除分类聚合）
	 * 
	 * @param content
	 * 
	 *            网络返回的数据
	 * */
	private void analyticalHomeData(String content, boolean isCache) {
		try {
			JSONObject roots = new JSONObject(content);
			if (roots.getString("type").equals("1")) {
				JSONArray bannerArrays = roots.getJSONArray("bannerList");

				//活动轮播图
				List<GalleryAd> listGalleryAds = new ArrayList<GalleryAd>();
				for (int i = 0; i < bannerArrays.length(); i++) {
					JSONObject obj = bannerArrays.getJSONObject(i);
					GalleryAd bean = new GalleryAd();
					if (obj.has("BANNER_IMG"))
						bean.setImageUrl(URLs.IMAGE_URL + obj.getString("BANNER_IMG"));
					if (obj.has("BANNER_JUMP_FLAG"))
						bean.setFlag(obj.getInt("BANNER_JUMP_FLAG"));
					if (bean.getFlag() != 1 && obj.has("BANNER_JUMP_ID"))
						bean.setRowId(obj.getInt("BANNER_JUMP_ID"));
					if (bean.getFlag() == 1 && obj.has("ID"))
						bean.setHuodongId(obj.getInt("ID"));
					//add by zhyao @2015/12/10 添加Banner广告Html5活动页面
					if (bean.getFlag() == 5 && obj.has("ID"))
						bean.setHuodongId(obj.getInt("ID"));
					//add by zhyao @2015/12/30添加Banner视频
					if (bean.getFlag() == 6 && obj.has("BANNER_CONTENT"))	
						bean.setVideoUrl(obj.getString("BANNER_CONTENT"));
					listGalleryAds.add(bean);
				}

				//快捷服务导航
				List<ChannelItem> listChannelItems = new ArrayList<ChannelItem>();
				JSONArray channelArrays = roots.getJSONArray("fastList");
				for (int i = 0; i < channelArrays.length(); i++) {
					JSONObject obj = channelArrays.getJSONObject(i);
					ChannelItem bean = new ChannelItem();
					bean.setImageUrl(URLs.IMAGE_URL + obj.getString("FAST_IMG"));
					bean.setName(obj.getString("FAST_NAME"));
					bean.setChannelId(obj.getInt("ID"));
					listChannelItems.add(bean);
				}

				//设置轮播数据
				homeBean.setListGallerys(listGalleryAds);
				
				//设置快速通道数据
				homeBean.setListChannelItems(listChannelItems);

				if (roots.has("fastImg") && !StringUtils.isEmpty(roots.getString("fastImg"))) {
					homeBean.setFastImg(URLs.IMAGE_URL + roots.getString("fastImg"));
				} else {
					homeBean.setFastImg(null);
				}
				
				//广告1
				if (!isCache && roots.has("adv01") && !roots.isNull("adv01")) {
					HomeAdBean mHomeAdBean01 = new HomeAdBean();
					JSONObject obj = roots.getJSONObject("adv01");
					if (!obj.isNull("LINK_URL"))
						mHomeAdBean01.setLINK_URL(obj.getString("LINK_URL"));
					if (!obj.isNull("NAME"))
						mHomeAdBean01.setNAME(obj.getString("NAME"));
					if (!obj.isNull("ID"))
						mHomeAdBean01.setID(obj.getInt("ID"));
					if (!obj.isNull("IMG"))
						mHomeAdBean01.setIMG(obj.getString("IMG"));
					homeBean.setHomeAdBean01(mHomeAdBean01);
				} else {
					homeBean.setHomeAdBean01(null);
				}
				
				//广告2
				if (!isCache && roots.has("adv02") && !roots.isNull("adv02")) {
					HomeAdBean mHomeAdBean02 = new HomeAdBean();
					JSONObject obj = roots.getJSONObject("adv02");
					if (!obj.isNull("LINK_URL"))
						mHomeAdBean02.setLINK_URL(obj.getString("LINK_URL"));
					if (!obj.isNull("NAME"))
						mHomeAdBean02.setNAME(obj.getString("NAME"));
					if (!obj.isNull("ID"))
						mHomeAdBean02.setID(obj.getInt("ID"));
					if (!obj.isNull("IMG"))
						mHomeAdBean02.setIMG(obj.getString("IMG"));
					homeBean.setHomeAdBean02(mHomeAdBean02);
				} else {
					homeBean.setHomeAdBean02(null);
				}

				// 促销排行版
				List<TopList> listTopLists = new ArrayList<TopList>();
				JSONArray chartpositionArrays = roots.getJSONArray("chartpositionList");
				for (int i = 0; i < chartpositionArrays.length(); i++) {
					JSONObject obj = chartpositionArrays.getJSONObject(i);
					TopList bean = new TopList();
					bean.setImageUrl(URLs.IMAGE_URL + obj.getString("IMG"));
					// bean.setRowId(obj.getString("LINK_ID"));
					// bean.setFlag(obj.getInt("LINK_FLAG"));
					listTopLists.add(bean);
				}
				
				//促销商品
				JSONArray prommotionObj = roots.getJSONArray("prommotionLayout");
				List<SalesPromotion> listSalesPromotions = new ArrayList<SalesPromotion>();
				for (int i = 0; i < prommotionObj.length(); i++) {
					JSONObject obj = prommotionObj.getJSONObject(i);
					SalesPromotion salesPromotion = new SalesPromotion();
					salesPromotion.setId(obj.getString("id"));
					salesPromotion.setPromotion_name(obj.getString("promotion_name"));
					salesPromotion.setPromotion_adv(obj.getString("promotion_adv"));
					salesPromotion.setLayoutType(obj.getInt("promotion_layout"));
					JSONArray detailObj = obj.getJSONArray("detail");
					if (salesPromotion.getLayoutType() == 1 || salesPromotion.getLayoutType() == 3) {
						salesPromotion.setImageUrl1(detailObj.getJSONObject(0).getString("IMG1"));
						salesPromotion.setImageUrl2(detailObj.getJSONObject(1).getString("IMG2"));
						salesPromotion.setImageUrl3(detailObj.getJSONObject(2).getString("IMG3"));
						// salesPromotion.setUrl1(obj.getString("IMG1URL"));
						// salesPromotion.setUrl2(obj.getString("IMG2URL"));
						// salesPromotion.setUrl3(obj.getString("IMG3URL"));
						salesPromotion.setImage1Id(detailObj.getJSONObject(0).getString("COMMODITY_ID1"));
						salesPromotion.setImage2Id(detailObj.getJSONObject(1).getString("COMMODITY_ID2"));
						salesPromotion.setImage3Id(detailObj.getJSONObject(2).getString("COMMODITY_ID3"));

					} else if (salesPromotion.getLayoutType() == 2) {
						salesPromotion.setImageUrl1(detailObj.getJSONObject(0).getString("IMG1"));
						salesPromotion.setImageUrl2(detailObj.getJSONObject(1).getString("IMG2"));
						// salesPromotion.setUrl1(obj.getString("IMG1URL"));
						// salesPromotion.setUrl2(obj.getString("IMG2URL"));
						salesPromotion.setImage1Id(detailObj.getJSONObject(0).getString("COMMODITY_ID1"));
						salesPromotion.setImage2Id(detailObj.getJSONObject(1).getString("COMMODITY_ID2"));
					} else if (salesPromotion.getLayoutType() == 5) {
						salesPromotion.setImageUrl1(detailObj.getJSONObject(0).getString("IMG1"));
						salesPromotion.setImageUrl2(detailObj.getJSONObject(1).getString("IMG2"));
						salesPromotion.setImageUrl3(detailObj.getJSONObject(2).getString("IMG3"));
						salesPromotion.setImageUrl4(detailObj.getJSONObject(3).getString("IMG4"));
						// salesPromotion.setUrl1(obj.getString("IMG1URL"));
						// salesPromotion.setUrl2(obj.getString("IMG2URL"));
						salesPromotion.setImage1Id(detailObj.getJSONObject(0).getString("COMMODITY_ID1"));
						salesPromotion.setImage2Id(detailObj.getJSONObject(1).getString("COMMODITY_ID2"));
						salesPromotion.setImage3Id(detailObj.getJSONObject(2).getString("COMMODITY_ID3"));
						salesPromotion.setImage4Id(detailObj.getJSONObject(3).getString("COMMODITY_ID4"));
					} else if (salesPromotion.getLayoutType() == 4) {
						continue;
						// salesPromotion.setKEY1(obj.getString("KEY1"));
						// salesPromotion.setKEY2(obj.getString("KEY2"));
						// salesPromotion.setKEY3(obj.getString("KEY3"));
						// salesPromotion.setKEY4(obj.getString("KEY4"));
						// salesPromotion.setKEY5(obj.getString("KEY5"));
						// salesPromotion.setKEY6(obj.getString("KEY6"));
						// salesPromotion.setKEY7(obj.getString("KEY7"));
						// salesPromotion.setKEY8(obj.getString("KEY8"));
						// salesPromotion.setKEY9(obj.getString("KEY9"));
						// salesPromotion.setKEY10(obj.getString("KEY10"));
						// salesPromotion.setKEY11(obj.getString("KEY11"));
						// salesPromotion.setKEY12(obj.getString("KEY12"));
						//
						// salesPromotion.setKEY1URL(obj.getString("KEY1URL"));
						// salesPromotion.setKEY2URL(obj.getString("KEY2URL"));
						// salesPromotion.setKEY3URL(obj.getString("KEY3URL"));
						// salesPromotion.setKEY4URL(obj.getString("KEY4URL"));
						// salesPromotion.setKEY5URL(obj.getString("KEY5URL"));
						// salesPromotion.setKEY6URL(obj.getString("KEY6URL"));
						// salesPromotion.setKEY7URL(obj.getString("KEY7URL"));
						// salesPromotion.setKEY8URL(obj.getString("KEY8URL"));
						// salesPromotion.setKEY9URL(obj.getString("KEY9URL"));
						// salesPromotion.setKEY10URL(obj.getString("KEY10URL"));
						// salesPromotion.setKEY11URL(obj.getString("KEY11URL"));
						// salesPromotion.setKEY12URL(obj.getString("KEY12URL"));
						//
						// salesPromotion.setImageUrl1(obj.getString("IMG1"));
						// salesPromotion.setImageUrl2(obj.getString("IMG2"));
						// salesPromotion.setUrl1(obj.getString("IMG1URL"));
						// salesPromotion.setUrl2(obj.getString("IMG2URL"));
						// salesPromotion.setImage1Id(obj.getString("IMG1ID"));
						// salesPromotion.setImage2Id(obj.getString("IMG2ID"));
						//
						// salesPromotion.setDETAIL1ID(obj.getString("DETAIL1ID"));
						// salesPromotion.setDETAIL2ID(obj.getString("DETAIL2ID"));
						// salesPromotion.setDETAIL3ID(obj.getString("DETAIL3ID"));
						// salesPromotion.setDETAIL4ID(obj.getString("DETAIL4ID"));
						// salesPromotion.setDETAIL5ID(obj.getString("DETAIL5ID"));
						// salesPromotion.setDETAIL6ID(obj.getString("DETAIL6ID"));
						// salesPromotion.setDETAIL7ID(obj.getString("DETAIL7ID"));
						// salesPromotion.setDETAIL8ID(obj.getString("DETAIL8ID"));
						// salesPromotion.setDETAIL9ID(obj.getString("DETAIL9ID"));
						// salesPromotion.setDETAIL10ID(obj
						// .getString("DETAIL10ID"));
						// salesPromotion.setDETAIL11ID(obj
						// .getString("DETAIL11ID"));
						// salesPromotion.setDETAIL12ID(obj
						// .getString("DETAIL12ID"));
					}
					Log.i("", "aaa>>>>>d1>>>" + isCache);
					listSalesPromotions.add(salesPromotion);
					Log.i("", "aaa>>>>>d2>>>" + isCache);
				}
				Log.i("", "aaa>>>>>d3>>>" + isCache);
				homeBean.setListTopLists(listTopLists);

				homeBean.setListSalesPromotions(listSalesPromotions);
				Log.i("", "aaa>>>>>d4>>>" + isCache);
				if (!isCache) {
					Log.i("", "aaa>>>>>dddddd>>>" + homeBean.getListSalesPromotions());
				}
				Log.i("", "aaa>>>>>d5>>>" + isCache);
				if (roots.has("hotline")) {
					PreferenceUtil.getInstance(getActivity()).setHotLine(roots.getString("hotline"));
				}

				// 秒杀 英语要过关啊
				if (!isCache && roots.has("seckillList") && !roots.isNull("seckillList")) {
					HomeSeckillBean mHomeSeckillBean = new HomeSeckillBean();
					JSONObject obj = roots.getJSONObject("seckillList");
					if (!obj.isNull("ID"))
						mHomeSeckillBean.setID(MathUtil.stringToInt(obj.getString("ID")));
					if (!obj.isNull("REMARK"))
						mHomeSeckillBean.setREMARK(obj.getString("REMARK"));
					if (!obj.isNull("PROMOTION_TYPE"))
						mHomeSeckillBean.PROMOTION_TYPE=MathUtil.stringToInt(obj.getString("PROMOTION_TYPE"));
					if (!obj.isNull("END_TIME"))
						mHomeSeckillBean.setEND_TIME(obj.getString("END_TIME"));
					if (!obj.isNull("SECKILL_NAME"))
						mHomeSeckillBean.setSECKILL_NAME(obj.getString("SECKILL_NAME"));
					if (!obj.isNull("START_TIME"))
						mHomeSeckillBean.setSTART_TIME(obj.getString("START_TIME"));
					if (!roots.isNull("curTime"))
						mHomeSeckillBean.setCurDate(roots.getString("curTime"));
					if (!obj.isNull("COMMODITY_LIST")) {
						JSONArray array = obj.getJSONArray("COMMODITY_LIST");
						ArrayList<HomeSeckillListItemBean> mList = new ArrayList<HomeSeckillListItemBean>();
						for (int i = 0; i < array.length(); i++) {
							HomeSeckillListItemBean mBean = new HomeSeckillListItemBean();
							JSONObject obj1 = array.getJSONObject(i);
							if (!obj1.isNull("GOODS_PRICE"))
								mBean.setPRICE(Float.valueOf(obj1.getString("GOODS_PRICE")));
							if (!obj1.isNull("PRICE"))
								mBean.PRICE=Float.valueOf(obj1.getString("PRICE"));
							if (!obj1.isNull("GOODS_ID"))
								mBean.GOODS_ID=obj1.getString("GOODS_ID");
							if (!obj1.isNull("STORE_ID"))
								mBean.STORE_ID=obj1.getString("STORE_ID");
							if (!obj1.isNull("GOODS_NO"))
								mBean.GOODS_NO=obj1.getString("GOODS_NO");
							if (!obj1.isNull("GOODS_IMG"))
								mBean.setCOMMODITY_IMAGE_PATH(obj1.getString("GOODS_IMG"));
							if (!obj1.isNull("NUM"))
								mBean.setNUM(Integer.valueOf(obj1.getString("NUM")));
							if (!obj1.isNull("GOODS_NAME"))
								mBean.setCOMMODITY_NAME(obj1.getString("GOODS_NAME"));
							if (!obj1.isNull("DISCOUNT"))
								mBean.setDISCOUNT(Float.valueOf(obj1.getString("DISCOUNT")));
							mList.add(mBean);
						}
						mHomeSeckillBean.setCOMMODITY_LIST(mList);
					}
					homeBean.setSeckillList(mHomeSeckillBean);
				} else {
					homeBean.setSeckillList(null);
				}
				
				//闪购
				if (!isCache && roots.has("flashActive") && !roots.isNull("flashActive") && roots.getJSONObject("flashActive").length() > 0) {
					
					JSONObject obj = roots.getJSONObject("flashActive");
					HomeFlashBean mHomeFlsahBean = new HomeFlashBean(obj);
					homeBean.setmHomeFlashBean(mHomeFlsahBean);
			
				} else {
					homeBean.setmHomeFlashBean(null);
				}
				// 新品预约
				if (!isCache && roots.has("productBespeak") && !roots.isNull("productBespeak")
						&& roots.getJSONObject("productBespeak").length() > 0) {
					ProductBespeakBean productBespeakBean = new ProductBespeakBean();
					JSONObject obj = roots.getJSONObject("productBespeak");
					if (!obj.isNull("ADVER"))
						productBespeakBean.setADVER(obj.getString("ADVER"));
					if (!obj.isNull("ANAME"))
						productBespeakBean.setANAME(obj.getString("ANAME"));
					if (!obj.isNull("PID1"))
						productBespeakBean.PID1=obj.getString("PID1");
					if (!obj.isNull("CID1"))
						productBespeakBean.CID1=obj.getString("CID1");
					if (!obj.isNull("GOODS_NO1"))
						productBespeakBean.GOODS_NO1=obj.optString("GOODS_NO1");
					if (!obj.isNull("STORE_ID1"))
                        productBespeakBean.STORE_ID1=obj.optString("STORE_ID1");
					if (!obj.isNull("IMG1"))
						productBespeakBean.setIMG1(obj.getString("IMG1"));
					if (!obj.isNull("PID2"))
						productBespeakBean.PID2=obj.getString("PID2");
					if (!obj.isNull("CID2"))
						productBespeakBean.CID2=obj.getString("CID2");
					if (!obj.isNull("IMG2"))
						productBespeakBean.setIMG2(obj.getString("IMG2"));
                    if (!obj.isNull("GOODS_NO2"))
                        productBespeakBean.GOODS_NO2=obj.optString("GOODS_NO2");
                    if (!obj.isNull("STORE_ID2"))
                        productBespeakBean.STORE_ID2=obj.optString("STORE_ID2");
					if (!obj.isNull("PID3"))
						productBespeakBean.PID3=obj.getString("PID3");
					if (!obj.isNull("CID3"))
						productBespeakBean.CID3=obj.getString("CID3");
					if (!obj.isNull("IMG3"))
						productBespeakBean.setIMG3(obj.getString("IMG3"));
                    if (!obj.isNull("GOODS_NO3"))
                        productBespeakBean.GOODS_NO3=obj.optString("GOODS_NO3");
                    if (!obj.isNull("STORE_ID3"))
                        productBespeakBean.STORE_ID3=obj.optString("STORE_ID3");
					homeBean.setProductBespeakBean(productBespeakBean);
				} else {
					homeBean.setProductBespeakBean(null);
				}
				
				//add by zhyao @2016/2/25 添加热点推荐
				if (!isCache && roots.has("hotRecommendation") && !roots.isNull("hotRecommendation")) {
					HotRecommendationBean hotRecommendationBean = new HotRecommendationBean();
					
					JSONObject obj = roots.getJSONObject("hotRecommendation");
					if (!obj.isNull("title")) {
						hotRecommendationBean.setTitle(obj.getString("title"));
					}
					if (!obj.isNull("desc")) {
						hotRecommendationBean.setDesc(obj.getString("desc"));
					}
					if (!obj.isNull("recommendations")) {
						JSONArray array = obj.getJSONArray("recommendations");
						ArrayList<RecommendationBean> mRecommendList = new ArrayList<RecommendationBean>();
						for (int i = 0; i < array.length(); i++) {
							RecommendationBean mRecommendBean = new HotRecommendationBean().new RecommendationBean();
							JSONObject obj1 = array.getJSONObject(i);
							if (!obj1.isNull("TEMPLATE")){
								mRecommendBean.setTEMPLATE(obj1.getInt("TEMPLATE"));
							}
							if (!obj1.isNull("ID")){
								mRecommendBean.setID(obj1.getInt("ID"));
							}	
							if(!obj1.isNull("goods")) {
								JSONArray array1 = obj1.getJSONArray("goods");
								ArrayList<GoodBean> mGoodList = new ArrayList<GoodBean>();
								for (int j = 0; j < array1.length(); j++) {
									GoodBean mGoodBean = new HotRecommendationBean().new GoodBean();
									JSONObject obj2 = array1.getJSONObject(j);
									mGoodBean.setGOODS_ID(obj2.getInt("GOODS_ID"));
									mGoodBean.setIMG_URL(obj2.getString("IMG_URL"));
									mGoodList.add(mGoodBean);
								}
								mRecommendBean.setGoods(mGoodList);
							}
							mRecommendList.add(mRecommendBean);
						}
						hotRecommendationBean.setRecommendations(mRecommendList);
					}
					homeBean.setHotRecommendation(hotRecommendationBean);
				}
				else {
					homeBean.setHotRecommendation(null);
				}
				
				// add by zhyao @2016/3/3 添加国家馆
				if (!isCache && roots.has("nationalPavilionList") && !roots.isNull("nationalPavilionList")) {
					JSONArray array = roots.getJSONArray("nationalPavilionList");
					ArrayList<NationalPavilionBean> mNationalPavilionList = new ArrayList<NationalPavilionBean>();
					for (int i = 0; i < array.length(); i++) {
						NationalPavilionBean nationalPavilionBean = new NationalPavilionBean();
						JSONObject obj = array.getJSONObject(i);
						if (!obj.isNull("ORIGIN_IDS")){
							nationalPavilionBean.setORIGIN_IDS(obj.getString("ORIGIN_IDS"));
						}
						if (!obj.isNull("ID")){
							nationalPavilionBean.setID(obj.getString("ID"));
						}
						if (!obj.isNull("IMG")){
							nationalPavilionBean.setIMG(obj.getString("IMG"));
						}
						mNationalPavilionList.add(nationalPavilionBean);
					}
					homeBean.setNationalPavilionList(mNationalPavilionList);
				}
				else {
					homeBean.setNationalPavilionList(null);
				}

				if (!isCache) {
					AppContext.getInstance().saveCacheAppindexFirstData(content);
					layout_cuxiao.removeAllViews();
					// layout_prommotion1.removeAllViews();
					// layout_prommotion2.removeAllViews();
					// layout_prommotion3.removeAllViews();
					// layout_prommotion4.removeAllViews();
				}

				loadHomeData();
			} else {
				layout_seckill.removeAllViews();
				layout_cuxiao.removeAllViews();
				String errorMsg = roots.getString("msg");
				Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
				return;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 解析分类聚合（主题馆）
	 * 
	 * @param content
	 * 
	 *            网络返回的数据
	 * */
	private void analyticalPolyData(String content, boolean isCache) {
		try {
			JSONObject roots = new JSONObject(content);
			if (roots.getString("type").equals("1")) {
				JSONArray polyLists = roots.getJSONArray("polyList");
				List<HomePoly> listPolyLists = new ArrayList<HomePoly>();

				for (int i = 0; i < polyLists.length(); i++) {

					JSONObject obj = polyLists.getJSONObject(i);

					HomePoly bean = new HomePoly();
					bean.setTypeId(obj.getInt("POLY_TYPE_ID"));
					bean.setTypeImageUrl(obj.getString("POLY_TYPE_IMG"));
					bean.setName(obj.getString("POLY_TYPE_NAME"));

					JSONArray lists = obj.getJSONArray("polyList");
					List<HomePoly> listChilds = new ArrayList<HomePoly>();
					for (int j = 0; j < lists.length(); j++) {
						JSONObject objj = lists.getJSONObject(j);
						HomePoly object = new HomePoly();
						object.setTypeId(objj.getInt("POLY_TYPE_ID"));
						object.setImageUrl(objj.getString("POLY_IMG"));
						object.setId(objj.getInt("ID"));
						object.setLayout(objj.getInt("LAYOUT"));
						object.setName(objj.getString("POLY_NAME"));
						object.setLinkFlag(objj.getString("LINK_FLAG"));
						object.setLinkId(objj.getString("LINK_ID"));
						listChilds.add(object);
					}
					bean.setListPolies(listChilds);
					listPolyLists.add(bean);
				}

				homeBean.setListPolies(listPolyLists);
				initClassificationData(homeBean.getListPolies());

				if (!isCache) {
					AppContext.getInstance().saveCacheAppIndexPolyData(content);
				}
			} else {
				String errorMsg = roots.getString("msg");
				Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
				return;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 填充数据,除分类聚合数据外
	 */
	private void loadHomeData() {
		listGalleryAds.clear();
		listGalleryAds.addAll(homeBean.getListGallerys());

		updateLnkTools();

		// 广告图
		galleryViewflowAdapter = new GalleryViewflowAdapter(getActivity(), listGalleryAds);
		if (listGalleryAds.size() < 1) {
			indic.setVisibility(View.GONE);
		} else {
			indic.setVisibility(View.VISIBLE);
			viewFlow.setAdapter(galleryViewflowAdapter); // 对viewFlow添加图片
			viewFlow.setmSideBuffer(listGalleryAds.size());
			viewFlow.setFlowIndicator(indic);
			viewFlow.setTimeSpan(5000);
			viewFlow.setSelection(listGalleryAds.size() * 10); // 设置初始位置
			viewFlow.stopAutoFlowTimer();
			if (listGalleryAds.size() <= 1) {
				// 图片大于1才有指示器
				indic.setVisibility(View.GONE);
			} else {
				viewFlow.startAutoFlowTimer(); // 启动自动播放
			}
		}

		//add by zhyao @2016/2/25 添加热点推荐
		List<RecommendationBean> recommendList = null;
		if(homeBean.getHotRecommendation() != null) {
			recommendList = homeBean.getHotRecommendation().getRecommendations();
		}
		
		mHotRecommendLayout.removeAllViews();
		if(recommendList != null && recommendList.size() > 0) {
			mHotRecommendLayout.setVisibility(View.VISIBLE);
			HotRecommendView view = new HotRecommendView(getActivity(), homeBean.getHotRecommendation().getTitle(), homeBean.getHotRecommendation().getDesc(), recommendList);
			mHotRecommendLayout.addView(view);
			
//			hot_recommend_tilte_name.setText(homeBean.getHotRecommendation().getTitle());
//			hot_recommend_tilte_desc.setText(homeBean.getHotRecommendation().getDesc());
//			
//			hotRecommendViewflowAdapter = new MomentsViewflowAdapter(getActivity(), recommendList);
//			
//			hotRecommendViewFlow.setAdapter(hotRecommendViewflowAdapter); // 对viewFlow添加图片
//			hotRecommendViewFlow.setmSideBuffer(recommendList.size());
//			hotRecommendViewFlow.setFlowIndicator(hotRecommendIndic);
//			hotRecommendViewFlow.setTimeSpan(5000);
//			hotRecommendViewFlow.setSelection(recommendList.size() * 10); // 设置初始位置
//			hotRecommendViewFlow.startAutoFlowTimer();
//			hotRecommendViewFlow.setOnViewSwitchListener(new ViewSwitchListener() {
//				
//				public void onSwitched(View view, int position) {
//					
//				}
//			});
		}
		else {
			mHotRecommendLayout.setVisibility(View.GONE);
		}
		
		//add by zhyao @2016/3/3 添加国家馆
		mNationalPavilionLayout.removeAllViews();
		if(homeBean.getNationalPavilionList() != null && homeBean.getNationalPavilionList().size() > 0) {
			
			mNationalPavilionLayout.setVisibility(View.VISIBLE);
			NationalPavilionView view = new NationalPavilionView(getActivity(), homeBean.getNationalPavilionList());
			mNationalPavilionLayout.addView(view);
		}
		else {
			mNationalPavilionLayout.setVisibility(View.GONE);
		}
		
		// 销量排行榜
		DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true)
				.showStubImage(R.drawable.image_default_gallery).showImageForEmptyUri(R.drawable.image_default_gallery)
				.showImageOnFail(R.drawable.image_default_gallery).imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		
		if (homeBean.getListTopLists().size() > 0) {
			list_top.setVisibility(View.VISIBLE);
            //统一图片获取方式用于省流量设置
            ImageManager.LoadWithServer(homeBean.getListTopLists().get(0).getImageUrl(), list_top,options);
		}
		else {
			list_top.setVisibility(View.GONE);
		}

		if (homeBean.getFastImg() != null) {
            //统一图片获取方式用于省流量设置
            ImageManager.LoadWithServer(homeBean.getFastImg(),fastImg,options);
			fastImg.setVisibility(View.VISIBLE);
		} else {
			fastImg.setVisibility(View.GONE);
		}
		
		//广告位1
		if (homeBean.getHomeAdBean01() != null) {
			ImageManager.LoadWithServer(homeBean.getHomeAdBean01().getIMG(),adImg01,options);
			adImg01.setVisibility(View.VISIBLE);
			adImg01.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ChannelItem bean = new ChannelItem();
					bean.setChannelId(MathUtil.stringToInt(homeBean.getHomeAdBean01().getLINK_URL()));
					bean.setName(homeBean.getHomeAdBean01().getNAME());
					backitenClick(bean);
				}
			});
		}
		else {
			adImg01.setVisibility(View.GONE);
		}
		
		//广告位2
		if (homeBean.getHomeAdBean02() != null) {
			ImageManager.LoadWithServer(homeBean.getHomeAdBean02().getIMG(),adImg02,options);
			adImg02.setVisibility(View.VISIBLE);
			adImg02.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ChannelItem bean = new ChannelItem();
					bean.setChannelId(MathUtil.stringToInt(homeBean.getHomeAdBean02().getLINK_URL()));
					bean.setName(homeBean.getHomeAdBean02().getNAME());
					backitenClick(bean);
				}
			});
		}
		else {
			adImg02.setVisibility(View.GONE);
		}
				
		layout_seckill.removeAllViews();
		// 今日秒杀
		if (homeBean.getSeckillList() != null && homeBean.getSeckillList().getCOMMODITY_LIST() != null
				&& homeBean.getSeckillList().getCOMMODITY_LIST().size() > 0) {
			HomeSeckillBean temp = homeBean.getSeckillList();
			SeckillView view = new SeckillView(getActivity(), temp);
			layout_seckill.addView(view);
			layout_seckill.setVisibility(View.VISIBLE);
		}

		// 新品预约
		if (homeBean.getProductBespeakBean() != null) {
			BespeakView view = new BespeakView(getActivity(), homeBean.getProductBespeakBean());
			layout_seckill.addView(view);
			layout_seckill.setVisibility(View.VISIBLE);
		}

		// 闪购
		if (homeBean.getmHomeFlashBean() != null) {
			FlashView view = new FlashView(getActivity(), homeBean.getmHomeFlashBean());
			layout_seckill.addView(view);
			layout_seckill.setVisibility(View.VISIBLE);
		}

		layout_cuxiao.removeAllViews();
		for (int i = 0; i < homeBean.getListSalesPromotions().size(); i++) {
			View view = LayoutInflater.from(getActivity()).inflate(R.layout.prommotion_view, null);
			TextView promotion_name = (TextView) view.findViewById(R.id.promotion_name);
			TextView promotion_adv = (TextView) view.findViewById(R.id.promotion_adv);

			TextView promotion_more = (TextView) view.findViewById(R.id.promotion_more);

			CuXiaoHuodongView huodongView = (CuXiaoHuodongView) view.findViewById(R.id.layout_prommotion);
			final SalesPromotion promotion = homeBean.getListSalesPromotions().get(i);
			promotion_name.setText(promotion.getPromotion_name());
			promotion_adv.setText(promotion.getPromotion_adv());
			setPrommotion(huodongView, i);
			promotion_more.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					//活动专题
					Intent intent = new Intent(getActivity(), ZhuanQuActivity.class);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, Constants.ZhuangQu_Home_Promote);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, promotion.getId());
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, 1);
					startActivity(intent);
				}
			});
			layout_cuxiao.addView(view);
		}

		// if (homeBean.getListSalesPromotions().size() >= 1) {
		// setPrommotion(layout_prommotion1, 0);
		// } else {
		// layout_prommotion1.setVisibility(View.GONE);
		// }
		//
		// if (homeBean.getListSalesPromotions().size() >= 2) {
		// setPrommotion(layout_prommotion2, 1);
		// } else {
		// layout_prommotion2.setVisibility(View.GONE);
		// }
		//
		// if (homeBean.getListSalesPromotions().size() >= 3) {
		// setPrommotion(layout_prommotion3, 2);
		// } else {
		// layout_prommotion3.setVisibility(View.GONE);
		// }
		//
		// if (homeBean.getListSalesPromotions().size() >= 4) {
		// setPrommotion(layout_prommotion4, 3);
		// } else {
		// layout_prommotion4.setVisibility(View.GONE);
		// }

	}

	private void setPrommotion(CuXiaoHuodongView view, final int position) {
		view.setCoallBack(new CuXiaoHuodongViewCoallBack() {

			// 促销活动的四种布局的各个条目的点击事件
			@Override
			public void onCuxiaoClick(int id) {

				SalesPromotion bean = homeBean.getListSalesPromotions().get(position);

				Log.i("", position + "bbbb>>>>>>>" + homeBean.getListSalesPromotions());
				// String layoutType = "";
				String _id = "";
				switch (id) {
				case 1:
					// layoutType = bean.getUrl1();
					_id = bean.getImage1Id();
					break;
				case 2:
					// layoutType = bean.getUrl2();
					_id = bean.getImage2Id();
					break;
				case 3:
					// layoutType = bean.getUrl3();
					_id = bean.getImage3Id();
					break;
				case 4:
					_id = bean.getImage4Id();
					break;
				}

				// if (layoutType.equals("1")) {
				// Intent intent = new Intent(getActivity(),
				// ZhuanQuActivity.class);
				// intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, 2);
				// intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, _id);
				// intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, layoutType);
				// startActivity(intent);
				// } else if (layoutType.equals("2")) {
				// Intent intent = new Intent(getActivity(),
				// ZhuanQuActivity.class);
				// intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, 2);
				// intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, _id);
				// intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, layoutType);
				// startActivity(intent);
				// } else if (layoutType.equals("3")) {
				Intent intent = new Intent(getActivity(), ProductsDetailActivity.class);
				intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID, _id);
				getActivity().startActivity(intent);
				// }
			}

			// 第四种布局的条目的单击事件
			@Override
			public void onkeywordClick(String keyword, String layoutType, String _id) {
				Log.i("", "sdgsdsfasdf= =====" + _id);
				if (layoutType.equals("1")) {
					Intent intent = new Intent(getActivity(), ZhuanQuActivity.class);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, _id);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, Constants.ZhuangQu_Home_Promote);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_NAME, keyword);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, layoutType);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_BARNER_ID, _id);
					startActivity(intent);
				} else if (layoutType.equals("2")) {
					Intent intent = new Intent(getActivity(), ZhuanQuActivity.class);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, _id);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, Constants.ZhuangQu_Home_Promote);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_NAME, keyword);
					intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, layoutType);

					intent.putExtra(ZhuanQuActivity.INTENT_KEY_BARNER_ID, _id);
					startActivity(intent);
				} else if (layoutType.equals("3")) {
					Intent intent = new Intent(getActivity(), ProductsDetailActivity.class);
					intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID, _id);
					getActivity().startActivity(intent);
				}
			}
		});

		view.setData(homeBean.getListSalesPromotions().get(position));
	}

	/**
	 * 填充分类聚合的数据(主题馆)
	 * 
	 * @param array
	 */
	private void initClassificationData(List<HomePoly> array) {
		Cassification cassification;
		for (int i = 0; i < array.size(); i++) {
			HomePoly polys = array.get(i);
			List<HomePoly> polyList = polys.getListPolies();
			ArrayList<Cassification> arrayList = new ArrayList<Cassification>();
			for (HomePoly poly : polyList) {
				cassification = new Cassification();
				cassification.color = "#f4feef";
				cassification.iconPath = URLs.IMAGE_URL + poly.getImageUrl();
				cassification.name = poly.getName();
				cassification.id = poly.getId();
				cassification.layout = poly.getLayout();
				arrayList.add(cassification);
			}
			if (arrayList.size() > 0) {
				ClassificationView view = new ClassificationView(getActivity());
				view.setName(polys.getName());
				view.setData(arrayList);
				view.setCoallBack(mCassificationViewCoallBack);
				home_classification_linlayout.addView(view);
			}
		}
	}

	CassificationViewCoallBack mCassificationViewCoallBack = new CassificationViewCoallBack() {
		@Override
		public void onItemClick(Cassification arg0) {
			
			arg0.linkFlag = "1";
			
			//分类聚合
			Intent intent = new Intent(getActivity(), ZhuanQuActivity.class);
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_ID, arg0.id + "");
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_NAME, arg0.name + "");
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_FLAG, arg0.linkFlag);
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_BARNER_ID, arg0.linkId);
			intent.putExtra(ZhuanQuActivity.INTENT_KEY_TYPE, Constants.ZhuangQu_Home_Class);
			startActivity(intent);

		}
	};

	@Override
	public void onClick(View view) {
		if (list_top == view) {
			// 销量排行榜
			homeBean.getListTopLists();
			if (homeBean.getListTopLists() != null && homeBean.getListTopLists().size() > 0) {
				Intent intent = new Intent(getActivity(), SaleRankActivity.class);
				startActivity(intent);
			}
		} else if (view == activity_index_code_scan) {
			startActivity(new Intent(getActivity(), ZxingCodeActivity.class));
		} 
		 else if (view == new_msg_layout) {
			//消息
			String userId = AppContext.getInstance().userId;
    		if (TextUtils.isEmpty(userId) || null == AppContext.mUserInfo) {
    			startActivity(new Intent(getActivity(), LoginActivity.class));
    		}
    		else {
    			startActivity(new Intent(getActivity(), MyMessageActivity.class));
    		}
		} 
		//add by xiatianhang @2015/8/11 新增城市列表,选择城市
//		else if (view == regionTv) {
//			
//			Log.d("HomeFragement", "before : provinceid = " + provinceid + "  cityid = " + cityid + " areaid = " + areaid);
//			
//			PositionSelectNew positionSelect = new PositionSelectNew(getActivity(),
//					this.view, provinceid, cityid, areaid, provinceName, cityName, areaName);
//			positionSelect.setTitle("选择城市");
//			positionSelect
//					.setOnSelectResultListener(new PositionSelectNew.SelectResultListener() {
//						@Override
//						public void selectResult(int pId, int cId, int aId, String pName, String cName, String aName) {
//							provinceid =pId;
//							cityid =cId;
//							areaid = aId;
//							
//							provinceName = pName;
//							cityName = cName;
//							areaName = aName;
//							
//							Log.d("HomeFragement", "after : provinceid = " + provinceid + "  cityid = " + cityid + " areaid = " + areaid);
//							
////							soi = new SQLOpearteImpl(getActivity());
////		                    ArrayList<SQLOpearteImpl.Area> provices = soi.checkAllProvince();
////		                    for (SQLOpearteImpl.Area area : provices) {
////		                        if (area.rowId == provinceid) {
////		                        	provinceName = area.areaName;
////		                        }
////		                    }
////		                    ArrayList<SQLOpearteImpl.Area> cities = soi.checkAllCityById(provinceid);
////		                    for (SQLOpearteImpl.Area area : cities) {
////		                        if (area.rowId == cityid) {
////		                        	cityName = area.areaName;
////		                        }
////		                    }
//////		                    if (aId == -1) {//如果没有3级
//////		                    	soi.CloseDB();
//////		                    	cityname.setText(cityName);
//////		                    	AppContext.getInstance().setRegionId(cityid);
//////		                        return;
//////		                    }
////		                    ArrayList<SQLOpearteImpl.Area> areas = soi.checkAllDistriceById(cityid);
////		                    for (SQLOpearteImpl.Area area : areas) {
////		                        if (area.rowId == areaid) {
////		                        	areaName = area.areaName;
////		                        }
////		                    }
////		                    soi.CloseDB();
//		                    regionTv.setText(cityName + ">" + areaName);
//							AppContext.getInstance().setRegionId(areaid);
//							AppContext.getInstance().saveLocation(provinceid, cityid, areaid, provinceName, cityName, areaName);
//						}
//					});
//		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == MainActivity.LNKTOOLS_REQUEST) {
			updateLnkTools();
		}
	}

	public void updateLnkTools() {
		List<ChannelItem> lists = new ArrayList<ChannelItem>();
		if (homeBean != null && homeBean.getListChannelItems() != null) {
			lists = homeBean.getListChannelItems();
		}

		listChannelItems.clear();

		List<ChannelItem> listDatas = lnkToolsDao.fetcheAll();// 数据库存取的包含了数据的顺序

		if (listDatas.size() <= 0 && PreferenceUtil.getInstance(getActivity()).isFirstSet()) {
			for (int i = 0; i < lists.size(); i++) {
				listChannelItems.add(lists.get(i));
			}
		} else {
			// for (ChannelItem channelItem : listDatas) {
			// for (ChannelItem bean : lists) {
			// // 服务器
			// if (channelItem.getChannelId() == bean.getChannelId()) {
			// listChannelItems.add(bean);
			// }
			// }
			// }
			for (int i = 0; i < listDatas.size(); i++) {
				for (ChannelItem bean : lists) {
					if (listDatas.get(i).getChannelId() == bean.getChannelId()) {
						listChannelItems.add(bean);
					}
				}
			}
		}
		
//		ChannelItem channelItem = new ChannelItem();
//		channelItem.setChannelId(-1);
//		channelItem.setImageUrl("");
//		channelItem.setName("更多");
//		listChannelItems.add(channelItem);

//		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, (int) getResources().getDimension(
//				R.dimen.fragment_home_tools_height_half)
//				* ((listChannelItems.size()) / 4 + 1));

		// mGridView.setLayoutParams(params);
		//
		// // 快捷工具
		// toolsGridViewAdapter.notifyDataSetChanged();

		mScrollLayout.removeAllViews();
		mScrollLayout.snapToScreen(0);

//		int pageNo = (int) Math.ceil((listChannelItems.size() + 1) / 4.0f);
		int pageNo = (int) Math.ceil(listChannelItems.size() / 4.0f);
		for (int i = 0; i < pageNo; i++) {
			GridView appPage = new GridView(getActivity());
			LnkToolsGridViewAdapter lnkToolsGridViewAdapter;
			lnkToolsGridViewAdapter = new LnkToolsGridViewAdapter(getActivity(), listChannelItems, i);
			appPage.setAdapter(lnkToolsGridViewAdapter);
			appPage.setNumColumns(4);
			lnkToolsGridViewAdapter.setBackItemClickListener(this);
			mScrollLayout.addView(appPage);
		}
		if (pageNo <= 0) {
			GridView appPage = new GridView(getActivity());
			LnkToolsGridViewAdapter lnkToolsGridViewAdapter = new LnkToolsGridViewAdapter(getActivity());
			appPage.setAdapter(lnkToolsGridViewAdapter);
			appPage.setNumColumns(4);
			lnkToolsGridViewAdapter.setBackItemClickListener(this);
			mScrollLayout.addView(appPage);
		}

		// 加载分页
		pageControl = (PageControlView) view.findViewById(R.id.pageControl);
		pageControl.bindScrollViewGroup(mScrollLayout);
		if (pageNo > 1) {
			pageControl.setVisibility(View.VISIBLE);
		} else {
			pageControl.setVisibility(View.GONE);
		}
		// 加载分页数据
		dataLoad.bindScrollViewGroup(mScrollLayout);

	}

	// 分页数据
	class DataLoading {
		private int count;

		public void bindScrollViewGroup(ScrollLayout scrollViewGroup) {
			this.count = scrollViewGroup.getChildCount();
			scrollViewGroup.setOnScreenChangeListenerDataLoad(new OnScreenChangeListenerDataLoad() {
				public void onScreenChange(int currentIndex) {
				}
			});
		}
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase refreshView) {

		if (!NetworkUtils.isNetworkAvailable(getActivity())) {
			Toast.makeText(getActivity(), "网络故障，请先检查网络连接!", Toast.LENGTH_LONG).show();
		}

		requestFlag = false;
		requestPolyFlag = false;
		// add by zhyao @2016/5/6 新增秀场推荐
		requestShowFlag = false;
		
		
		requestData();// 请求主页数据
		
		requestPolyData();// 请求分类聚合的数据
		
		// add by zhyao @2016/5/6 新增秀场推荐
	    requestShowRecommendData(); 
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase refreshView) {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void backitenClick(ChannelItem bean) {
		Intent intent;
		switch (bean.getChannelId()) {
		case 1:
			// 今日头条
			intent = new Intent(new Intent(getActivity(), NewsTodayActivity.class));
			startActivity(intent);
			break;
		case 2:
			// 乐虎彩票
			intent = new Intent(new Intent(getActivity(), LotteryActivity.class));
			startActivity(intent);
			break;
		case 3:
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			} else {
				// 预约服务
				intent = new Intent(new Intent(getActivity(), ApplyBespeakActivity.class));
				startActivity(intent);
			}
			break;
		case 4:
			// 物流查询
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			} else {
                //跳转到待收货订单
                Intent wr_i = new Intent();
			    wr_i.setClass(getActivity(), AllOrderActivity.class);
			    wr_i.putExtra(AllOrderActivity.ORDER_TYPE, 3);
			    startActivity(wr_i);
			}
			break;
		case 5:
			// 智慧管家
//			AppContext.MAIN_TASK = AppContext.HOUSEKEEPER;
//			intent = new Intent(new Intent(getActivity(), MainActivity.class));
//			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			startActivity(intent);
			Intent i = new Intent();
			i.setClass(getActivity(), HouseKeeperActivity.class);
			getActivity().startActivity(i);
			break;
//		case 6:
//			// 秀场
//			intent = new Intent(new Intent(getActivity(), TheShowActivity.class));
//			startActivity(intent);
//			break;
		case 7:
			// 积分club  天天有奖
			intent = new Intent(new Intent(getActivity(), ClubActivity.class));
			intent.putExtra(ClubActivity.INTENT_KEY_TITLE, bean.getName());
			startActivity(intent);
			break;
		case 8:
			// 客户中心
			intent = new Intent(new Intent(getActivity(), KefuCenterActivity.class));
			startActivity(intent);
			break;
//		case 9:
//			intent = new Intent(new Intent(getActivity(), NearTheShopActivityNew.class));
//			startActivity(intent);
//			break;
		case 10:
			// 服务卡
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			} else {
				intent = new Intent(new Intent(getActivity(), LotteryActivity.class));
				if (AppContext.getInstance().mUserInfo.getBDStatus() == 1) {
					intent.setClass(getActivity(), ServiceCardActivity.class);
				} else {
					intent.setClass(getActivity(), BindServiceCard.class);
				}
				startActivity(intent);
			}
			break;
		case 11://话费
			
			intent = new Intent(getActivity(), PhonePayActivity.class);
			startActivity(intent);
			
			break;
        case 12: //水费
        case 13: //燃气
        case 14: //电费
        	intent = new Intent(new Intent(getActivity(), WaterCoalElectricActivity.class));
        	intent.putExtra(WaterCoalElectricActivity.ID,bean.getChannelId());
        	startActivity(intent);
        	break;
		case 15:
			// 红包申领
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			} else {
				intent = new Intent(new Intent(getActivity(), LehuVoucherActivity.class));
				startActivity(intent);
			}
			break;
		case 16:
			// 文体俱乐部
			intent = new Intent(new Intent(getActivity(), LotteryActivity.class));
			intent.putExtra("type",1);
			startActivity(intent);
			break;
		// add by zhyao @2015/5/8  添加摇一摇入口
		case 17:
			// 摇一摇
			intent = new Intent(new Intent(getActivity(), ShakeActivity.class));
			intent.putExtra(ClubActivity.INTENT_KEY_TITLE, bean.getName());
			startActivity(intent);
			break;
			// add by zhyao @2015/8/31 添加免费试用快捷方式
		case 22:
			//免费试用
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			}
			else {
				intent = new Intent(getActivity(), TryApplyActivity.class);
				intent.putExtra(TryApplyActivity.TRY_TYPE, TryApplyActivity.TRY_ACTITY);
				startActivity(intent);
			}
			
			break;
		case 23:
			//投票活动
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			}
			else {
				intent = new Intent(getActivity(), VoteActivity.class);
				startActivity(intent);
			}
			break;
		case 25:
			//每日签到
			if (StringUtils.isBlank(AppContext.getInstance().userId)) {
				Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
				intent = new Intent(new Intent(getActivity(), LoginActivity.class));
				startActivity(intent);
			}
			else {
				solveComplain();
			}
			break;
		default:
			intent = new Intent(new Intent(getActivity(), LnkToolsActivity.class));
			intent.putExtra("selected_data",  listChannelItems.toArray());
//			intent.putParcelableArrayListExtra("selected_data", (ArrayList<? extends Parcelable>) listChannelItems);

			if (homeBean != null && homeBean.getListChannelItems() != null) {
				intent.putExtra("all_data", homeBean.getListChannelItems().toArray());
//				intent.putParcelableArrayListExtra("all_data", (ArrayList<? extends Parcelable>) homeBean.getListChannelItems());
			} else {
				List<ChannelItem> temps = new ArrayList<ChannelItem>();
				intent.putExtra("all_data", temps.toArray());
//				intent.putParcelableArrayListExtra("all_data", (ArrayList<? extends Parcelable>) temps);
			}
			startActivityForResult(intent, MainActivity.LNKTOOLS_REQUEST);
			getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			
			break;
		}

		// if (bean.getChannelId() >= 11) {
		// // 点击的加号
		// Intent intent = new Intent(getActivity(), LnkToolsActivity.class);
		// intent.putParcelableArrayListExtra("selected_data", (ArrayList<?
		// extends Parcelable>) listChannelItems);
		//
		// if (homeBean != null && homeBean.getListChannelItems() != null) {
		// intent.putParcelableArrayListExtra("all_data", (ArrayList<? extends
		// Parcelable>) homeBean.getListChannelItems());
		// } else {
		// List<ChannelItem> temps = new ArrayList<ChannelItem>();
		// intent.putParcelableArrayListExtra("all_data", (ArrayList<? extends
		// Parcelable>) temps);
		// }
		//
		// startActivityForResult(intent, MainActivity.LNKTOOLS_REQUEST);
		// getActivity().overridePendingTransition(R.anim.slide_in_right,
		// R.anim.slide_out_left);
		//
		// } else {
		// switch (bean.getChannelId()) {
		// case 1:
		// // 今日头条
		// startActivity(new Intent(getActivity(), NewsTodayActivity.class));
		// break;
		// case 2:
		// // 乐虎彩票
		// startActivity(new Intent(getActivity(), LotteryActivity.class));
		// break;
		// case 3:
		// if (StringUtils.isBlank(AppContext.getInstance().userId)) {
		// Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
		// startActivity(new Intent(getActivity(), LoginActivity.class));
		// } else {
		// // 预约服务
		// startActivity(new Intent(getActivity(),
		// ApplyBespeakActivity.class));
		// }
		// break;
		// case 4:
		// // 物流查询
		// if (StringUtils.isBlank(AppContext.getInstance().userId)) {
		// Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
		// startActivity(new Intent(getActivity(), LoginActivity.class));
		// } else {
		// startActivity(new Intent(getActivity(),
		// LogisticsQueryActivity.class));
		// }
		// break;
		// case 5:
		// // 智慧管家
		// AppContext.MAIN_TASK = AppContext.HOUSEKEEPER;
		// Intent i = new Intent();
		// i.setClass(getActivity(), MainActivity.class);
		// i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// getActivity().startActivity(i);
		// break;
		// case 6:
		// // 秀场
		// startActivity(new Intent(getActivity(), TheShowActivity.class));
		// break;
		// case 7:
		// // 积分club
		// Intent intent = new Intent(getActivity(), ClubActivity.class);
		// intent.putExtra(ClubActivity.INTENT_KEY_TITLE, bean.getName());
		// startActivity(intent);
		// break;
		// case 8:
		// // 客户中心
		// startActivity(new Intent(getActivity(), KefuCenterActivity.class));
		// break;
		// case 9:
		// startActivity(new Intent(getActivity(),
		// NearTheShopActivityNew.class));
		// break;
		// case 10:
		// // 服务卡
		// if (StringUtils.isBlank(AppContext.getInstance().userId)) {
		// Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
		// startActivity(new Intent(getActivity(), LoginActivity.class));
		// } else {
		// Intent fuwu_intent = new Intent();
		// if (AppContext.getInstance().mUserInfo.bdStatus == 1) {
		// fuwu_intent.setClass(getActivity(), ServiceCardActivity.class);
		// } else {
		// fuwu_intent.setClass(getActivity(), BindServiceCard.class);
		// }
		// startActivity(fuwu_intent);
		// }
		// break;
		// case 15:
		// // 红包申领
		// if (StringUtils.isBlank(AppContext.getInstance().userId)) {
		// Toast.makeText(getActivity(), "请先登录", Toast.LENGTH_SHORT).show();
		// startActivity(new Intent(getActivity(), LoginActivity.class));
		// } else {
		// startActivity(new Intent(getActivity(), LehuVoucherActivity.class));
		// }
		// break;
		// default:
		// break;
		// }
		// }
	}
	
	/**
	 * 签到
	 */
	private void solveComplain(){
		
		//未签到
		if(0 == AppContext.mUserInfo.getFlagSign()){
			RequstClient.sign(new CustomResponseHandler(getActivity(), true) {
				@Override
				public void onSuccess(int statusCode, Header[] headers, String content) {
					super.onSuccess(statusCode, headers, content);
					
					//异常消息显示
					if(JSONParseUtils.isErrorJSONResult(content)){
						String msg = JSONParseUtils.getString(content, "msg");
						UIHelper.showToast(msg);
						return;
					}
					
					//签到时间,最新积分
					String curTime = JSONParseUtils.getString(content, "curTime");
					int integral = JSONParseUtils.getInt(content, "INTEGRAL");
					
					
					try{
						String newTime = DateUtil.convertBirthday(curTime);
						curTime = newTime;
					}catch(Exception e){
						e.printStackTrace();
					}
					
					//显示已签到
					UIHelper.showToast("签到成功！");
					AppContext.mUserInfo.FLAG_SIGN = "1";
					AppContext.mUserInfo.addSignDay(1);
					AppContext.mUserInfo.SIGNDAY_TIME = curTime;
					
					//刷新积分
					if(integral > 0){
						AppContext.mUserInfo.integral = integral+"";
					}
				}
				
			});
		}else{
			UIHelper.showToast("您今天已签到，请不要重复签到呦！请明天再来吧！");
		}
	}
	
}
