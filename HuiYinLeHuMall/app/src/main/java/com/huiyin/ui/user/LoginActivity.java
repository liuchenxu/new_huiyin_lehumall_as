package com.huiyin.ui.user;

import java.util.Map;

import org.apache.http.Header;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import cn.bidaround.youtui_template.YtTemplate;
import cn.bidaround.ytcore.login.AuthListener;
import cn.bidaround.ytcore.login.AuthLogin;
import cn.bidaround.ytcore.login.AuthUserInfo;

import com.google.gson.Gson;
import com.grong.hyin.sdk.entity.AccountKeeper;
import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.UserInfo;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.api.URLs;
import com.huiyin.base.BaseActivity;
import com.huiyin.bean.LoginInfo;
import com.huiyin.bean.ThirdLoginBean;
import com.huiyin.pay.wxpay.Constants;
import com.huiyin.ui.MainActivity;
import com.huiyin.ui.user.order.AllOrderDetailActivity;
import com.huiyin.utils.AppManager;
import com.huiyin.utils.JSONParseUtils;
import com.huiyin.utils.LogUtil;
import com.huiyin.utils.MyCustomResponseHandler;
import com.huiyin.utils.PreferenceUtil;
import com.huiyin.utils.YsfUtil;
import com.huiyin.wight.Tip;
import com.huiyin.wxapi.TokenBean;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * 登录界面
 * @author 刘远祺
 *
 * @todo TODO
 *
 * @date 2015-7-29
 */
public class LoginActivity extends BaseActivity implements OnClickListener {

	/**登录成功之后的动作**/
	public final static String TAG_Action 			= "Action";
	
	/**登录成功之后的动作**/
	public final static String TAG_PushFlag 		= "pushFlag";
	
	/**订单Id**/
	public final static String TAG_OrderId 			= "orderId";
	
	/**用户Id**/
	public final static String TAG_OrderUserId 		= "orderUserId";
	
	
	
	/**登录成功后-结束当前界面**/
	public static final String Login_To_Finish 		= "finish";
	
	/**登录成功后-跳转到MainActivity**/
	public static final String Login_To_Main 		= "main";
	
	/**登录成功后-跳转到共融直播**/
	public static final String Login_To_GRLive 		= "GRLive";
	
	/**
	 * 登录成功之后跳转到领券页面并且刷新
	 */
	public static final String Login_Get_Ticket 		= "Get_Ticket";
	
	private TextView login_forget_pwd, left_ib, middle_title_tv;
	private EditText login_user_et, login_pwd_et;
	private Toast mToast;
	private Button login_btn;
	private CheckBox login_check_box;
	private LoginInfo mLoginInfo;
	private String userName, password;
	
	private String tagAction;			//登录成功后的动作		
	private String tagPushFlag;
	private String tagOrderId;			//订单对应的Id,这个参数在订单二维码扫描时，带过来的，目的是为了验证订单是否为当前登录用户的订单
	private String tagOrderUserId;		//订单对应的userId,这个参数在订单二维码扫描时，带过来的，目的是为了验证订单是否为当前登录用户的订单
	
	private boolean isChecked = false;
	private TextView ab_right;

	private ImageView zfb, wx, wb, qq;
	private int loginType = 0;

	private PreferenceUtil instance = null;

	public static Map<String, String> map = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_fragment_layout);
		
		instance = PreferenceUtil.getInstance(mContext);
		
		//初始化控件
		initView();
		
		//初始化数据
		initData();
		
		//初始化第三方登录
		YtTemplate.init(this);
	}

	
	/**
	 * 初始化数据
	 */
	private void initData(){
		
		mLoginInfo = new LoginInfo();
		
		tagAction = getStringExtra(TAG_Action);
		tagPushFlag = getStringExtra(TAG_PushFlag, "0");
		tagOrderUserId = getStringExtra(TAG_OrderUserId);
		tagOrderId = getStringExtra(TAG_OrderId);

	}
	
	/**
	 * 初始化控件
	 */
	private void initView() {

		zfb = (ImageView) findViewById(R.id.login_zfb);
		zfb.setOnClickListener(this);
		wx = (ImageView) findViewById(R.id.login_wx);
		wx.setOnClickListener(this);
		wb = (ImageView) findViewById(R.id.login_wb);
		wb.setOnClickListener(this);
		qq = (ImageView) findViewById(R.id.login_qq);
		qq.setOnClickListener(this);

		login_forget_pwd = (TextView) findViewById(R.id.login_forget_pwd);
		login_forget_pwd.setOnClickListener(this);

		left_ib = (TextView) findViewById(R.id.ab_back);
		left_ib.setOnClickListener(this);
		
		middle_title_tv = (TextView) findViewById(R.id.ab_title);
		middle_title_tv.setText("登录");

		ab_right = (TextView) findViewById(R.id.ab_right);
		ab_right.setText("注册");
		ab_right.setOnClickListener(this);

		login_user_et = (EditText) findViewById(R.id.login_user_et);
		login_pwd_et = (EditText) findViewById(R.id.login_pwd_et);

		login_btn = (Button) findViewById(R.id.login_btn);
		login_btn.setOnClickListener(this);

		login_check_box = (CheckBox) findViewById(R.id.login_check_box);
		login_check_box.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				isChecked = arg1;
				if (arg1) {
					login_check_box.setTextColor(getResources().getColor(R.color.black));
				} else {
					login_check_box.setTextColor(getResources().getColor(R.color.grey1));
				}
			}
		});

		if (login_check_box.isChecked()) {
			isChecked = true;
			login_check_box.setTextColor(getResources().getColor(R.color.black));
		} else {
			isChecked = false;
			login_check_box.setTextColor(getResources().getColor(R.color.grey1));
		}

	}

	/**
	 * 判断登录信息
	 * @param userName
	 * @param password
	 * @return
	 */
	private boolean checkLoginInfo(String userName, String password) {

		if (TextUtils.isEmpty(userName)) {
			showMyToast(getString(R.string.user_is_null));
			return false;
		} else if (TextUtils.isEmpty(password)) {
			showMyToast(getString(R.string.pwd_is_null));
			return false;
		}
		return true;

	}

	/**
	 * 登录后刷新数据
	 */
	private void refreshData(String userId) {

		mLoginInfo.psw = password;
		mLoginInfo.userName = userName;
		mLoginInfo.isChecked = isChecked;
		AppContext.saveLoginInfo(getApplicationContext(), mLoginInfo);


		if (tagAction != null && tagAction.equals(Login_To_Finish)) {
			//判断orderUserId
			if(!TextUtils.isEmpty(tagOrderUserId)){
				
				//扫描二维码，进入订单详情
				if(userId.equals(tagOrderUserId)){
					//该订单是当前登录用户的订单
					Intent intent = new Intent(this, AllOrderDetailActivity.class);
			        intent.putExtra(AllOrderDetailActivity.ORDER_ID, tagOrderId);
			        startActivity(intent);
				}else{
					showMyToast(getString(R.string.text_order_login_tip));
				}
			}
			
			setResult(RESULT_OK);
			//结束登录界面,回到上一个界面
			//LoginActivity.this.finish();
			AppManager.getAppManager().finishActivity();
			
		} 
		//add by zhyao @2016/8/8 共融直播登录返回
		else if(tagAction != null && tagAction.equals(Login_To_GRLive)) {
			//共融直播设置用户信息
			RequstClient.saveUserInfoForGongRong(AppContext.mUserInfo.phone, AppContext.mUserInfo.userName, AppContext.mUserInfo.userName, AppContext.mUserInfo.img, new CustomResponseHandler(mContext) {
				@Override
				public void onSuccess(String content) {
					super.onSuccess(content);
					if(JSONParseUtils.getInt(content, "statusCode") == 200) {
						AccountKeeper aKeeper = new AccountKeeper(mContext);
						aKeeper.setAccount(AppContext.mUserInfo.phone, AppContext.mUserInfo.userName, URLs.IMAGE_URL + AppContext.mUserInfo.img);
						
						//setResult(RESULT_OK);
						//结束登录界面,回到上一个界面
						AppManager.getAppManager().finishActivity();
					}
				}
			});
			
		}
		//结束这个画面并且跳回到
		else if(tagAction!=null&&tagAction.equals(Login_Get_Ticket)){
			AppManager.getAppManager().finishActivity();
		}
		else {
			
			//跳转到主界面
			Intent i = new Intent();
			i.setClass(LoginActivity.this, MainActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.putExtra(MainActivity.From, "login");
			startActivity(i);
			LoginActivity.this.finish();
		}

	}


	/**
	 * 登录没带了猜你喜欢数据
	 */
	private void doLogin() {

		userName = login_user_et.getText().toString().trim();
		password = login_pwd_et.getText().toString().trim();
		if (!checkLoginInfo(userName, password)) {
			return;
		}
		
		//登录调用新接口
		RequstClient.loginNew(userName, password, "", "-1", new CustomResponseHandler(this) {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String content) {
				super.onSuccess(statusCode, headers, content);
				
				//异常消息显示
				if(JSONParseUtils.isErrorJSONResult(content)){
					String msg = JSONParseUtils.getString(content, "msg");
					showMyToast(msg);
					return;
				}

				//解析用户信息
				String userJson = JSONParseUtils.getJSONObject(content, "user");
				UserInfo mUserInfo = UserInfo.explainJson(userJson, mContext);
				AppContext.mUserInfo = mUserInfo;
				AppContext.userId = mUserInfo.userId;
				
				//删除本地购物车Id
				AppContext.deleteShopcardId();
				
				//设置七鱼云客服用户信息
				YsfUtil.setYsfUserInfo(mUserInfo.userId, mUserInfo.userName, mUserInfo.phone);
				
				//处理跳转
				refreshData(AppContext.userId);
			}

		});
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (tagPushFlag.equals("1")) {
				Intent i = new Intent();
				i.setClass(getApplicationContext(), MainActivity.class);
				startActivity(i);
			} else {
				finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_btn:
			
			//汇银登录
			doLogin();
			
			break;
		case R.id.login_zfb:
			
			// 支付宝第三方登录

			break;
		case R.id.login_wx:
			
			// 微信第三方登录
			onWXClickLogin();
			
			break;
		case R.id.login_wb:
			
			// 新浪微博第三方登录
			onSinaClickLogin();
			
			break;
		case R.id.login_qq:
			
			// QQ第三方登录
			onQQClickLogin();
			break;
		case R.id.login_forget_pwd:
			
			//忘记密码
			Intent i = new Intent();
			i.setClass(LoginActivity.this, BackPswActivity.class);
			i.putExtra(RegisterActivity.TAG_Action, tagAction);
			startActivity(i);
			//finish();
			AppManager.getAppManager().finishActivity();
			break;
		case R.id.ab_back:
			
			//返回
			if (tagPushFlag.equals("1")) {
				Intent iback = new Intent();
				iback.setClass(getApplicationContext(), MainActivity.class);
				startActivity(iback);
			} else {
				AppManager.getAppManager().finishActivity();
			}
			
			break;
		case R.id.ab_right:
			
			//注册
			Intent iregist = new Intent();
			iregist.setClass(LoginActivity.this, RegisterActivity.class);
			iregist.putExtra(RegisterActivity.TAG_Action, tagAction);
			startActivity(iregist);
			//finish();
			AppManager.getAppManager().finishActivity();
			break;
		default:
			break;
		}
	}

	/**
	 * 微信登录
	 */
	private void onWXClickLogin() {
		loginType = 1;
		IWXAPI api;
		api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
		if (!api.isWXAppInstalled() || !api.isWXAppSupportAPI()) {
			Toast.makeText(mContext, "微信未安装或微信版本过低，请安装升级最新版。", Toast.LENGTH_SHORT).show();
			return;
		}
		// 注册到微信
		if (api.registerApp(Constants.APP_ID)) {
			SendAuth.Req req = new SendAuth.Req();
			req.scope = "snsapi_userinfo";
			req.state = "com.huiyin.login";

			api.sendReq(req);
		}
	}

	/**
	 * 新浪登录
	 */
	private void onSinaClickLogin() {
		Tip.showLoadDialog(mContext, "正在加载...");
		loginType = 2;
		AuthLogin authSinaLogin = new AuthLogin();
		AuthListener SinaListener = new AuthListener() {
			@Override
			public void onAuthFail() {
				Tip.colesLoadDialog();
				Toast.makeText(mContext, "新浪微博未安装或微信版本过低，请安装升级最新版。", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onAuthCancel() {
				Tip.colesLoadDialog();
			}

			@Override
			public void onAuthSucess(AuthUserInfo info) {
				instance.setQQOpenid(info.getQqOpenid());
				Gson gson = new Gson();
				String i = gson.toJson(info, AuthUserInfo.class);
				oauthUser(null, info.getSinaUid(), i);
			}
		};
		authSinaLogin.sinaAuth(this, SinaListener);
	}

	/**
	 * QQ登录
	 */
	private void onQQClickLogin() {
		Tip.showLoadDialog(mContext, "正在加载...");
		loginType = 3;
		AuthLogin authQQLogin = new AuthLogin();
		AuthListener QQListener = new AuthListener() {
			@Override
			public void onAuthCancel() {
				Tip.colesLoadDialog();
			}

			@Override
			public void onAuthFail() {
				Tip.colesLoadDialog();
				Toast.makeText(mContext, "QQ未安装或微信版本过低，请安装升级最新版。", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onAuthSucess(AuthUserInfo info) {
				
				instance.setQQOpenid(info.getQqOpenid());
				Gson gson = new Gson();
				String i = gson.toJson(info, AuthUserInfo.class);
				oauthUser(null, info.getQqOpenid(), i);
			}
		};
		authQQLogin.qqAuth(this, QQListener);
	}

	@Override
	protected void onDestroy() {
		YtTemplate.release(this);
		super.onDestroy();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		String code = intent.getStringExtra("code");
		if (code != null) {
			getAssessToken(code);
		}
	}

	/**
	 * 微信 
	 * @param code
	 */
	private void getAssessToken(String code) {
		MyCustomResponseHandler handler = new MyCustomResponseHandler(mContext, false) {

			@Override
			public void onStart() {
				super.onStart();
				Tip.showLoadDialog(mContext, "正在加载...");
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				Tip.colesLoadDialog();
			}

			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				TokenBean bean = TokenBean.explainJson(content, mContext);
				if (bean.access_token != null && bean.openid != null) {
					oauthUser(bean, null, null);
				} else {
					Tip.colesLoadDialog();
					Toast.makeText(mContext, "授权失败", Toast.LENGTH_SHORT).show();
				}
			}
		};
		RequstClient.getWeChatToken(code, handler);
	}

	/**
	 * 验证第三方用户有没有在汇银登录过
	 * @param bean
	 * @param openId
	 * @param userinfo
	 */
	private void oauthUser(final TokenBean bean, final String openId, final String userinfo) {
		MyCustomResponseHandler handler = new MyCustomResponseHandler(mContext, false) {

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				Tip.colesLoadDialog();
			}

			@Override
			public void onRefreshData(String content) {
				super.onRefreshData(content);
				Tip.colesLoadDialog();
				ThirdLoginBean data = ThirdLoginBean.explainJson(content, mContext);
				if(data.type < 0) {
					Toast.makeText(mContext, "授权失败", Toast.LENGTH_SHORT).show();
					return;
				}
				if (data.type == 0) {
					showChoseDialog(bean, openId, userinfo);
					return;
				}
				if (data.type == 1) {
					AppContext.mUserInfo = data.user;
					AppContext.userId = data.user.userId;

					//删除本地购物车Id
					AppContext.deleteShopcardId();
					
					//处理跳转
					refreshData(AppContext.userId);

					return;
				}
			}
		};
		switch (loginType) {
		case 1:
			RequstClient.login4ThirdParty(bean.openid, loginType, handler);
			break;
		case 2:
			RequstClient.login4ThirdParty(openId, loginType, handler);
			break;
		case 3:
			RequstClient.login4ThirdParty(openId, loginType, handler);
			break;
		}

	}

	private Dialog mDialog;

	private void showChoseDialog(final TokenBean bean, final String openId, final String userinfo) {
		View mView = LayoutInflater.from(mContext).inflate(R.layout.chose_bind_dialog, null);
		mDialog = new Dialog(mContext, R.style.dialog);
		Button yes = (Button) mView.findViewById(R.id.com_ok_btn);
		Button cancle = (Button) mView.findViewById(R.id.com_cancel_btn);
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
				Intent i = new Intent();
				i.setClass(LoginActivity.this, RegisterActivity.class);
				i.putExtra("LoginType", loginType);
				switch (loginType) {
				case 1:
					i.putExtra("access_token", bean.access_token);
					i.putExtra("openid", bean.openid);
					break;
				case 2:
					i.putExtra("userinfo", userinfo);
					break;
				case 3:
					i.putExtra("userinfo", userinfo);
					break;
				}
				startActivity(i);
				//LoginActivity.this.finish();
				AppManager.getAppManager().finishActivity();
			}
		});
		yes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog.dismiss();
				Intent i = new Intent();
				i.setClass(LoginActivity.this, ThirdBindActivity.class);
				i.putExtra("LoginType", loginType);
				switch (loginType) {
				case 1:
					i.putExtra("openid", bean.openid);
					break;
				case 2:
					i.putExtra("openid", openId);
					break;
				case 3:
					i.putExtra("openid", openId);
					break;
				}
				startActivity(i);
				//LoginActivity.this.finish();
				AppManager.getAppManager().finishActivity();
			}
		});
		mDialog.setContentView(mView);
		mDialog.setCanceledOnTouchOutside(true);
		mDialog.setCancelable(true);
		mDialog.show();
	}
}
