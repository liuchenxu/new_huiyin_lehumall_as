package com.huiyin.ui.classic;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.api.URLs;
import com.huiyin.base.BaseFragment;
import com.huiyin.bean.GoodsContent;
import com.huiyin.utils.StringUtils;
import com.huiyin.utils.Utils;
import com.huiyin.utils.WebViewUtil;
import com.huiyin.view.MyWebView;

/**
 * Created by lian on 2015/5/16.
 */
public class GoodsDetailWebViewFragment extends BaseFragment implements View.OnClickListener {

	/**商品ID**/
	public static final String GOODSID = "goodsId";
	/**上级页面**/
	public static final String FROM = "from";
	
	public static final String FROM_TITLE = "PictureAndWordsDetailActivity";
	public static final String FROM_DETAIL = "ProductsDetailActivity";
	
	private MyWebView webview;
	private TextView btn_twxq;// 图文详情
	private TextView btn_bzgg;// 包装规格
	private TextView btn_shfw;// 售后服务
	
	// add by zhyao @2016/5/11 添加置顶按钮
	private ImageView img_back_up;

	private String dataHtmlTwxq;
	private String dataHtmlBzgg;
	private String dataHtmlShfw;

	/**商品ID**/
	private String goodsId;
	
	private String from;
	
	public static GoodsDetailWebViewFragment getInstance(String goodsId, String form){
		GoodsDetailWebViewFragment fragment = new GoodsDetailWebViewFragment();
		Bundle data = new Bundle();
		data.putString(GOODSID, goodsId);
		data.putString(FROM, form);
		fragment.setArguments(data);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle data = getArguments();
		if(null != data){
			this.goodsId = data.getString(GOODSID);
			this.from = data.getString(FROM);
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_goods_detail_webview, container, false);

		initViews(view);

		if (dataHtmlTwxq != null && !dataHtmlTwxq.equals("")) {
			loadWebData();
		} else {
			dataHtmlTwxq = "暂无信息";
			loadWebData();
		}
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		//加载商品信息
		goodsContent(goodsId);
	}

	/**
	 * 请求商品详情数据(商品信息,包装参数,售后服务)
	 */
	private void goodsContent(String goodsId) {
		
        RequstClient.goodsContent(goodsId, new CustomResponseHandler(mContext, false) {

            @Override
            public void onRefreshData(String content) {
                super.onRefreshData(content);
                if(isSuccessResponse(content)){
                	GoodsContent goodsContent = new GoodsContent(content);
                	showData(goodsContent);
                }
            }
        });
    }
	
	/**
	 * 显示商品信息 
	 * @param goodsContent
	 */
	private void showData(GoodsContent goodsContent){
		if(null != goodsContent){
			// add by zhyao @2015/10/24 添加视频播放
			StringBuffer newGoodInfo = new StringBuffer();
			if(!StringUtils.isEmpty(goodsContent.getVideo())) {
				newGoodInfo.append("<video poster='http://lehumall.b0.upaiyun.com/upload/image/admin/2015/20151019/201510191027489513.jpg' controls='controls' preload='metadata' loop='loop' width='100%' height='100%' name='media' autoplay='autoplay'><source src='" + URLs.IMAGE_URL + goodsContent.getVideo() + "' controls='controls' type='video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;'></video>");
				newGoodInfo.append(goodsContent.getInfo_content_map());
			}
			else {
				newGoodInfo.append(goodsContent.getInfo_content_map());
			}
			
			// 商品信息 包装参数 售后服务
			setWebview(newGoodInfo.toString(), goodsContent.gePackinglist_content_map(), goodsContent.getAfterservice_content_map());
		}
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	private void initViews(View view) {
		webview = (MyWebView) view.findViewById(R.id.webview);
		btn_twxq = (TextView) view.findViewById(R.id.btn_twxq);
		btn_bzgg = (TextView) view.findViewById(R.id.btn_bzgg);
		btn_shfw = (TextView) view.findViewById(R.id.btn_shfw);
		btn_twxq.setTextColor(getResources().getColor(R.color.red));
		btn_twxq.setOnClickListener(this);
		btn_bzgg.setOnClickListener(this);
		btn_shfw.setOnClickListener(this);
		
		img_back_up = (ImageView) view.findViewById(R.id.img_back_up);
		//下拉加载图文详情，则显示向上Logo
		if(FROM_DETAIL.equals(from)) {
			img_back_up.setVisibility(View.VISIBLE);
			img_back_up.setOnClickListener(this);
		}
		else {
			img_back_up.setVisibility(View.GONE);
		}
		
		WebSettings webSettings = webview.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		webSettings.setBlockNetworkImage(false);
		webSettings.setBlockNetworkLoads(false);
		
//		webSettings.setLoadWithOverviewMode(true);
//		webSettings.setBuiltInZoomControls(true);
//		webSettings.setSupportZoom(true);			//设定支持缩放
//		webSettings.setUseWideViewPort(true);		//设定支持viewport
		
		//delete by zhyao @2015/10/24 解决无法播放视频问题
//		webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		webSettings.setUseWideViewPort(true);
		webSettings.setSupportZoom(true);//可以缩放
		webSettings.setBuiltInZoomControls(true);//显示放大缩小
		webSettings.setDefaultZoom(ZoomDensity.CLOSE);//默认缩放模式
		
		
		
		webview.setBackgroundResource(R.color.transparent); // 设置背景色
		webview.getBackground().setAlpha(0); // 设置填充透明度 范围：0-255
		webview.setVisibility(View.INVISIBLE);
		webview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				// 结束
				super.onPageFinished(view, url);
				webview.setVisibility(View.VISIBLE);
			}

		});
		
		webview.setTouchListener(new MyWebView.IOnTouchListener() {
			
			private float scrollY;
			
			public void onTouch(MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_UP:
					
					//处理不能重复滑动，动画重复执行的问题
					if(Utils.isFastClick()){
                		return ;
                	}
					
					if (webview != null && webview.getScrollY() == 0) {
						if (null != listener) {
							float y = event.getY();
							if (y > scrollY) {
								listener.changeToGoodsDetail();// 跳转到商品详情
							}
						}
					}
					break;
				case MotionEvent.ACTION_DOWN:
					scrollY = event.getY();
					break;
				}
			}
		});
	}

	/**
	 * 设置HTML内容
	 * 
	 * @param dataHtmlTwxq
	 * @param dataHtmlBzgg
	 * @param dataHtmlShfw
	 */
	public void setWebview(String dataHtmlTwxq, String dataHtmlBzgg, String dataHtmlShfw) {
		this.dataHtmlTwxq = dataHtmlTwxq;
		this.dataHtmlBzgg = dataHtmlBzgg;
		this.dataHtmlShfw = dataHtmlShfw;
		
		if (StringUtils.isBlank(dataHtmlTwxq)) {
			dataHtmlTwxq = "没有搜索到相关信息";
		}
		
		if (StringUtils.isBlank(dataHtmlBzgg)) {
			dataHtmlBzgg = "没有搜索到相关信息";
		}
		
		if (StringUtils.isBlank(dataHtmlShfw)) {
			dataHtmlShfw = "没有搜索到相关信息";
		}
		
		if (webview != null) {
			webview.loadDataWithBaseURL(null, WebViewUtil.initWebViewFor19_ZOOM(dataHtmlTwxq), "text/html", "utf-8", null);
		}
	}

	public void loadWebData() {
		if (webview != null) {
			webview.loadDataWithBaseURL(null, WebViewUtil.initWebViewFor19_ZOOM(dataHtmlTwxq), "text/html", "utf-8", null);
		}
	}

	@Override
	public void onClick(View v) {
		btn_twxq.setTextColor(getResources().getColor(R.color.light_gray));
		btn_bzgg.setTextColor(getResources().getColor(R.color.light_gray));
		btn_shfw.setTextColor(getResources().getColor(R.color.light_gray));
		switch (v.getId()) {
		case R.id.btn_twxq:// 图文详情
			if (webview != null) {
				btn_twxq.setTextColor(getResources().getColor(R.color.red));
				if (StringUtils.isBlank(dataHtmlTwxq)) {
					dataHtmlTwxq = "没有搜索到相关信息";
				}
				webview.loadDataWithBaseURL(null, WebViewUtil.initWebViewFor19_ZOOM(dataHtmlTwxq), "text/html", "utf-8", null);
			}
			break;
		case R.id.btn_bzgg:// 包装规格
			if (webview != null) {
				btn_bzgg.setTextColor(getResources().getColor(R.color.red));
				if (StringUtils.isBlank(dataHtmlBzgg)) {
					dataHtmlBzgg = "没有搜索到相关信息";
				}
				webview.loadDataWithBaseURL(null, WebViewUtil.initWebViewFor19_ZOOM(dataHtmlBzgg), "text/html", "utf-8", null);
			}
			break;
		case R.id.btn_shfw:// 售后服务
			if (webview != null) {
				btn_shfw.setTextColor(getResources().getColor(R.color.red));
				if (StringUtils.isBlank(dataHtmlShfw)) {
					dataHtmlShfw = "没有搜索到相关信息";
				}
				webview.loadDataWithBaseURL(null, WebViewUtil.initWebViewFor19_ZOOM(dataHtmlShfw), "text/html", "utf-8", null);
			}
			break;
			//置顶
		case R.id.img_back_up:
			listener.changeToGoodsDetail();
			break;
		}
	}

	private OnFragmentTouchListener listener;

	public void setOnFragmentTouchListener(OnFragmentTouchListener listener) {
		this.listener = listener;
	}

	public interface OnFragmentTouchListener {
		void changeToGoodsDetail();
	}

	// add by zhyao @2015/10/24 销毁webview，结局返回播放视频问题
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(webview != null) {
			webview.destroy();
		}
	}
}
