package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.MomentsGoodsItem;
import com.huiyin.utils.ImageManager;

/**
 * 秀好物选择商品列表adapter
 * @author zhyao
 *
 */
public class MomentsGoodsAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<MomentsGoodsItem> listDatas;
	
	/**选择的商品**/
	private int selectPosition = 0;
	
	public MomentsGoodsAdapter(Context context, ArrayList<MomentsGoodsItem> listDatas) {
		mContext = context;
		this.listDatas = listDatas;
	}

	@Override
	public int getCount() {
		return listDatas == null ? 0 : listDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return listDatas == null ? null : listDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder;
		if(convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_moments_goods, null);
			
			viewHolder.mGoodsImg = (ImageView) convertView.findViewById(R.id.img_goods);
			viewHolder.mGoodsNameTv = (TextView) convertView.findViewById(R.id.tv_goods_name);
			viewHolder.mGoodsPriceTv = (TextView) convertView.findViewById(R.id.tv_goods_price);
			viewHolder.mGoodsSelectImg = (ImageView) convertView.findViewById(R.id.img_goods_select);
			
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		if(position == selectPosition) {
			viewHolder.mGoodsSelectImg.setVisibility(View.VISIBLE);
		}
		else {
			viewHolder.mGoodsSelectImg.setVisibility(View.GONE);
		}
		
		MomentsGoodsItem goodsItem = listDatas.get(position);
		
		//图片
		ImageManager.Load(goodsItem.getIMG(), viewHolder.mGoodsImg, ImageManager.squareOptions);
		//商品名
		viewHolder.mGoodsNameTv.setText(goodsItem.getGOODS_NAME());
		//商品价格
		viewHolder.mGoodsPriceTv.setText(String.format(mContext.getString(R.string.goods_price), goodsItem.getGOODS_PRICE()));
		
		
		return convertView;
	}
	
	static class ViewHolder {
		
		/**商品图片**/
		ImageView mGoodsImg;
		
		/**商品名称**/
		TextView mGoodsNameTv;
		
		/**商品价格**/
		TextView mGoodsPriceTv;
		
		/**商品是否选中**/
		ImageView mGoodsSelectImg;
		
	}

	public int getSelectPosition() {
		return selectPosition;
	}

	public void setSelectPosition(int selectPosition) {
		this.selectPosition = selectPosition;
	}

	
}
