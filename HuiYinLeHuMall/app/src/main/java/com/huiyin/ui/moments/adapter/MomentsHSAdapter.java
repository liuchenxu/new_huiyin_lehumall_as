package com.huiyin.ui.moments.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.MomentsCircleItem;
import com.huiyin.utils.ImageManager;
import com.huiyin.wight.RoundImageView;

/**
 * 秀场水平滑动圈子Adpater
 * @author zhyao
 *
 */
public class MomentsHSAdapter extends BaseAdapter {

	private Context mContext;

	private ArrayList<MomentsCircleItem> listDatas;

	public MomentsHSAdapter(Context context, ArrayList<MomentsCircleItem> listDatas) {
		mContext = context;

		this.listDatas = listDatas;
	}

	@Override
	public int getCount() {
		return listDatas == null ? 0 : listDatas.size();
	}

	@Override
	public Object getItem(int position) {
		return listDatas == null ? null : listDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if(convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_moments_hs, null);
			viewHolder.mCircleImg = (RoundImageView) convertView.findViewById(R.id.img_circle);
			viewHolder.mCricleTv = (TextView) convertView.findViewById(R.id.tv_circle);
			
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		ImageManager.Load(listDatas.get(position).getIMG(), viewHolder.mCircleImg, ImageManager.squareOptions);
		viewHolder.mCricleTv.setText(listDatas.get(position).getNAME());
		
		return convertView;
	}

	
	static class ViewHolder {
		RoundImageView mCircleImg;
		TextView  mCricleTv;
	}

}