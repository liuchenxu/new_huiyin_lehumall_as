package com.huiyin.ui.home;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonSyntaxException;
import com.huiyin.R;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.base.BaseActivity;
import com.huiyin.js.JsInterface;
import com.huiyin.js.JsInterface.WebViewClientClickListener;
import com.huiyin.ui.classic.ProductsDetailActivity;
import com.huiyin.ui.user.AddressManagementActivity;
import com.huiyin.utils.LogUtil;
import com.huiyin.utils.WebViewUtil;

public class HuodongWebPageActivity extends BaseActivity {

	private final static String TAG = "HuodongWebPageActivity";
	WebView mWebView;
	TextView left_ib, middle_title_tv;
	String flag, title;
	String html_text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_webpage);

		initView();
		initData();

	}

	@SuppressLint("SetJavaScriptEnabled")
	private void initView() {

		title = "活动介绍";
		flag = getIntent().getStringExtra("flag");

		left_ib = (TextView) findViewById(R.id.ab_back);
		left_ib.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}

		});

		mWebView = (WebView) findViewById(R.id.webview);
		WebSettings webSettings = mWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		webSettings.setBlockNetworkImage(false);
		webSettings.setBlockNetworkLoads(false);
		mWebView.setBackgroundResource(android.R.color.transparent); // 设置背景色
		mWebView.getBackground().setAlpha(0); // 设置填充透明度 范围：0-255

		middle_title_tv = (TextView) findViewById(R.id.ab_title);
		middle_title_tv.setText(title);

	}

	/**
	 * 获取数据
	 */
	private void initData() {

		RequstClient.getBannerIntroduce(flag, new CustomResponseHandler(this) {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String content) {

				super.onSuccess(statusCode, headers, content);
				LogUtil.i(TAG, "getBannerIntroduce:" + content);
				try {
					JSONObject obj = new JSONObject(content);
					if (!obj.getString("type").equals("1")) {
						String errorMsg = obj.getString("msg");
						Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_SHORT).show();
						return;
					}

					html_text = obj.getString("banner_content");
					loadWebData(html_text);

				} catch (JsonSyntaxException e) {

					e.printStackTrace();
				} catch (JSONException e) {

					e.printStackTrace();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}

		});
	}

	JsInterface jsInterface = new JsInterface();
	private void loadWebData(String dataHtml) {
		//"<div style="text-align:center;">	<img src="http://lehumall.b0.upaiyun.com/upload/image/editor/2015/20151208/201512081637383638.jpg" alt="" /><img src="http://lehumall.b0.upaiyun.com/upload/image/editor/2015/20151208/20151208163745533.jpg" alt="" /><img src="http://lehumall.b0.upaiyun.com/upload/image/editor/2015/20151208/201512081637519091.jpg" alt="" /><br /></div>"
		
//		dataHtml = "<div style=\"text-align:center;\">"
//				+ "<img onclick=\"nativeFunction('startGoodDetail', 0, -1 ,80377)\" src=\"http://lehumall.b0.upaiyun.com/upload/image/editor/2015/20151208/201512081637519091.jpg\" alt=\"\" /><br />"
//				+ "</div>"
//				+ "<script> "
//				+ "function nativeFunction(action, goodsNo, storeId, goodsId) "
//				+ "{ var params = \"[{'goodsNo':\" + goodsNo + \", 'storeId':\" + storeId + \",'goodsId':\" + goodsId + \"}]\"; "
//				+ "JSInterface.nativeFunction(action, params);"
//				+ "}"
//				+ "</script>";
//		dataHtml = "<div style='text-align:center;'>	<img onclick=\"nativeFunction('startGoodDetail', 0, -1 ,80377)\" src='http://lehumall.b0.upaiyun.com/upload/image/editor/2015/20151208/201512081637519091.jpg'/><br /></div><script>	function nativeFunction(action, goodsNo, storeId, goodsId)	{ var params = \"[{'goodsNo':\" + goodsNo + \", 'storeId':\" + storeId + \",'goodsId':\" + goodsId + \"}]\";		JSInterface.nativeFunction(action, params);	}</script>";
		
		mWebView.addJavascriptInterface(jsInterface, "JSInterface");
		mWebView.loadDataWithBaseURL(null, WebViewUtil.initWebViewFor19(dataHtml), "text/html", "utf-8", null);
		Log.d(TAG, "WebViewUtil.initWebViewFor19(dataHtml) = " + WebViewUtil.initWebViewFor19(dataHtml));
		jsInterface.setWebViewClientClickListener(new WebViewClientClickListener() {
			
			@Override
			public void wvHasClickEnvent(String action, String args) {
				LogUtil.d(TAG, "wvHasClickEnvent : action = " + action);
				LogUtil.d(TAG, "wvHasClickEnvent : args = " + args);
				JSONArray jsonArray;
				JSONObject jsonObject;
				Intent intent;
				if("startGoodDetail".equals(action)) {
					try {
						jsonArray = new JSONArray(args);
						jsonObject = (JSONObject) jsonArray.get(0);
		
						String storeId = jsonObject.getString("storeId");
						String goodsNo = jsonObject.getString("goodsNo");
						String goodsId = jsonObject.getString("goodsId");
						intent = new Intent(HuodongWebPageActivity.this, ProductsDetailActivity.class);
						intent.putExtra(ProductsDetailActivity.STORE_ID, storeId);
						intent.putExtra(ProductsDetailActivity.GOODS_NO, goodsNo);
						intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID, goodsId);
						
						startActivity(intent);
					} catch (JSONException e1) {
						e1.printStackTrace();
					}
					
				}
				
			}
		});
	}
}
