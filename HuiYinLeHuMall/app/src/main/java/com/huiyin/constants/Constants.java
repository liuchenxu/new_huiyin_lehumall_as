package com.huiyin.constants;

/**
 * 常量
 * @author 刘远祺
 *
 * @todo TODO
 *
 * @date 2015-7-28
 */
public class Constants {
	
	/**专区-首页分类聚合**/
	public static final int ZhuangQu_Home_Class 	= 1;
	
	/**专区-首页活动专题**/
	public static final int ZhuangQu_Home_Promote 	= 2;
	
	/**专区-首页导航**/
	public static final int ZhuangQu_Home_Banner 	= 3;
}
