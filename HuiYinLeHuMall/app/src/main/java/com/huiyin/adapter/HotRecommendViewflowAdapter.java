package com.huiyin.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.huiyin.R;
import com.huiyin.bean.HotRecommendationBean.GoodBean;
import com.huiyin.bean.HotRecommendationBean.RecommendationBean;
import com.huiyin.ui.classic.ProductsDetailActivity;
import com.huiyin.utils.ImageManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class HotRecommendViewflowAdapter extends BaseAdapter {

	private DisplayImageOptions options;
	private Context mContext;

	private List<RecommendationBean> listDatas;

	public HotRecommendViewflowAdapter(Context context, List<RecommendationBean> listDatas) {
		mContext = context;
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true)
				.showStubImage(R.drawable.image_default_gallery)
				.showImageForEmptyUri(R.drawable.image_default_gallery)
				.showImageOnFail(R.drawable.image_default_gallery)
				.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(0)).build();

		this.listDatas = listDatas;
	}

	@Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public int getViewTypeCount() {
		return 3;
	}
	
	@Override
	public int getItemViewType(int position) {
		return listDatas.get(position % listDatas.size()).getTEMPLATE();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
//		ViewHolder viewHolder1 = null;
//		ViewHolder viewHolder2 = null;
//		ViewHolder viewHolder3 = null;
		RecommendationBean recommendBean = listDatas.get(position % listDatas.size());
		int type = getItemViewType(position);
//		if (convertView == null) {
//			
//			switch (type) {
//			case 1:
//				viewHolder1 = new ViewHolder();
//			    convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_hot_recommend_1, null);
//			    viewHolder1.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
//                convertView.setTag(viewHolder1);
//				break;
//			case 2:
//				viewHolder2 = new ViewHolder();
//				convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_hot_recommend_2, null);
//				viewHolder2.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
//				viewHolder2.imageView2 = (ImageView) convertView.findViewById(R.id.imageView2);
//	            convertView.setTag(viewHolder2);
//				break;
//			case 3:
//				viewHolder3 = new ViewHolder();
//				convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_hot_recommend_3, null);
//				viewHolder3.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
//				viewHolder3.imageView2 = (ImageView) convertView.findViewById(R.id.imageView2);
//				viewHolder3.imageView3 = (ImageView) convertView.findViewById(R.id.imageView3);
//	            convertView.setTag(viewHolder3);
//				break;
//
//			default:
//				break;
//			}
//			
//		} else {
//			switch (type) {
//			case 1:
//				viewHolder1 = (ViewHolder) convertView.getTag();
//				break;
//			case 2:
//				viewHolder2 = (ViewHolder) convertView.getTag();
//				break;
//			case 3:
//				viewHolder3 = (ViewHolder) convertView.getTag();
//				break;
//
//			default:
//				break;
//			}
//			
//		}
//		GoodBean goodBean1 = null;
//		GoodBean goodBean2 = null;
//		GoodBean goodBean3 = null;
//		
//		switch (type) {
//		case 1:
//			goodBean1 = recommendBean.getGoods().get(0);
//			ImageManager.LoadWithServer(goodBean1.getIMG_URL(), viewHolder1.imageView1, options);
//			//viewHolder1.imageView1.setOnClickListener(new ItemClickListener(goodBean1.getGOODS_ID()));
//			break;
//		case 2:
//			goodBean1 = recommendBean.getGoods().get(0);
//			ImageManager.LoadWithServer(goodBean1.getIMG_URL(), viewHolder2.imageView1, options);
//			//viewHolder2.imageView1.setOnClickListener(new ItemClickListener(goodBean1.getGOODS_ID()));
//			
//			goodBean2 = recommendBean.getGoods().get(1);
//			ImageManager.LoadWithServer(goodBean2.getIMG_URL(), viewHolder2.imageView2, options);
//			//viewHolder2.imageView2.setOnClickListener(new ItemClickListener(goodBean2.getGOODS_ID()));
//			break;
//		case 3:
//			goodBean1 = recommendBean.getGoods().get(0);
//			ImageManager.LoadWithServer(goodBean1.getIMG_URL(), viewHolder3.imageView1, options);
//			//viewHolder3.imageView1.setOnClickListener(new ItemClickListener(goodBean1.getGOODS_ID()));
//			
//			goodBean2 = recommendBean.getGoods().get(1);
//			ImageManager.LoadWithServer(goodBean2.getIMG_URL(), viewHolder3.imageView2, options);
//			//viewHolder3.imageView2.setOnClickListener(new ItemClickListener(goodBean2.getGOODS_ID()));
//			
//			goodBean3 = recommendBean.getGoods().get(2);
//			ImageManager.LoadWithServer(goodBean3.getIMG_URL(), viewHolder3.imageView3, options);
//			//viewHolder3.imageView3.setOnClickListener(new ItemClickListener(goodBean3.getGOODS_ID()));
//			break;
//
//		default:
//			break;
//		}

		GoodBean goodBean1 = null;
		GoodBean goodBean2 = null;
		GoodBean goodBean3 = null;
		
		switch (type) {
		case 1:
			convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_hot_recommend_1, null);
			ImageView imageView11 = (ImageView)convertView.findViewById(R.id.imageView11);
			goodBean1 = recommendBean.getGoods().get(0);
			ImageManager.LoadWithServer(goodBean1.getIMG_URL(), imageView11, options);
			imageView11.setOnClickListener(new ItemClickListener(goodBean1.getGOODS_ID()));
			break;
		case 2:
			convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_hot_recommend_2, null);
			ImageView imageView21 = (ImageView)convertView.findViewById(R.id.imageView21);
			ImageView imageView22 = (ImageView)convertView.findViewById(R.id.imageView22);
			
			goodBean1 = recommendBean.getGoods().get(0);
			ImageManager.LoadWithServer(goodBean1.getIMG_URL(), imageView21, options);
			imageView21.setOnClickListener(new ItemClickListener(goodBean1.getGOODS_ID()));
			
			goodBean2 = recommendBean.getGoods().get(1);
			ImageManager.LoadWithServer(goodBean2.getIMG_URL(), imageView22, options);
			imageView22.setOnClickListener(new ItemClickListener(goodBean2.getGOODS_ID()));
			break;
		case 3:
			convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_hot_recommend_3, null);
			ImageView imageView31 = (ImageView)convertView.findViewById(R.id.imageView31);
			ImageView imageView32 = (ImageView)convertView.findViewById(R.id.imageView32);
			ImageView imageView33 = (ImageView)convertView.findViewById(R.id.imageView33);
			
			goodBean1 = recommendBean.getGoods().get(0);
			ImageManager.LoadWithServer(goodBean1.getIMG_URL(), imageView31, options);
			imageView31.setOnClickListener(new ItemClickListener(goodBean1.getGOODS_ID()));
			
			goodBean2 = recommendBean.getGoods().get(1);
			ImageManager.LoadWithServer(goodBean2.getIMG_URL(), imageView32, options);
			imageView32.setOnClickListener(new ItemClickListener(goodBean2.getGOODS_ID()));
			
			goodBean3 = recommendBean.getGoods().get(2);
			ImageManager.LoadWithServer(goodBean3.getIMG_URL(), imageView33, options);
			imageView33.setOnClickListener(new ItemClickListener(goodBean3.getGOODS_ID()));
			break;

		default:
			break;
		}
		
		return convertView;
	}

	
//	static class ViewHolder {
//		ImageView imageView1;
//		ImageView imageView2;
//		ImageView imageView3;
//	}
	
	private class ItemClickListener implements OnClickListener {
		
		private String id;
		
		public ItemClickListener(int id) {
			this.id = String.valueOf(id);
		}

		@Override
		public void onClick(View v) {
			Intent intent = new Intent(mContext, ProductsDetailActivity.class);
			intent.putExtra(ProductsDetailActivity.BUNDLE_KEY_GOODS_ID, id);
			mContext.startActivity(intent);
		}
		
	}

}