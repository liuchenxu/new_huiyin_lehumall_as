package com.huiyin.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.CategoryAndBrandBean.Brand;
import com.huiyin.bean.CategoryAndBrandBean.Navigator;
import com.huiyin.ui.classic.CategorySearchActivity;
import com.huiyin.ui.web.GoodsListWebActivity;
import com.huiyin.wight.MyGridView;
/**
 * 分类-品牌一级适配器
 * @author zhyao
 *
 */
public class CategoryBrandAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<Navigator> mNavigators;
	
	
	public CategoryBrandAdapter(Context context, ArrayList<Navigator> navigators) {
		mContext = context;
		mNavigators = navigators;
	}

	@Override
	public int getCount() {
		return mNavigators != null ? mNavigators.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return mNavigators != null ? mNavigators.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.category_brand_list_item, parent, false);
			holder.mTitleTv = (TextView) convertView.findViewById(R.id.tv_tile);
			holder.mBrandGridView = (MyGridView) convertView.findViewById(R.id.gd_brand);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Navigator navigator = mNavigators.get(position);
		
		holder.mTitleTv.setText(navigator.getNAME());
		
		CategoryBrandItemAdapter adapter = new CategoryBrandItemAdapter(mContext, navigator.getBrands());
		holder.mBrandGridView.setAdapter(adapter);
		holder.mBrandGridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position1, long id) {
				Brand brand = mNavigators.get(position).getBrands().get(position1);
//				Intent intent = new Intent(mContext, CategorySearchActivity.class);
//                intent.putExtra(CategorySearchActivity.BUNDLE_MARK, "4");
//				intent.putExtra(CategorySearchActivity.BUNDLE_BRAND_ID, brand.getID());
//				intent.putExtra(CategorySearchActivity.BUNDLE_KEY_CATEGORY_ID, brand.getFAST_GOODSCATEGORY_ID());
				Intent intent = new Intent(mContext, GoodsListWebActivity.class);
                intent.putExtra(GoodsListWebActivity.BUNDLE_MARK, "4");
				intent.putExtra(GoodsListWebActivity.BUNDLE_BRAND_ID, brand.getID());
				intent.putExtra(GoodsListWebActivity.BUNDLE_KEY_CATEGORY_ID, brand.getFAST_GOODSCATEGORY_ID());
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}

	static class ViewHolder {
		TextView mTitleTv;
		MyGridView mBrandGridView;
	}
	
}
