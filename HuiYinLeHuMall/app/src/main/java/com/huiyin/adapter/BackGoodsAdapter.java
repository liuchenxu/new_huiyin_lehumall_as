package com.huiyin.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huiyin.R;
import com.huiyin.bean.GoodsListEntity;
import com.huiyin.newpackage.entity.GoodsDetail;
import com.huiyin.utils.ImageManager;

/**
 * 退货--刘晨旭
 * 
 * @author 刘晨旭
 * 
 */
public class BackGoodsAdapter extends BaseAdapter {

	private Context context;

	private boolean isComplete = true;

	/** 上下文 **/
	private List<GoodsDetail> dataList = new ArrayList<GoodsDetail>();

	/** 选中的商品 **/
	private Map<String, GoodsDetail> checkedGoods;

	public BackGoodsAdapter(Context context, List<GoodsDetail> dataList) {
		this.context = context;
		if (dataList != null) {
			this.dataList.clear();
			this.dataList.addAll(dataList);
		}
		this.checkedGoods = new HashMap<String, GoodsDetail>();
	}

	/**
	 * 刷新数据
	 * 
	 * @param dataList
	 */
	public void refreshData(List<GoodsDetail> dataList, boolean isComplete) {
		this.dataList.clear();
		this.dataList.addAll(dataList);
		checkedGoods.clear();
		for (GoodsDetail goodsListEntity : dataList) {
			checkedGoods.put(goodsListEntity.getGOODS_ID() + "", goodsListEntity);
		}
		this.isComplete = isComplete;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return null != dataList ? dataList.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/**
	 * 选中商品
	 * 
	 * @param GoodsListEntity
	 */
	public void checkProduct(GoodsDetail GoodsListEntity) {

		if (null == GoodsListEntity) {
			return;
		}

		if (!checkedGoods.containsKey(GoodsListEntity.getGOODS_ID() + "")) {

			checkedGoods.put(GoodsListEntity.getGOODS_ID() + "", GoodsListEntity);
		} else {
			checkedGoods.remove(GoodsListEntity.getGOODS_ID() + "");
		}

		// 刷新
		notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (null == convertView) {
			convertView = LayoutInflater.from(context).inflate(R.layout.adapter_back_goods, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		}
		holder = (ViewHolder) convertView.getTag();

		// 显示数据
		holder.show(dataList.get(position), position);

		return convertView;
	}

	class ViewHolder {

		public ImageView iv_check; // 选中的图标
		public ImageView iv_logo; // 商品logo
		public TextView tv_name; // 商品名称
		public TextView tv_norms; // 商品规格
		public RelativeLayout goods_info_layout;
		public ImageView adapter_minus;
		public ImageView adapter_plus;
		public EditText adapter_goods_num;

		// add by zhyao @2016/7/5 删除老的加减数量，改为固定最大数量
		public TextView numberNewTv; // 数量

		private TextView back_goods_num;

		public View line; // 线条

		private GoodsDetail GoodsListEntity; // 当前显示的人商品

		ViewHolder(View convertView) {
			iv_check = (ImageView) convertView.findViewById(R.id.goods_check_imageview);
			iv_logo = (ImageView) convertView.findViewById(R.id.goods_pic_imageview);
			tv_name = (TextView) convertView.findViewById(R.id.goods_name_tv);
			tv_norms = (TextView) convertView.findViewById(R.id.norms_value_tv);
			line = convertView.findViewById(R.id.line);
			numberNewTv = (TextView) convertView.findViewById(R.id.apply_product_count_textview_new);
			goods_info_layout = (RelativeLayout) convertView.findViewById(R.id.goods_info_layout);
			adapter_minus = (ImageView) convertView.findViewById(R.id.adapter_minus);
			adapter_plus = (ImageView) convertView.findViewById(R.id.adapter_plus);
			adapter_goods_num = (EditText) convertView.findViewById(R.id.adapter_goods_num);
			back_goods_num = (TextView) convertView.findViewById(R.id.back_goods_num);
		}

		/**
		 * 显示商品信息
		 * 
		 * @param GoodsListEntity
		 */
		void show(final GoodsDetail GoodsListEntity, final int position) {
			// 记住当前的商品
			this.GoodsListEntity = GoodsListEntity;
			// 名称，规格
			ImageManager.LoadWithServer(GoodsListEntity.getGOODS_IMG(), iv_logo);
			tv_name.setText(GoodsListEntity.getGOODS_NAME());
			tv_norms.setText("");
			String goodsNum = GoodsListEntity.getQUANTITY() + "";
			numberNewTv.setText("数量：" + goodsNum);
			back_goods_num.setText("数量: x" + goodsNum);
			final String numGoodsNeedReturn = GoodsListEntity.NUM_GOODS_NEED_RETURN;
			adapter_goods_num.setText(numGoodsNeedReturn);
		}
	}

	/**
	 * 获取被选中的商品
	 * 
	 * @return
	 */
	public List<GoodsDetail> getCheckedGoods() {
		List<GoodsDetail> GoodsListEntitys = new ArrayList<GoodsDetail>();
		GoodsListEntitys.addAll(checkedGoods.values());
		return GoodsListEntitys;
	}

	/**
	 * 获取adapter中的待退货数据
	 * 
	 * @return
	 */
	public List<GoodsDetail> getReturnGoodsList() {
		return dataList;
	}
}
