package com.huiyin.adapter;

import java.util.ArrayList;

import org.apache.http.Header;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.huiyin.AppContext;
import com.huiyin.R;
import com.huiyin.api.CustomResponseHandler;
import com.huiyin.api.RequstClient;
import com.huiyin.bean.DianPingShaiDanList;
import com.huiyin.bean.DianPingShaiDanList.DianPing;
import com.huiyin.ui.classic.DianPingReplayActivity;
import com.huiyin.ui.classic.DianPingShaiDanActivity;
import com.huiyin.ui.classic.PhotoViewActivity;
import com.huiyin.ui.show.adapter.ShowSharePicGridViewAdapter;
import com.huiyin.ui.user.LoginActivity;
import com.huiyin.utils.JSONParseUtils;
import com.huiyin.wight.MyGridView;

/**
 * 点评晒单适配器
 * @author 刘远祺
 *
 * @todo TODO
 *
 * @date 2015-8-13
 */
public class DianPingShaiDanAdapterNew extends BaseAdapter {

	private Activity mContext;
	private LayoutInflater inflater;
	private ArrayList<DianPing> dianPingList;

	/**商品ID**/
	private String goodsId;
	
	public DianPingShaiDanAdapterNew(Activity content) {

		this.mContext = content;
		dianPingList = new ArrayList<DianPingShaiDanList.DianPing>();
		inflater = LayoutInflater.from(content);
	}

	public void addDianPingList(ArrayList<DianPing> list) {

		if (list == null || list.size() == 0) {
			return;
		}
		if (list instanceof ArrayList) {
			dianPingList.clear();
			dianPingList.addAll(list);
			notifyDataSetChanged();
		}
	}

	public void addMoreDianPingList(ArrayList<DianPing> list) {

		if (list == null || list.size() == 0) {
			return;
		}
		if (list instanceof ArrayList) {
			dianPingList.addAll(list);
			notifyDataSetChanged();
		}
	}

	public void clearDianPingList() {

		dianPingList.clear();
		notifyDataSetChanged();
	}

	/**
	 * 设置当前评论的商品ID
	 * @param goodsId
	 */
	public void setGoodsId(String goodsId){
		this.goodsId = goodsId;
	}
	
	@Override
	public int getCount() {

		return null != dianPingList ? dianPingList.size() : 0;
		//return 10;
	}

	@Override
	public Object getItem(int position) {

		return dianPingList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.dian_ping_shai_dan_lv_new, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		//显示数据
		DianPing dianPing = dianPingList.get(position);
		holder.showData(dianPing);

		return convertView;
	}

	class ViewHolder implements OnClickListener {
		
		TextView tv_name;					//用户名
		ImageView iv_pingxing;				//评分
		TextView tv_create_time;			//评价时间
		MyGridView share_pic;				//评价图片
		TextView tv_pingjia;				//评价内容
		View dian_ping_shai_dan_color_layout;//颜色规格布局
		TextView tv_color;					//颜色，规格
		TextView tv_model;					//型号
		
		TextView tv_buy_time;				//购买日期
		TextView dianzan;					//点赞
		TextView replay;					//回复
		
		View manager_reply_layout;			//管理员回复布局
		TextView manager_reply_content;		//管理员回复内容
		
		View dian_ping_shai_dan_add_reply; 	//管理员追加评论回复布局
		TextView dian_ping_shai_dan_reply_add_content;////管理员追加回复内容
		
		View comment_add;					//追加评论布局
		TextView tv_create_time_add;		//追加时间
		TextView tv_pingjia_add;			//追加评价内容
		MyGridView share_pic_add;			//追加评价图片
		TextView dianzan_add;				//追加点赞
		TextView replay_add;				//追加回复
		
		
		DianPing dianPing;					//当前显示的评价对象
		
		ViewHolder(View convertView){
			
			//评价用户名，评分，创建时间，图片，评价内容，购买时间
			tv_name = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_username);
			iv_pingxing = (ImageView) convertView.findViewById(R.id.dian_ping_shai_dan_pingxing);
			tv_create_time = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_create_time);
			share_pic = (MyGridView) convertView.findViewById(R.id.grv_share_pic);
			tv_pingjia = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_pingjia);
			tv_buy_time = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_buy_time);
			dian_ping_shai_dan_color_layout = convertView.findViewById(R.id.dian_ping_shai_dan_color_layout);
			tv_color = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_color);
			tv_model = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_model);
			
			//管理员回复
			manager_reply_layout = (LinearLayout) convertView.findViewById(R.id.dian_ping_shai_dan_reply);
			manager_reply_content = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_reply_content);
			
			//管理员对追加评论的回复
			dian_ping_shai_dan_add_reply = (LinearLayout) convertView.findViewById(R.id.dian_ping_shai_dan_add_reply);
			dian_ping_shai_dan_reply_add_content = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_reply_add_content);
			
			//点赞，回复
			dianzan = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_dianzan);
			replay = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_replay);
			dianzan.setOnClickListener(this);
			replay.setOnClickListener(this);
			
			//追加属性
			comment_add = convertView.findViewById(R.id.dian_ping_shai_dan_add_comment_layout);
			tv_create_time_add = (TextView) convertView.findViewById(R.id.add_comment_date);
			tv_pingjia_add = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_add_comment);
			dianzan_add = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_dianzan_add);
			replay_add = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_replay_add);
			share_pic_add = (MyGridView) convertView.findViewById(R.id.grv_share_pic_add);
			dianzan_add.setOnClickListener(this);
			replay_add.setOnClickListener(this);
		}
		
		/**
		 * 显示数据
		 */
		void showData(DianPing dianPing) {
			
			//记住当前点评
			this.dianPing = dianPing;
			
			//用户名，手机号
			if (dianPing.USER_NAME != null) {
				tv_name.setText(dianPing.getUSER_NAME());
				dian_ping_shai_dan_color_layout.setVisibility(View.VISIBLE);
			} else {
				dian_ping_shai_dan_color_layout.setVisibility(View.GONE);
			}

			//评分等级
			int imageResource = dianPing.getScoreImage();
			iv_pingxing.setImageResource(imageResource);
			
			//创建时间，评论内容，购买时间
			tv_create_time.setText(dianPing.CREATE_TIME);
			tv_pingjia.setText(dianPing.CONTENT);
			tv_buy_time.setText(dianPing.BUY_TIME);
			
			//颜色(规格) 型号
			String normData = dianPing.getNormData();
			if(!TextUtils.isEmpty(normData)){
				tv_color.setText(Html.fromHtml(normData));
			}else{
				tv_color.setVisibility(View.GONE);
			}
			tv_model.setText(dianPing.COMMDOITY_NAME);
			
			//点赞数，回复数
			dianzan.setText("点赞("+dianPing.PRIASE_NUM+")");
			replay.setText("回复("+dianPing.REPLY_NUM+")");
			
			//图片
			ArrayList<String> picList = dianPing.getIMGList();
			showCommentPic(picList, share_pic);

			
			//管理员对主评回复
			String adminReplayContent = dianPing.getAdminReplyList();
			if (!TextUtils.isEmpty(adminReplayContent)) {
				manager_reply_layout.setVisibility(View.VISIBLE);
				manager_reply_content.setText(Html.fromHtml(adminReplayContent));
			} else {
				manager_reply_layout.setVisibility(View.GONE);
			}
			
			//管理员对追评回复
			String adminAddReplayContent = dianPing.getAdminAddReplyList();
			if (!TextUtils.isEmpty(adminAddReplayContent)) {
				dian_ping_shai_dan_add_reply.setVisibility(View.VISIBLE);
				dian_ping_shai_dan_reply_add_content.setText(Html.fromHtml(adminAddReplayContent));
			} else {
				dian_ping_shai_dan_add_reply.setVisibility(View.GONE);
			}
			
			
			//追加评论
			if(dianPing.hasAddComment()){
				comment_add.setVisibility(View.VISIBLE);
				tv_create_time_add.setText(dianPing.ADD_EVA_TIME);
				tv_pingjia_add.setText(dianPing.ADD_CONTENT);
				dianzan_add.setText("点赞("+dianPing.ADD_PRIASE_NUM+")");
				replay_add.setText("回复("+dianPing.ADD_REPLY_NUM+")");
				
				//追加评论图片显示
				ArrayList<String> picListAdd = dianPing.getIMGListAdd();
				showCommentPic(picListAdd, share_pic_add);
				
			}else{
				comment_add.setVisibility(View.GONE);
			}
			
		}

		/**
		 * 显示上传的评论图片
		 * @param picList
		 * @param share_pic
		 */
		private void showCommentPic(ArrayList<String> picList, MyGridView share_pic){
			final ShowSharePicGridViewAdapter spadapter = new ShowSharePicGridViewAdapter(mContext);
			if (null != picList && picList.size() > 0) {
				spadapter.addItem(picList);
				if (picList.size() == 2) {
					share_pic.setNumColumns(2);
				} else if (picList.size() == 4) {
					share_pic.setNumColumns(2);
				} else if (picList.size() == 1) {
					share_pic.setNumColumns(2);
				} else {
					share_pic.setNumColumns(3);
				}
				share_pic.setAdapter(spadapter);
				share_pic.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						Intent intent = new Intent();
						intent.setClass(mContext, PhotoViewActivity.class);
						intent.putStringArrayListExtra(PhotoViewActivity.INTENT_KEY_PHOTO, spadapter.getListDatas());
						intent.putExtra(PhotoViewActivity.INTENT_KEY_POSITION, position);
						mContext.startActivity(intent);						
					}
				});
			} else {
				share_pic.setAdapter(spadapter);
			}
		}
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.dian_ping_shai_dan_dianzan:
				
				//点赞
				doDianzan(dianPing, false);
				
				break;

			case R.id.dian_ping_shai_dan_replay:
				
				//回复
				doReplay(dianPing, false);
				
				break;
				
			case R.id.dian_ping_shai_dan_dianzan_add:
			
				//追加点赞
				doDianzan(dianPing, true);
				
				break;
				
			case R.id.dian_ping_shai_dan_replay_add:
				
				//追加回复
				doReplay(dianPing, true);
				
				break;
			}
		}
		
		/**
		 * 发出点赞请求
		 * @param dianpin 
		 * @param isAdd
		 */
		void doDianzan(final DianPing dianpin, final boolean isAdd){
			
			if(null == AppContext.userId){
				Toast.makeText(mContext, "请先登录", Toast.LENGTH_LONG).show();
				
				//跳转到登录页面
				Intent intent = new Intent(mContext,LoginActivity.class);
				intent.putExtra(LoginActivity.TAG_Action, LoginActivity.Login_To_Finish);
				mContext.startActivity(intent);
				
				return;
			}
			
			//调用点赞接口
			String ID = isAdd ? dianpin.ADD_EVA_ID : dianpin.ID;
			String Flag = isAdd ? "1" : "0";
			RequstClient.evaPriaseGoods(ID, Flag, AppContext.userId, new CustomResponseHandler(mContext, false) {
				@Override
				public void onSuccess(int statusCode, Header[] headers, String content) {
					super.onSuccess(statusCode, headers, content);
					if(JSONParseUtils.isErrorJSONResult(content)){
						String msg = JSONParseUtils.getString(content, "msg");
						Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
					}else{
						
						Toast.makeText(mContext, "感谢点赞", Toast.LENGTH_LONG).show();
						
						//点赞数
						int priaseNum = JSONParseUtils.getInt(content, "priaseNum");
						if(isAdd){
							dianpin.setEvaDianzan(priaseNum);
						}else{
							dianpin.setDianzan(priaseNum);
						}
						
						//刷新数据
						notifyDataSetChanged();
					}
					
				}
			});
		}
		
		//进入回复界面
		void doReplay(DianPing dianPing, boolean isAdd){
			
			//2追加评论， 1评论
			String type = isAdd ? "2" : "1";
			String commentId = isAdd ? dianPing.ADD_EVA_ID : dianPing.ID;
			
			Intent intent = new Intent(mContext, DianPingReplayActivity.class);
			intent.putExtra(DianPingReplayActivity.GOODSID, goodsId);
			intent.putExtra(DianPingReplayActivity.TYPE, type);
			intent.putExtra(DianPingReplayActivity.Comment_ID, commentId);
			mContext.startActivityForResult(intent, DianPingShaiDanActivity.Request_Code_Replay);
			
			
			//记住当前回复的评论
			replayDianping = dianPing;
		}
	}

	/**
	 * 当前回复的评论
	 */
	private DianPing replayDianping;
	
	/**
	 * 刷新回复数量
	 * @param commentId
	 * @param type 追加 2 非追加 1
	 * @param goodsId
	 * @param replayCount
	 */
	public void refreshReplayCount(String commentId, String type, String goodsId, String replayCount) {
		
		if(null == replayDianping){
			return;
		}
		
		boolean isAdd = type.equals("2");
		String curCommentId = isAdd ? replayDianping.ADD_EVA_ID : replayDianping.ID;
		if(curCommentId.equals(commentId) && this.goodsId.equals(goodsId)){
			
			//刷新数量
			if(isAdd){
				replayDianping.ADD_REPLY_NUM = replayCount;
			}else{
				replayDianping.REPLY_NUM = replayCount;
			}
			
			//刷新界面
			notifyDataSetChanged();
		}
	}

}
