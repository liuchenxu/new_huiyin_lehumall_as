package com.huiyin.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.Review;
import com.huiyin.utils.ResourceUtils;
import com.huiyin.utils.StringUtils;

/**
 * 点评晒单回复-适配器
 * @author 刘远祺
 *
 * @todo TODO
 *
 * @date 2015-8-13
 */
public class DianPingReplayAdapter extends BaseAdapter {

	private Context content;
	private LayoutInflater inflater;
	private List<Review> dataList;

	private IReplayCallback reviewCallback;
	
	public DianPingReplayAdapter(Context content) {

		this.content = content;
		dataList = new ArrayList<Review>();
		inflater = LayoutInflater.from(content);
	}

	public void setData(List<Review> list) {

		if (list == null || list.size() == 0) {
			return;
		}
		dataList = list;
		notifyDataSetChanged();
	}

	/**
	 * 叠加数据
	 * @param list
	 */
	public void appendData(List<Review> list) {

		if (list == null || list.size() == 0) {
			return;
		}
		
		dataList.addAll(list);
		notifyDataSetChanged();
	}

	public void clearDianPingList() {

		dataList.clear();
		notifyDataSetChanged();
	}


	@Override
	public int getCount() {

		return null != dataList ? dataList.size() : 0;
		//return 10;
	}

	@Override
	public Object getItem(int position) {

		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.dian_ping_replay, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Review review = dataList.get(position);
		holder.showData(review, position);
		
		return convertView;
	}

	class ViewHolder implements OnClickListener {
		
		TextView dian_ping_shai_dan_username;		//用户名
		TextView dian_ping_shai_dan_shafa;			//沙发
		TextView dian_ping_shai_dan_pingjia;		//评价内容
		TextView date_textview;						//评价时间
		Button replay_button;						//回复
		
		Review curReview;							//当前显示的review
		
		ViewHolder(View convertView){
			
			//评价用户名，沙发，评价内容，评价时间，回复
			dian_ping_shai_dan_username = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_username);
	        dian_ping_shai_dan_shafa = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_shafa);
	        dian_ping_shai_dan_pingjia = (TextView) convertView.findViewById(R.id.dian_ping_shai_dan_pingjia);
	        date_textview = (TextView) convertView.findViewById(R.id.date_textview);
	        replay_button = (Button) convertView.findViewById(R.id.replay_button);
	        replay_button.setOnClickListener(this);
		}
		
		/**
		 * 显示数据
		 */
		void showData(Review review, int index){
			curReview = review;
			
			if(StringUtils.isEmpty(review.BE_USER_NAME)){
				
				//对评论的回复
				dian_ping_shai_dan_username.setText(review.getUSER_NAME());		//用户名
				dian_ping_shai_dan_shafa.setText(review.getShafa(index));	//沙发，板凳，地板
				dian_ping_shai_dan_shafa.setVisibility(View.GONE);//因为是倒序显示，则不显示这个楼层
				
				String conent = "回复 "+ ResourceUtils.changeStringColor("#a8a8a8", review.getF_USER_NAME())+ " "+review.CONTENT;
				dian_ping_shai_dan_pingjia.setText(Html.fromHtml(conent));	//评价内容
				
			}else{
				
				//对回复的回复
				dian_ping_shai_dan_username.setText(review.getUSER_NAME());		//用户名
				dian_ping_shai_dan_shafa.setText(review.getShafa(index));	//沙发，板凳，地板
				dian_ping_shai_dan_shafa.setVisibility(View.GONE);//因为是倒序显示，则不显示这个楼层
				
				String conent = "回复 "+ ResourceUtils.changeStringColor("#a8a8a8", review.getBE_USER_NAME())+ " "+review.CONTENT;
				dian_ping_shai_dan_pingjia.setText(Html.fromHtml(conent));	//评价内容
				
			}
			date_textview.setText(review.REPLY_TIME);					//评价时间
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.replay_button:
				
				//回复
				if(null != reviewCallback){
					reviewCallback.onReview(curReview);
				}
				
				break;
			}
				
		}
		
	}

	/**
	 * 添加一条新的回复
	 * @param review
	 */
	public void addNewReview(Review review) {
		if(null != review){
			//最新的回复显示在list的最新一条
			dataList.add(0, review);
			notifyDataSetChanged();
		}
	}

	/**
	 * 设置回复回调
	 */
	public void setOnReviewCallback(IReplayCallback callback){
		this.reviewCallback = callback;
	}
	
	
	/**
	 * 对回复的回复做回调处理
	 * @author 刘远祺
	 *
	 * @todo TODO
	 *
	 * @date 2015-8-15
	 */
	public interface IReplayCallback{
		
		/**回复**/
		public void onReview(Review review);
	}
}
