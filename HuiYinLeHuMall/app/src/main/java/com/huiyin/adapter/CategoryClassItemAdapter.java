package com.huiyin.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.custom.vg.list.CustomAdapter;
import com.huiyin.R;
import com.huiyin.bean.CategoryAndBrandBean.SubCategory;
/**
 * 分类-分类二级适配器
 * @author zhyao
 *
 */
public class CategoryClassItemAdapter extends CustomAdapter{
	
	private Context mContext;
	
	private ArrayList<SubCategory> mSubCategories;
	
	public CategoryClassItemAdapter(Context context, ArrayList<SubCategory> subCategories) {
		mContext = context;
		mSubCategories = subCategories;
	}

	@Override
	public int getCount() {
		return mSubCategories != null ? mSubCategories.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return mSubCategories != null ? mSubCategories.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.category_classic_item_list_item, parent, false);
			holder.mCategoryTv = (TextView) convertView.findViewById(R.id.tv_category);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		SubCategory subCategory = mSubCategories.get(position);
		holder.mCategoryTv.setText(subCategory.getCATEGORY_NAME());
		return convertView;
	}

	static class ViewHolder {
		TextView mCategoryTv;
	}
	
	
}


