package com.huiyin.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

import com.huiyin.R;
import com.huiyin.bean.CategoryAndBrandBean.Brand;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
/**
 * 分类-品牌二级适配器
 * @author zhyao
 *
 */
public class CategoryBrandItemAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<Brand> mBrands;
	
	private DisplayImageOptions options;
	
	public CategoryBrandItemAdapter(Context context, ArrayList<Brand> brands) {
		mContext = context;
		mBrands = brands;
	
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true)
				.showStubImage(R.drawable.image_default_gallery)
				.showImageForEmptyUri(R.drawable.image_default_gallery)
				.showImageOnFail(R.drawable.image_default_gallery)
				.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new RoundedBitmapDisplayer(0)).build();
	}

	@Override
	public int getCount() {
		return mBrands != null ? mBrands.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return mBrands != null ? mBrands.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.category_brand_item_list_item, parent, false);
			holder.mBrandImg = (ImageView) convertView.findViewById(R.id.img_brand);
			int width = (DensityUtil.getScreenWidth((Activity)mContext) - 2 * DensityUtil.dip2px(mContext, 15) - 3 * DensityUtil.dip2px(mContext, 10)) /4;
			LayoutParams params = new LayoutParams(width, width);
			holder.mBrandImg.setLayoutParams(params);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Brand brand = mBrands.get(position);
		ImageManager.LoadWithServer(brand.getIMG(), holder.mBrandImg, options);
		return convertView;
	}

	static class ViewHolder {
		ImageView mBrandImg;
	}
	
}
