package com.huiyin.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.custom.vg.list.CustomListView;
import com.custom.vg.list.OnItemClickListener;
import com.custom.vg.list.OnItemLongClickListener;
import com.huiyin.R;
import com.huiyin.bean.CategoryAndBrandBean.Category;
import com.huiyin.bean.CategoryAndBrandBean.SubCategory;
import com.huiyin.ui.classic.CategorySearchActivity;
import com.huiyin.ui.web.GoodsListWebActivity;
import com.huiyin.utils.DensityUtil;
import com.huiyin.utils.ImageManager;
/**
 * 分类-分类一级适配器
 * @author zhyao
 *
 */
public class CategoryClassAdapter extends BaseAdapter{
	
	private Context mContext;
	
	private ArrayList<Category> mCategories;
	
	public CategoryClassAdapter(Context context, ArrayList<Category> categories) {
		mContext = context;
		
		mCategories = categories;
		
	}

	@Override
	public int getCount() {
		return mCategories != null ? mCategories.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return mCategories != null ? mCategories.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		ViewHolder holder;
		if(convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(R.layout.category_classic_list_item, parent, false);
			holder.mCategoryBgImg = (ImageView) convertView.findViewById(R.id.img_category_bg);
			holder.mCategoryNameTv = (TextView) convertView.findViewById(R.id.tv_category_name);
			holder.mSubTitleTv = (TextView) convertView.findViewById(R.id.tv_sub_title);
			holder.mAllLayout = (LinearLayout) convertView.findViewById(R.id.layout_all);
			holder.mCustomListView = (CustomListView) convertView.findViewById(R.id.lv_category);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		final Category category = mCategories.get(position);
		
		ImageManager.Load(category.getBG_IMG_URL(), holder.mCategoryBgImg);
		holder.mCategoryNameTv.setText(category.getCATEGORY_NAME());
		holder.mSubTitleTv.setText(category.getSUB_TITLE());
		//查看全部
		holder.mAllLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				Intent intent = new Intent(mContext, CategorySearchActivity.class);
//                intent.putExtra(CategorySearchActivity.BUNDLE_MARK, category.getRANK());
//				intent.putExtra(CategorySearchActivity.BUNDLE_KEY_CATEGORY_ID, category.getID());
//				mContext.startActivity(intent);
				Intent intent = new Intent(mContext, GoodsListWebActivity.class);
                intent.putExtra(GoodsListWebActivity.BUNDLE_MARK, category.getRANK());
				intent.putExtra(GoodsListWebActivity.BUNDLE_KEY_CATEGORY_ID, category.getID());
				mContext.startActivity(intent);
			}
		});
		
		holder.mCustomListView.setDividerHeight(DensityUtil.dip2px(mContext, 25));
		holder.mCustomListView.setDividerWidth(DensityUtil.dip2px(mContext, 20));
		CategoryClassItemAdapter adapter = new CategoryClassItemAdapter(mContext, category.getSub_categories());
		holder.mCustomListView.setAdapter(adapter);
		holder.mCustomListView.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				SubCategory subCategory = mCategories.get(position).getSub_categories().get(arg2);
//				Intent intent = new Intent(mContext, CategorySearchActivity.class);
//                intent.putExtra(CategorySearchActivity.BUNDLE_MARK, subCategory.getRANK());
//				intent.putExtra(CategorySearchActivity.BUNDLE_KEY_CATEGORY_ID, subCategory.getID());
				Intent intent = new Intent(mContext, GoodsListWebActivity.class);
                intent.putExtra(GoodsListWebActivity.BUNDLE_MARK, subCategory.getRANK());
				intent.putExtra(GoodsListWebActivity.BUNDLE_KEY_CATEGORY_ID, subCategory.getID());
				mContext.startActivity(intent);
			}
		});
		holder.mCustomListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				return false;
			}
		});
		return convertView;
	}

	static class ViewHolder {
		ImageView mCategoryBgImg;
		TextView mCategoryNameTv;
		TextView mSubTitleTv;
		LinearLayout mAllLayout;
		CustomListView mCustomListView;
	}
	
}
