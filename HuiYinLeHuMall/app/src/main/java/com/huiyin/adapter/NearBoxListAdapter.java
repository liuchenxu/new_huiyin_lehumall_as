package com.huiyin.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.huiyin.R;
import com.huiyin.bean.NearBoxBean.DevicesItem;
import com.huiyin.utils.MathUtil;
import com.huiyin.utils.ViewHolder;

//add by zhyao @2015/7/25 添加附件箱子自提列表adapter
public class NearBoxListAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<DevicesItem> list;

	public NearBoxListAdapter(Context mContext) {
		this.mContext = mContext;
		list = new ArrayList<DevicesItem>();
	}

	@Override
	public int getCount() {
		return null != list ? list.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.dialog_listview_near_box_item, parent, false);
		}

		TextView name = ViewHolder.get(convertView, R.id.name);
		TextView address = ViewHolder.get(convertView, R.id.address);

		DevicesItem temp = (DevicesItem) getItem(position);

		name.setText(temp.name);
		address.setText(temp.address);
		return convertView;
	}

	public void deleteItem() {
		list.clear();
		notifyDataSetChanged();
	}

	public void addItem(ArrayList<DevicesItem> temp) {
		if (temp == null) {
			return;
		}
		if (temp instanceof ArrayList) {
			list.addAll(temp);
		}
		notifyDataSetChanged();
	}

	public int getBoxId(int position) {
		return MathUtil.stringToInt(list.get(position).id);
	}

	public String getBoxName(int position) {
		return list.get(position).name;
	}
	
	public String getBoxAddress(int position) {
		return list.get(position).address;
	}
}
