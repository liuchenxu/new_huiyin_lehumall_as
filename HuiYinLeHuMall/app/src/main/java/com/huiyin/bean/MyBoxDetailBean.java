package com.huiyin.bean;

import java.io.Serializable;

/**
 * 速易递箱子自提，订单详情中箱子详情
 * @author zhyao 
 *
 */
public class MyBoxDetailBean implements Serializable{

	private static final long serialVersionUID = 11212332L;

	public BoxAddressDTO boxAddressDTO;
	
	public int box_no;
	public int box_type;
	public int brand_type;
	public int code;
	public String device_id;
	public String msg;
	public String order_code;
	public String order_id;
	public String resv_code;
	public String sender_mobile;
	public String sender_name;
	public int status;
	
	public class BoxAddressDTO {
		public String android_code;
		public String android_edition;
		public String area;
		public String box_address;
		public String box_name;
		public String box_type;
		public String city;
		public String id;
		public String ios_code;
		public String ios_edition;
		public String latitude;
		public String longitude;
		public String point_id;
		public String point_name;
		public String province;
	}
}
