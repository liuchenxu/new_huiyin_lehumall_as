package com.huiyin.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.text.TextUtils;

import com.huiyin.R;
import com.huiyin.utils.ResourceUtils;
import com.huiyin.utils.StringUtils;

/**
 * 点评晒单列表
 * @author 刘远祺
 *
 * @todo TODO
 *
 * @date 2015-8-14
 */
public class DianPingShaiDanList implements Serializable {

	private static final long serialVersionUID = 1L;
	public String type;
	public String msg;

	public ArrayList<DianPing> reviewList = new ArrayList<DianPing>();// 也许喜欢

	public class DianPing {

		public String PHONE; 				// 手机号
		public String FACE_IMAGE_PATH; 		// 用户头像
		
		public String ID;					// 评论ID
		public String CONTENT; 				// 评价内容
		public String SCORE; 				// 分数
		
		public String USER_NAME; 			// 用户名
		public String CREATE_TIME; 			// 创建时间
		public String BUY_TIME; 			// 购买时间
		public String COMMODITY_IMAGE_PATH; // 晒图
		public String NUM; 					// 数量
		
		public String PRIASE_NUM; 			// 点赞数
		public String REPLY_NUM; 			// 回复数
		public String COMMDOITY_NAME; 		// 型号
		
		public String LEVEL_NAME; 			// 用户等级
		public String SEPC_VALUE; 			// 颜色 型号
		public String REPLY_STATUS; 		// 回复状态 1为已回复
		public String REPLY_TIME; 			// 回复时间(管理员回复时间)
		public String REPLY_CONTENT; 		// 回复内容(管理员回复内容)
		
		
		public String ADD_EVA_ID;			// 追加评论ID
		public String ADD_CONTENT;			// 追加评论内容
		public String ADD_EVA_IMG;			// 追加评论图片
		public String ADD_EVA_TIME;			// 追加时间 
		public String ADD_PRIASE_NUM;		// 追加评论点赞数
		public String ADD_REPLY_NUM;		// 追加评论回复数
		
		public List<GoodsNormData> GOODS_NORM_DATA;//规格集合
		
		public List<Review> admin_reply_list;//管理员对主评的回复
		public List<Review> admin_add_reply_list;//管理员对追评的回复
		
		public String getNormData(){
			
			if(null == GOODS_NORM_DATA || GOODS_NORM_DATA.size() == 0){
				return "";
			}
			
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<GOODS_NORM_DATA.size(); i++){
				sb.append(ResourceUtils.changeStringColor("#a8a8a8", GOODS_NORM_DATA.get(i).NORMS_NAME)).append("：").append(GOODS_NORM_DATA.get(i).NORMS_VALUE);
				if(i < (GOODS_NORM_DATA.size()-1)){
					sb.append("<br />");
				}
			}
			return sb.toString();
		}
		
		
		/**
		 * 获得管理员回复列表
		 * @return
		 */
		public String getAdminReplyList(){
			
			if(null == admin_reply_list || admin_reply_list.size() == 0){
				return "";
			}
			
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<admin_reply_list.size(); i++){
				sb.append(ResourceUtils.changeStringColor("#a8a8a8", "管理员于"))
				.append(ResourceUtils.changeStringColor("#a8a8a8", admin_reply_list.get(i).REPLY_TIME))
				.append(ResourceUtils.changeStringColor("#a8a8a8", "回复：")).append("<br />")
				.append(admin_reply_list.get(i).CONTENT);
				if(i < (admin_reply_list.size()-1)){
					sb.append("<br />");
				}
			}
			return sb.toString();
		}
		
		/**
		 * 获得管理员回复列表
		 * @return
		 */
		public String getAdminAddReplyList(){
			
			if(null == admin_add_reply_list || admin_add_reply_list.size() == 0){
				return "";
			}
			
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<admin_add_reply_list.size(); i++){
				sb.append(ResourceUtils.changeStringColor("#a8a8a8", "管理员于"))
				.append(ResourceUtils.changeStringColor("#a8a8a8", admin_add_reply_list.get(i).REPLY_TIME))
				.append(ResourceUtils.changeStringColor("#a8a8a8", "回复：")).append("<br />")
				.append(admin_add_reply_list.get(i).CONTENT);
				if(i < (admin_add_reply_list.size()-1)){
					sb.append("<br />");
				}
			}
			return sb.toString();
		}
		
		
		/**
		 * 获取评分图标
		 * @return
		 */
		public int getScoreImage() {
			String score = SCORE == null ? "" : SCORE;
			if (score.equals("5")) {
				return R.drawable.px_5;
			} else if (score.equals("4")) {
				return R.drawable.px_4;
			} else if (score.equals("3")) {
				return R.drawable.px_3;
			} else if (score.equals("2")) {
				return R.drawable.px_2;
			} else if (score.equals("1")) {
				return R.drawable.px_1;
			} else {
				return R.drawable.px_0;
			}
		}
		
		
		public ArrayList<String> getIMGList() {
			
			String pic = COMMODITY_IMAGE_PATH;
			
			ArrayList<String> slist = new ArrayList<String>();
			slist.clear();
			
			if(null != pic){
				String[] s = pic.split(",");
				for (int i = 0; i < s.length; i++) {
					slist.add(s[i]);
				}
			}
			return slist;
		}


		/**
		 * 添加点赞数
		 * @param i
		 */
		public void addDianzan(int addCount) {
			try{
			
				int count = Integer.parseInt(PRIASE_NUM);
				count += addCount;
				PRIASE_NUM = String.valueOf(count);

			}catch(Exception e){
				PRIASE_NUM = "1";
			}
			
		}
		
		/**
		 * 添加点赞数
		 * @param i
		 */
		public void setDianzan(int count) {
			PRIASE_NUM = String.valueOf(count);
		}
		
		/**
		 * 添加追加点赞数
		 * @param i
		 */
		public void setEvaDianzan(int count) {
			ADD_PRIASE_NUM = String.valueOf(count);
		}
		
		/**
		 * 添加追加点赞数
		 * @param i
		 */
		public void addEvaDianzan(int addCount) {
			try{
			
				int count = Integer.parseInt(ADD_PRIASE_NUM);
				count += addCount;
				ADD_PRIASE_NUM = String.valueOf(count);

			}catch(Exception e){
				ADD_PRIASE_NUM = "1";
			}
		}


		/**
		 * 判断是否有追加评论
		 * @return
		 */
		public boolean hasAddComment() {
			if(!TextUtils.isEmpty(ADD_EVA_ID) && !TextUtils.isEmpty(ADD_CONTENT)){
				return true;
			}
			return false;
		}

		
		/**
		 * 获取追加评论图片
		 * @return
		 */
		public ArrayList<String> getIMGListAdd() {
			String pic = ADD_EVA_IMG;
			
			ArrayList<String> slist = new ArrayList<String>();
			slist.clear();
			
			if(null != pic){
				String[] s = pic.split(",");
				for (int i = 0; i < s.length; i++) {
					slist.add(s[i]);
				}
			}
			return slist;
		}


		public String getUSER_NAME() {
			if(null != USER_NAME){
				if(StringUtils.isPhoneNumber(USER_NAME)){
					return StringUtils.ensurePhoneNum(USER_NAME);
				}
			}
			return USER_NAME;
		}
	}
	
	/**
	 * 规格
	 * @author 刘远祺
	 *
	 * @todo TODO
	 *
	 * @date 2015-8-17
	 */
	public class GoodsNormData{
		 public String NORMS_NAME;
		 public String GOODS_NO;
		 public String NORMS_VALUE;
	}
}
