package com.huiyin.bean;

import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;

/**
 * 秀场首页bean
 * @author zhyao
 *
 */
public class MomentsShowListBean extends BaseBean {

	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 1232242379L;

	private int pageIndex;

	private int totalPageNum;

	private ArrayList<MomentsBannerItem> bannerList;

	private ArrayList<MomentsCircleItem> circleList;

	private ArrayList<MomentsShowItem> showList;
	
	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getTotalPageNum() {
		return totalPageNum;
	}

	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}

	public ArrayList<MomentsBannerItem> getBannerList() {
		return bannerList;
	}

	public void setBannerList(ArrayList<MomentsBannerItem> bannerList) {
		this.bannerList = bannerList;
	}

	public ArrayList<MomentsCircleItem> getCircleList() {
		return circleList;
	}

	public void setCircleList(ArrayList<MomentsCircleItem> circleList) {
		this.circleList = circleList;
	}

	public ArrayList<MomentsShowItem> getShowList() {
		return showList;
	}

	public void setShowList(ArrayList<MomentsShowItem> showList) {
		this.showList = showList;
	}
	
	/**
	 * 秀场首页banner
	 * @author zhyao
	 *
	 */
	public class MomentsBannerItem {
		private String URL;

		private String IMG;

		public String getURL() {
			return URL;
		}

		public void setURL(String uRL) {
			URL = uRL;
		}

		public String getIMG() {
			return IMG;
		}

		public void setIMG(String iMG) {
			IMG = iMG;
		}
		
		
	}


	/**
	 * json解析
	 * @param json
	 * @param context
	 * @return
	 */
	public static MomentsShowListBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			MomentsShowListBean bean = gson.fromJson(json, MomentsShowListBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}

	
}
