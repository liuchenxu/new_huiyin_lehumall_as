package com.huiyin.bean;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

import com.huiyin.utils.StringUtils;

/**
 * 回复
 * 
 * @author 刘远祺
 * 
 * @todo TODO
 * 
 * @date 2015-8-15
 */
public class Review {

	public String BE_USER_NAME;
	public String USER_ID;				//评论发起的userId
	public String F_USER_NAME;			
	public String USER_TYPE;			//用户类型	
	public String EVA_REPLY_ID;			//
	public String BE_TYPE;				//
	public String CONTENT;				//评论内容
	public String USER_NAME;
	public String REPLYTIMETAMP;		//
	public String ID;					//评论ID
	public String EVA_GOODS_ID;
	public String NUM;
	public String REPLY_TIME;
	public String TYPE;

	public Review(JSONObject obj) {
		this.parse(obj);
	}
	
	public Review(String objJson) {
		try {
			JSONObject obj = new JSONObject(objJson);
			obj = obj.optJSONObject("replay_map");
			
			this.parse(obj);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parse(JSONObject obj){
		
		if(null != obj){
			BE_USER_NAME = obj.optString("BE_USER_NAME");
			USER_ID = obj.optString("USER_ID");
			F_USER_NAME = obj.optString("F_USER_NAME");
			USER_TYPE = obj.optString("USER_TYPE");
			EVA_REPLY_ID = obj.optString("EVA_REPLY_ID");
			BE_TYPE = obj.optString("BE_TYPE");
			CONTENT = obj.optString("CONTENT");
			USER_NAME = obj.optString("USER_NAME");
			REPLYTIMETAMP = obj.optString("REPLYTIMETAMP");
			ID = obj.optString("ID");
			EVA_GOODS_ID = obj.optString("EVA_GOODS_ID");
			NUM = obj.optString("NUM");
			REPLY_TIME = obj.optString("REPLY_TIME");
			TYPE = obj.optString("TYPE");
		}
	}

	/**
	 * 返回评论楼层
	 * @param index
	 * @return
	 */
	public CharSequence getShafa(int index) {
		//沙发，板凳，地板
		switch (index) {
		case 0:
			return "沙发";
		case 1:
			return "板凳";
		case 2:
			return "地板";
		default:
			return (++index)+"#";
		}
	}

	/**
	 * 返回用户名
	 * @return
	 */
	public String getUSER_NAME() {
		//1、会员  2、店铺、3、管理员
		if(TextUtils.isEmpty(USER_TYPE)){
			if(null != USER_NAME){
				if(StringUtils.isPhoneNumber(USER_NAME)){
					return StringUtils.ensurePhoneNum(USER_NAME);
				}
			}
			return USER_NAME;
		}
		if("3".equals(USER_TYPE)){
			return "管理员";
		}else if("2".equals(USER_TYPE)){
			return "店铺";
		}else{
			
			//如果用户名是手机号码
			if(null != USER_NAME){
				if(StringUtils.isPhoneNumber(USER_NAME)){
					return StringUtils.ensurePhoneNum(USER_NAME);
				}
			}
		}
		return USER_NAME;
	}

	public String getF_USER_NAME() {
		if(null != F_USER_NAME){
			if(StringUtils.isPhoneNumber(F_USER_NAME)){
				return StringUtils.ensurePhoneNum(F_USER_NAME);
			}
		}
		return F_USER_NAME;
	}

	public String getBE_USER_NAME() {
		if(null != BE_USER_NAME){
			if(StringUtils.isPhoneNumber(BE_USER_NAME)){
				return StringUtils.ensurePhoneNum(BE_USER_NAME);
			}
		}
		return BE_USER_NAME;
	}
}
