package com.huiyin.bean;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;
//add by zhyao @2015/7/25 添加箱子自提bean
public class NearBoxBean extends BaseBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public ArrayList<NearByItem> nearbyList;

	public class NearByItem {
		public ArrayList<DevicesItem> devices;//
		public String name;//
		public String id;//
	}
	
	public class DevicesItem {
		public String address;
		public String id;
		public String name;
	}

	public static NearBoxBean explainJson(String json, Context context) {

		Gson gson = new Gson();
		try {
			NearBoxBean experLightBean = gson.fromJson(json,
					NearBoxBean.class);
			return experLightBean;
		} catch (Exception e) {
			LogUtil.d("NearBoxBean", e.toString());
			Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
			return null;

		}
	}
}
