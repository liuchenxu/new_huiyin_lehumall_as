package com.huiyin.bean;

import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;

/**
 * 圈子列表
 * @author zhyao 
 *
 */
public class MomentsCircleListBean extends BaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1445898751214453986L;
	
	private ArrayList<MomentsCircleItem> circleList;

	public ArrayList<MomentsCircleItem> getCircleList() {
		return circleList;
	}

	public void setCircleList(ArrayList<MomentsCircleItem> circleList) {
		this.circleList = circleList;
	}


	/**
	 * json解析
	 * @param json
	 * @param context
	 * @return
	 */
	public static MomentsCircleListBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			MomentsCircleListBean bean = gson.fromJson(json, MomentsCircleListBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}
	
}
