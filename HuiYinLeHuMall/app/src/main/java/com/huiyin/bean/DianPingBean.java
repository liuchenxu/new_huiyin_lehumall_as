package com.huiyin.bean;

import java.io.Serializable;

/**
 * 商品-评价晒单(顶部各个状态的评价数)
 * 
 * @author 刘远祺
 * 
 * @todo TODO
 * 
 * @date 2015-8-13
 */
public class DianPingBean implements Serializable {

	private static final long serialVersionUID = 1L;

	public String ZNUM; 		// 全部评论
	public String HAONUM; 		// 好评数
	public String HAOPING; 		// 好评率
	public String ZHONGNUM; 	// 中评数
	public String ZHONGPING; 	// 中评率
	public String CHANUM; 		// 差评数
	public String CHAPING; 		// 差评率
	public String SHAIDANNUM;	// 晒单数
	public String SHAIDANPING;	// 晒单率
}