package com.huiyin.bean;

/**
 * 
 * 
 * @author Administrator
 *
 */
public class HomeAdBean {
	/*adv01: {
	LINK_URL: "22"
	NAME: "移动端首页免费试用活动"
	ID: 500
	IMG: "upload/image/admin/2015/20151223/201512231052049191.jpg"
	}*/
	private String LINK_URL;
	
	private String NAME;
	
	private int ID;
	
	private String IMG;

	public String getLINK_URL() {
		return LINK_URL;
	}

	public void setLINK_URL(String lINK_URL) {
		LINK_URL = lINK_URL;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getIMG() {
		return IMG;
	}

	public void setIMG(String iMG) {
		IMG = iMG;
	}
	
	
}
