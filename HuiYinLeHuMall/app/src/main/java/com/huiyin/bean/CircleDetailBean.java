package com.huiyin.bean;

import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;

/**
 * 秀场圈子详情
 * 
 * @author zhyao
 * 
 */
public class CircleDetailBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 11231231L;

	private CircleMap circleMap;

	private int pageIndex;

	private int totalPageNum;

	private ArrayList<MomentsShowItem> showList;

	public class CircleMap {

		/** 今日发布数量 **/
		private String COUNT_TODAY;
		/** 发布总数量 **/
		private String COUNT_ALL;
		/** 圈子名称 **/
		private String NAME;
		/** 圈子简介 **/
		private String DESCRIPTION;
		private String CREATE_TIME;
		private String UPDATE_ID;
		private String RELEASE_COUNT;
		private String UPDATE_TIME;
		private String SORT;
		private String ENABLE;
		/** 圈子ID **/
		private String ID;
		private String CREATE_ID;
		private String DELETE_FLAG;
		/** 圈子缩略图 **/
		private String IMG;

		public String getCOUNT_TODAY() {
			return COUNT_TODAY;
		}

		public void setCOUNT_TODAY(String cOUNT_TODAY) {
			COUNT_TODAY = cOUNT_TODAY;
		}

		public String getCOUNT_ALL() {
			return COUNT_ALL;
		}

		public void setCOUNT_ALL(String cOUNT_ALL) {
			COUNT_ALL = cOUNT_ALL;
		}

		public String getNAME() {
			return NAME;
		}

		public void setNAME(String nAME) {
			NAME = nAME;
		}

		public String getDESCRIPTION() {
			return DESCRIPTION;
		}

		public void setDESCRIPTION(String dESCRIPTION) {
			DESCRIPTION = dESCRIPTION;
		}

		public String getCREATE_TIME() {
			return CREATE_TIME;
		}

		public void setCREATE_TIME(String cREATE_TIME) {
			CREATE_TIME = cREATE_TIME;
		}

		public String getUPDATE_ID() {
			return UPDATE_ID;
		}

		public void setUPDATE_ID(String uPDATE_ID) {
			UPDATE_ID = uPDATE_ID;
		}

		public String getRELEASE_COUNT() {
			return RELEASE_COUNT;
		}

		public void setRELEASE_COUNT(String rELEASE_COUNT) {
			RELEASE_COUNT = rELEASE_COUNT;
		}

		public String getUPDATE_TIME() {
			return UPDATE_TIME;
		}

		public void setUPDATE_TIME(String uPDATE_TIME) {
			UPDATE_TIME = uPDATE_TIME;
		}

		public String getSORT() {
			return SORT;
		}

		public void setSORT(String sORT) {
			SORT = sORT;
		}

		public String getENABLE() {
			return ENABLE;
		}

		public void setENABLE(String eNABLE) {
			ENABLE = eNABLE;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public String getCREATE_ID() {
			return CREATE_ID;
		}

		public void setCREATE_ID(String cREATE_ID) {
			CREATE_ID = cREATE_ID;
		}

		public String getDELETE_FLAG() {
			return DELETE_FLAG;
		}

		public void setDELETE_FLAG(String dELETE_FLAG) {
			DELETE_FLAG = dELETE_FLAG;
		}

		public String getIMG() {
			return IMG;
		}

		public void setIMG(String iMG) {
			IMG = iMG;
		}

	}

	public CircleMap getCircleMap() {
		return circleMap;
	}

	public void setCircleMap(CircleMap circleMap) {
		this.circleMap = circleMap;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getTotalPageNum() {
		return totalPageNum;
	}

	public void setTotalPageNum(int totalPageNum) {
		this.totalPageNum = totalPageNum;
	}

	public ArrayList<MomentsShowItem> getShowList() {
		return showList;
	}

	public void setShowList(ArrayList<MomentsShowItem> showList) {
		this.showList = showList;
	}

	/**
	 * json解析
	 * 
	 * @param json
	 * @param context
	 * @return
	 */
	public static CircleDetailBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			CircleDetailBean bean = gson.fromJson(json, CircleDetailBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}

}
