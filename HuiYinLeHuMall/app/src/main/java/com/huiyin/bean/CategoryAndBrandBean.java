package com.huiyin.bean;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;

import com.google.gson.Gson;
import com.huiyin.utils.LogUtil;

/**
 * 分类与品牌
 */
public class CategoryAndBrandBean implements Serializable{

	private static final long serialVersionUID = 3573198037072208576L;

	private int type;
	
	private String msg;
	
	//分类
	private ArrayList<Category> categories;
	
	//国家
	private ArrayList<NationalPavilion> nationalPavilionList;
	
	//品牌
	private ArrayList<Navigator> navigators;
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ArrayList<Category> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}

	public ArrayList<Navigator> getNavigators() {
		return navigators;
	}

	public void setNavigators(ArrayList<Navigator> navigators) {
		this.navigators = navigators;
	}

	public ArrayList<NationalPavilion> getNationalPavilionList() {
		return nationalPavilionList;
	}

	public void setNationalPavilionList(ArrayList<NationalPavilion> nationalPavilionList) {
		this.nationalPavilionList = nationalPavilionList;
	}



	/**
	 *  分类
	 * @author Administrator
	 *
	 */
	public class Category implements Serializable{
		
		private static final long serialVersionUID = -2610093903401804431L;

		private String CATEGORY_NAME;
		
		private String SUB_TITLE;
		
		private String RANK;
		
		private String ID;
		
		private String BG_IMG_URL;
		
		private ArrayList<SubCategory> sub_categories;

		public String getCATEGORY_NAME() {
			return CATEGORY_NAME;
		}

		public void setCATEGORY_NAME(String cATEGORY_NAME) {
			CATEGORY_NAME = cATEGORY_NAME;
		}

		public String getSUB_TITLE() {
			return SUB_TITLE;
		}

		public void setSUB_TITLE(String sUB_TITLE) {
			SUB_TITLE = sUB_TITLE;
		}

		public String getRANK() {
			return RANK;
		}

		public void setRANK(String rANK) {
			RANK = rANK;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public String getBG_IMG_URL() {
			return BG_IMG_URL;
		}

		public void setBG_IMG_URL(String bG_IMG_URL) {
			BG_IMG_URL = bG_IMG_URL;
		}

		public ArrayList<SubCategory> getSub_categories() {
			return sub_categories;
		}

		public void setSub_categories(ArrayList<SubCategory> sub_categories) {
			this.sub_categories = sub_categories;
		}
		
		
	}
	
	/**
	 * 品牌
	 * @author Administrator
	 *
	 */
	public class Navigator implements Serializable{

		private static final long serialVersionUID = -8710149617235607224L;

		private String FIRST_CLASS_CATEGORY_ID;
		
		private String NAME;
		
		private String SECOND_CLASS_CATEGORY_ID;
		
		private String THIRD_CLASS_CATEGORY_ID;
		
		private ArrayList<Brand> brands;

		public String getFIRST_CLASS_CATEGORY_ID() {
			return FIRST_CLASS_CATEGORY_ID;
		}

		public void setFIRST_CLASS_CATEGORY_ID(String fIRST_CLASS_CATEGORY_ID) {
			FIRST_CLASS_CATEGORY_ID = fIRST_CLASS_CATEGORY_ID;
		}

		public String getNAME() {
			return NAME;
		}

		public void setNAME(String nAME) {
			NAME = nAME;
		}

		public String getSECOND_CLASS_CATEGORY_ID() {
			return SECOND_CLASS_CATEGORY_ID;
		}

		public void setSECOND_CLASS_CATEGORY_ID(String sECOND_CLASS_CATEGORY_ID) {
			SECOND_CLASS_CATEGORY_ID = sECOND_CLASS_CATEGORY_ID;
		}

		public String getTHIRD_CLASS_CATEGORY_ID() {
			return THIRD_CLASS_CATEGORY_ID;
		}

		public void setTHIRD_CLASS_CATEGORY_ID(String tHIRD_CLASS_CATEGORY_ID) {
			THIRD_CLASS_CATEGORY_ID = tHIRD_CLASS_CATEGORY_ID;
		}

		public ArrayList<Brand> getBrands() {
			return brands;
		}

		public void setBrands(ArrayList<Brand> brands) {
			this.brands = brands;
		}
		
	}
	
	/**
	 * 子分类
	 * @author Administrator
	 *
	 */
	public class SubCategory implements Serializable{
		
		private static final long serialVersionUID = 6191770912491267796L;

		private String CATEGORY_NAME;
		
		private String RANK;
		
		private String ID;

		public String getCATEGORY_NAME() {
			return CATEGORY_NAME;
		}

		public void setCATEGORY_NAME(String cATEGORY_NAME) {
			CATEGORY_NAME = cATEGORY_NAME;
		}

		public String getRANK() {
			return RANK;
		}

		public void setRANK(String rANK) {
			RANK = rANK;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}
		
	}
	
	/**
	 * 子品牌
	 * @author Administrator
	 *
	 */
	public class Brand implements Serializable{
		
		private static final long serialVersionUID = -4344666382236807944L;

		private String NAME;
		
		private String ID;
		
		private String IMG;
		
		private String FAST_GOODSCATEGORY_ID;

		public String getNAME() {
			return NAME;
		}

		public void setNAME(String nAME) {
			NAME = nAME;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public String getIMG() {
			return IMG;
		}

		public void setIMG(String iMG) {
			IMG = iMG;
		}

		public String getFAST_GOODSCATEGORY_ID() {
			return FAST_GOODSCATEGORY_ID;
		}

		public void setFAST_GOODSCATEGORY_ID(String fAST_GOODSCATEGORY_ID) {
			FAST_GOODSCATEGORY_ID = fAST_GOODSCATEGORY_ID;
		}
		
	}
	
	/**
	 * 国家
	 * @author Administrator
	 *
	 */
	public class NationalPavilion implements Serializable{
		
		private static final long serialVersionUID = 9052598216190757572L;

		private String ORIGIN_IDS;
		
		private String ID;
		
		private String IMG;

		public String getORIGIN_IDS() {
			return ORIGIN_IDS;
		}

		public void setORIGIN_IDS(String oRIGIN_IDS) {
			ORIGIN_IDS = oRIGIN_IDS;
		}

		public String getID() {
			return ID;
		}

		public void setID(String iD) {
			ID = iD;
		}

		public String getIMG() {
			return IMG;
		}

		public void setIMG(String iMG) {
			IMG = iMG;
		}
	}
	
	public static CategoryAndBrandBean explainJson(String json, Context context) {
		Gson gson = new Gson();
		try {
			CategoryAndBrandBean bean = gson.fromJson(json, CategoryAndBrandBean.class);
			return bean;
		} catch (Exception e) {
			LogUtil.d("dataExplainJson", e.toString());
			return null;
		}
	}
}
