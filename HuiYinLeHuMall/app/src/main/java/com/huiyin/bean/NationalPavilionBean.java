package com.huiyin.bean;

public class NationalPavilionBean {
	private String ORIGIN_IDS;
	
	private String ID;
	
	private String IMG;

	public String getORIGIN_IDS() {
		return ORIGIN_IDS;
	}

	public void setORIGIN_IDS(String oRIGIN_IDS) {
		ORIGIN_IDS = oRIGIN_IDS;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getIMG() {
		return IMG;
	}

	public void setIMG(String iMG) {
		IMG = iMG;
	}
	
	
}
