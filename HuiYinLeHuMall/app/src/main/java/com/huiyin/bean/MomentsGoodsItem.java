package com.huiyin.bean;

import java.io.Serializable;
/**
 * 秀好物商品
 * @author zhyao
 *
 */
public class MomentsGoodsItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7775748539687068650L;
	
	private String RANKING;
	/** 商品价格 **/
	private String GOODS_PRICE;
	/** 实际购买价格 **/
	private String PRICE;
	private String GOODS_STATUS;
	private String EXAMINE_STATUS;
	private String ROWNO;
	private String CREATE_DATE;
	/** 商品名称 **/
	private String GOODS_NAME;
	/** 商品ID **/
	private String GOODS_ID;
	/** 商品图片 **/
	private String IMG;

	public String getRANKING() {
		return RANKING;
	}

	public void setRANKING(String rANKING) {
		RANKING = rANKING;
	}

	public String getGOODS_PRICE() {
		return GOODS_PRICE;
	}

	public void setGOODS_PRICE(String gOODS_PRICE) {
		GOODS_PRICE = gOODS_PRICE;
	}

	public String getPRICE() {
		return PRICE;
	}

	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}

	public String getGOODS_STATUS() {
		return GOODS_STATUS;
	}

	public void setGOODS_STATUS(String gOODS_STATUS) {
		GOODS_STATUS = gOODS_STATUS;
	}

	public String getEXAMINE_STATUS() {
		return EXAMINE_STATUS;
	}

	public void setEXAMINE_STATUS(String eXAMINE_STATUS) {
		EXAMINE_STATUS = eXAMINE_STATUS;
	}

	public String getROWNO() {
		return ROWNO;
	}

	public void setROWNO(String rOWNO) {
		ROWNO = rOWNO;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getGOODS_NAME() {
		return GOODS_NAME;
	}

	public void setGOODS_NAME(String gOODS_NAME) {
		GOODS_NAME = gOODS_NAME;
	}

	public String getGOODS_ID() {
		return GOODS_ID;
	}

	public void setGOODS_ID(String gOODS_ID) {
		GOODS_ID = gOODS_ID;
	}

	public String getIMG() {
		return IMG;
	}

	public void setIMG(String iMG) {
		IMG = iMG;
	}

}
