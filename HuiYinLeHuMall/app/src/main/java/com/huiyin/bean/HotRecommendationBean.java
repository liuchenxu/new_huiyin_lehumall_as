package com.huiyin.bean;

import java.util.ArrayList;

// add by zhyao @2016/2/25 首页热点推荐
public class HotRecommendationBean {
	
	private String title;
	 
	private String desc;
	
	private ArrayList<RecommendationBean> recommendations;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public ArrayList<RecommendationBean> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(ArrayList<RecommendationBean> recommendations) {
		this.recommendations = recommendations;
	}

	public class RecommendationBean {
		private ArrayList<GoodBean> goods;
		
		private int TEMPLATE;
		
		private int ID;

		public ArrayList<GoodBean> getGoods() {
			return goods;
		}

		public void setGoods(ArrayList<GoodBean> goods) {
			this.goods = goods;
		}

		public int getTEMPLATE() {
			return TEMPLATE;
		}

		public void setTEMPLATE(int tEMPLATE) {
			TEMPLATE = tEMPLATE;
		}

		public int getID() {
			return ID;
		}

		public void setID(int iD) {
			ID = iD;
		}
		
		
	}
	
	public class GoodBean {
		private String IMG_URL;
		
		private int GOODS_ID;

		public String getIMG_URL() {
			return IMG_URL;
		}

		public void setIMG_URL(String iMG_URL) {
			IMG_URL = iMG_URL;
		}

		public int getGOODS_ID() {
			return GOODS_ID;
		}

		public void setGOODS_ID(int gOODS_ID) {
			GOODS_ID = gOODS_ID;
		}
		
	}
}
