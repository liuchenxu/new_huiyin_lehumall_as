package com.huiyin.bean;

import java.io.Serializable;

public class AddressItem implements Serializable {
 
	private static final long serialVersionUID = 1L;
	public String CITY_ID;// 市
	public String ADDRESSID;// 地址条目id
	public String IS_DEFAULT;// 0否1是
	public String CONSIGNEE_PHONE;// 电话号码
	public String ADDRESS;// 详细地址
	public String CONSIGNEE_NAME;// 收件人姓名
	public String POSTAL_CODE;//邮编号码
	public String AREA_ID;// 区
	public String PROVINCE_ID;// 省
	public String LEVELADDR;// 省市县名字
	//add by zhyao @2016/5/30
	public String IS_VALIDATED;// 实名认证 0未认证1已认证
	public String ID_CARD_NUMBER;//身份证号
	public String CARD_FRONT_IMG;//身份证正面
	public String CARD_BACK_IMG;//身份证反面
	
}

 